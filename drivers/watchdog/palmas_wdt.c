/*
 * Driver for Watchdog part of Palmas PMIC Chips
 *
 * Copyright 2015 Nuran Wireless
 *
 * Author: Yves Godin <support@nuranwireless.com>
 *
 * Based on twl4030_wdt.c
 *
 * Author: Timo Kokkonen <timo.t.kokkonen at nokia.com>
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under  the terms of the GNU General  Public License as published by the
 *  Free Software Foundation;  either version 2 of the License, or (at your
 *  option) any later version.
 *
 */

#include <linux/module.h>
#include <linux/delay.h>
#include <linux/types.h>
#include <linux/slab.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/watchdog.h>
#include <linux/platform_device.h>
#include <linux/mfd/palmas.h>
#include <linux/interrupt.h>
#include <linux/palmas_wdt.h>
#include <linux/delay.h>
#include <linux/timer.h>


struct palmas_wdt_data {
	struct tasklet_struct *palmas_wdt_tasklet;
	struct palmas *palmas;
	volatile bool allowtoclear;
	bool alive;
	void __iomem *bankaddr;
	struct timer_list alarm_clock;
	bool timershutdown;
	PALMAS_WDT_BOOT_TYPE bootcount_orig;
};

/* Number of max attempt to program the watchdog */
#define PALMAS_WDT_PGM_LOOP 2

/* Number of max attempt to read state of the hw watchdog (must at least >= 2, > 2 gives some safety against reading errors) */
#define PALMAS_WDT_RD_LOOP 3

#define PALMAS_WDT_ENABLED	(1<<4)

/* Offset for gpiox irq0 bitset */
#define PALMAS_WDT_GPIO_IRQSTATUS_SET_0 0x34

/* Offset for gpiox irq0 bitclear */
#define PALMAS_WDT_GPIO_IRQSTATUS_CLR_0 0x3c

/* gpio base bank size */
#define PALMAS_WDT_GPIO_BANK_SIZE 0x198

static bool nowayout = WATCHDOG_NOWAYOUT;
module_param(nowayout, bool, 0);
MODULE_PARM_DESC(nowayout, "Watchdog cannot be stopped once started "
	"(default=" __MODULE_STRING(WATCHDOG_NOWAYOUT) ")");


static void alarm_trigger(unsigned long arg)
{
	struct palmas_wdt_data *mdev = (struct palmas_wdt_data *)arg;
	unsigned long now;
	unsigned long expire;

	/* Makes sure wdt irq servicing is disable if no wdt is ticked anymore */
	if (!mdev->allowtoclear) {
	        writel_relaxed((1 << mdev->palmas->irq_bit),(mdev->bankaddr + PALMAS_WDT_GPIO_IRQSTATUS_CLR_0));
		/*printk(KERN_ERR "palmas_wdt: alarm_trigger wdt not allowed to be cleared from now without ticking\n");*/
	}
/*
	else {
		printk(KERN_ERR "palmas_wdt: alarm_trigger\n");
	}
*/
	if (!mdev->timershutdown) {
		now = jiffies;
		expire = now + HZ; /* delay of 1 second */
		mod_timer(&mdev->alarm_clock,expire);
	}
}

static void set_alarm_clock(struct palmas_wdt_data *mdev)
{
	unsigned long now = jiffies;
	unsigned long expire = now + HZ; /* delay of 1 second */

	init_timer(&mdev->alarm_clock);

	mdev->alarm_clock.function = alarm_trigger;
	mdev->alarm_clock.expires = expire;
	mdev->alarm_clock.data = (unsigned long)mdev;

	add_timer(&mdev->alarm_clock);
}

static void palmas_wdt_tasklet_func(unsigned long data)
{
	struct palmas_wdt_data *palmas_wdt_data;

	palmas_wdt_data = watchdog_get_drvdata((void *)(data));
        if (palmas_wdt_data == NULL) {
		return;
	}

	if (!palmas_wdt_data->allowtoclear) {
	        writel_relaxed((1 << palmas_wdt_data->palmas->irq_bit),(palmas_wdt_data->bankaddr + PALMAS_WDT_GPIO_IRQSTATUS_CLR_0));
		printk(KERN_INFO "palmas_wdt: wdt not allowed to be cleared from now without ticking\n");

		if (!palmas_wdt_data->timershutdown) {
			/* Set alarm clock to validate the irq desactivation */
			if (!timer_pending(&palmas_wdt_data->alarm_clock)){
				set_alarm_clock(palmas_wdt_data);
			}
		}
	}
}


static int palmas_wdt_write(struct watchdog_device *wdt, unsigned char val)
{
	struct palmas_wdt_data *palmas_wdt_data;
	struct palmas *palmas;

	palmas_wdt_data = watchdog_get_drvdata(wdt);
        if (palmas_wdt_data == NULL) {
		return -ENODEV;
	}

	palmas = palmas_wdt_data->palmas;

        if (palmas == NULL) {
		return -ENODEV;
	}

	/*printk(KERN_ERR "palmas_wdt: palmas_wdt_write (%d)\n", val );*/
	return palmas_write(palmas, PALMAS_PMU_CONTROL_BASE, PALMAS_WATCHDOG, val);
}

/* val returns wdt programmed register value */
static int palmas_wdt_read(struct watchdog_device *wdt, unsigned char *val)
{
	struct palmas_wdt_data *palmas_wdt_data;
	struct palmas *palmas;
	int ret;
	unsigned rval=0;

	*val = 0;

	palmas_wdt_data = watchdog_get_drvdata(wdt);
        if (palmas_wdt_data == NULL) {
		return -ENODEV;
	}

	palmas = palmas_wdt_data->palmas;

        if (palmas == NULL) {
		return -ENODEV;
	}

	ret = palmas_read(palmas, PALMAS_PMU_CONTROL_BASE, PALMAS_WATCHDOG, &rval);

	if (ret == 0) {
		*val = (unsigned char)(rval);
		/*printk(KERN_ERR "palmas_wdt: palmas_wdt_read (%d)\n", *rval );*/
	}
	return (ret);
}

/* val returns 1 if wdt irq is enable */
static int palmas_wdtirq_read(struct watchdog_device *wdt, unsigned char *val)
{
	struct palmas_wdt_data *palmas_wdt_data;
	struct palmas *palmas;
	int ret;
	unsigned rval=0xff;

	*val = 0;

	palmas_wdt_data = watchdog_get_drvdata(wdt);
        if (palmas_wdt_data == NULL) {
		return -ENODEV;
	}

	palmas = palmas_wdt_data->palmas;

        if (palmas == NULL) {
		return -ENODEV;
	}

	ret = palmas_read(palmas, PALMAS_INTERRUPT_BASE, PALMAS_INT2_MASK, &rval);

	if (ret == 0) {
		/* Is wdt irq enabled ? */
		if ((rval & PALMAS_INT2_MASK_WDT) == 0) {
			*val = 1;
		}
		/*printk(KERN_ERR "palmas_wdt: palmas_wdtirq_read (%d)\n", *rval );*/
	}
	return (ret);
}

static int palmas_update_boot_info(struct palmas *palmas, enum palmas_wdt_boot_step step, bool count, unsigned bootcount)
{
	int ret;
	struct palmas_wdt_boot bootinfo;

	/* Initialize it */
	memset((void *)(&bootinfo),0,sizeof(bootinfo));

        if (palmas == NULL) {
		return -ENODEV;
	}

	/* Get current checksum */
	ret = palmas_read(palmas, PALMAS_WDT_BOOT_BASE, PALMAS_WDT_BOOT_REGCHK,
			&bootinfo.chk);
	if (ret < 0) {
		printk(KERN_ERR, "BOOT_REGCHK read failed, err = %d\n", ret);
		return ret;
	}

	/* Checks if we need to update the boot step */
	if (step != PALMAS_WDT_BOOT_STEP_DONOTHING) {
		ret = palmas_read(palmas, PALMAS_WDT_BOOT_BASE, PALMAS_WDT_BOOT_REGSTEP,
				&bootinfo.step);
		if (ret < 0) {
			printk(KERN_ERR, "BOOT_REGSTEP read failed, err = %d\n", ret);
			return ret;
		}
		bootinfo.chk = (unsigned char)((unsigned char)(bootinfo.chk) - ((unsigned char)(step) - (unsigned char)(bootinfo.step)));
		bootinfo.step = step;

		ret = palmas_write(palmas, PALMAS_WDT_BOOT_BASE, PALMAS_WDT_BOOT_REGSTEP,
				bootinfo.step);
		if (ret < 0) {
			printk(KERN_ERR, "BOOT_REGSTEP write failed, err = %d\n", ret);
			return ret;
		}
	}

	/* Checks if we need to update the boot count */
	if (count) {
		if (bootcount > 255)
			bootcount = 255;
		ret = palmas_read(palmas, PALMAS_WDT_BOOT_BASE, PALMAS_WDT_BOOT_REGCOUNT,
				&bootinfo.count);
		if (ret < 0) {
			printk(KERN_ERR, "BOOT_REGCOUNT read failed, err = %d\n", ret);
			return ret;
		}
		bootinfo.chk = (unsigned char)((unsigned char)(bootinfo.chk) - ((unsigned char)(bootcount) - (unsigned char)(bootinfo.count)));
		bootinfo.count = bootcount;

		ret = palmas_write(palmas, PALMAS_WDT_BOOT_BASE, PALMAS_WDT_BOOT_REGCOUNT,
				bootinfo.count);
		if (ret < 0) {
			printk(KERN_ERR, "BOOT_REGCOUNT write failed, err = %d\n", ret);
			return ret;
		}
	}

	/* update the checksum */
	ret = palmas_write(palmas, PALMAS_WDT_BOOT_BASE, PALMAS_WDT_BOOT_REGCHK,
			bootinfo.chk);
	if (ret < 0) {
		printk(KERN_ERR, "BOOT_REGCHK write failed, err = %d\n", ret);
		return ret;
	}
	return 0;
}

static int palmas_wdt_start(struct watchdog_device *wdt)
{
	int ret;
	struct palmas_wdt_data *palmas_wdt_data;
	struct palmas *palmas;
	unsigned char val, pval, tryloop;

	/*printk(KERN_ERR "palmas_wdt: palmas_wdt_start\n");*/

	palmas_wdt_data = watchdog_get_drvdata(wdt);
        if (palmas_wdt_data == NULL) {
		return -ENODEV;
	}

	palmas = palmas_wdt_data->palmas;

        if (palmas == NULL) {
		return -ENODEV;
	}

	/* watchdog was ticked, allow it to be cleared when service is needed */
	tasklet_disable(palmas_wdt_data->palmas_wdt_tasklet);
	if (!palmas_wdt_data->allowtoclear) {
		palmas_wdt_data->allowtoclear = 1;

		/* Stop the timer if it is running now */
		del_timer_sync(&palmas_wdt_data->alarm_clock);
		writel_relaxed((1 << palmas_wdt_data->palmas->irq_bit),(palmas_wdt_data->bankaddr + PALMAS_WDT_GPIO_IRQSTATUS_SET_0));
        }
	tasklet_enable(palmas_wdt_data->palmas_wdt_tasklet);

        /* 1st ticking, update the boot status correctly */
        if (!palmas_wdt_data->alive) {
		/* update the boot info */
		ret = palmas_update_boot_info(palmas, PALMAS_WDT_BOOT_STEP_KERNELWDT_TICK, 0, 0);
		if (ret < 0) {
			pr_err("watchdog%d: update boot info at tick failed, err = %d\n", wdt->id, ret);
			return ret;
		}

		palmas_wdt_data->alive = 1;
        }

        /* Gives us PALMAS_WDT_PGM_LOOP chances to set up the WDT */
        pval = (unsigned char)((fls(wdt->timeout)-1) | PALMAS_WATCHDOG_ENABLE);
        for (tryloop = 0, ret = -EIO; tryloop < PALMAS_WDT_PGM_LOOP && ret != 0 ; tryloop++) {
		/* Program the wdt value */
		ret = palmas_wdt_write(wdt, pval);
		if (ret == 0) {
			/* Read back the wdt programmed value */
			ret = palmas_wdt_read(wdt, &val);
			if (ret == 0) {
				if (val != pval) {
					ret = -EIO;
				}
			}
		}
	}

        if (ret < 0)
		pr_err("watchdog%d: palmas_wdt_start error setting wdt value, err = %d\n", wdt->id, ret);

        return(ret);
}

static int palmas_wdt_stop(struct watchdog_device *wdt)
{
	int ret;
	struct palmas_wdt_data *palmas_wdt_data;
	unsigned char val, pval, tryloop;

	/*printk(KERN_ERR "palmas_wdt: palmas_wdt_stop\n");*/

	palmas_wdt_data = watchdog_get_drvdata(wdt);
        if (palmas_wdt_data == NULL) {
		return -ENODEV;
	}

	/* Even if watchdog core is looking for nowayout before calling here, this is an additionnal */
	/* protection to avoid inadvertly disabling the watchdog if nowayout is set */
	if (!nowayout)
	{
		/* Gives us PALMAS_WDT_PGM_LOOP chances to disable the WDT */
		pval = (unsigned char)((fls(wdt->timeout)-1));
		for (tryloop = 0, ret = -EIO; tryloop < PALMAS_WDT_PGM_LOOP && ret != 0 ; tryloop++) {
			/* Program the wdt value */
			ret = palmas_wdt_write(wdt, pval);
			if (ret == 0) {
				/* Read back the wdt programmed value */
				ret = palmas_wdt_read(wdt, &val);
				if (ret == 0) {
					if (val != pval) {
						ret = -EIO;
					}
				}
			}
		}

		/* watchdog was stopped, allow it to be cleared when service is needed */
		tasklet_disable(palmas_wdt_data->palmas_wdt_tasklet);
		if (!palmas_wdt_data->allowtoclear) {
			palmas_wdt_data->allowtoclear = 1;

			/* Stop the timer if it is running now */
			del_timer_sync(&palmas_wdt_data->alarm_clock);
		}
		tasklet_enable(palmas_wdt_data->palmas_wdt_tasklet);

		if (ret < 0)
			pr_err("watchdog%d: palmas_wdt_stop error setting wdt value, err = %d\n", wdt->id, ret);
	}
	else {
		/* Get here by mistake, not allowed to stop because of nowayout set */
		ret = -EPERM;
	}

	return(ret);
}

static int palmas_wdt_set_timeout(struct watchdog_device *wdt,
				   unsigned int timeout)
{
	int ret = 0;

	printk(KERN_INFO "palmas_wdt: palmas_wdt_set_timeout (%d)\n", timeout );
	wdt->timeout = timeout;

	/* If wdt already active, reprograms it with new value */
	if (watchdog_active(wdt))
		ret = palmas_wdt_start(wdt);

	return(ret);
}

static int palmas_wdt_ping(struct watchdog_device *wdt)
{
	int ret = 0;
	struct palmas_wdt_data *palmas_wdt_data;
	struct palmas *palmas;

	/*printk(KERN_ERR "palmas_wdt: palmas_wdt_ping\n");*/

	palmas_wdt_data = watchdog_get_drvdata(wdt);
        if (palmas_wdt_data == NULL) {
		return -ENODEV;
	}

	palmas = palmas_wdt_data->palmas;

        if (palmas == NULL) {
		return -ENODEV;
	}

	/* watchdog was ticked, allow it to be cleared when service is needed */
	tasklet_disable(palmas_wdt_data->palmas_wdt_tasklet);
	if (!palmas_wdt_data->allowtoclear) {
		palmas_wdt_data->allowtoclear = 1;

		/* Stop the timer if it is running now */
		del_timer_sync(&palmas_wdt_data->alarm_clock);
		writel_relaxed((1 << palmas_wdt_data->palmas->irq_bit),(palmas_wdt_data->bankaddr + PALMAS_WDT_GPIO_IRQSTATUS_SET_0));
        }
	tasklet_enable(palmas_wdt_data->palmas_wdt_tasklet);

        /* 1st ticking, update the boot status correctly */
        if (!palmas_wdt_data->alive) {
		/* update the boot info */
		ret = palmas_update_boot_info(palmas, PALMAS_WDT_BOOT_STEP_KERNELWDT_TICK, 0, 0);
		if (ret < 0) {
			pr_err("watchdog%d: update boot info at tick failed, err = %d\n", wdt->id, ret);
			return ret;
		}

		palmas_wdt_data->alive = 1;
        }

        return(ret);
}

static const struct watchdog_info palmas_wdt_info = {
	.options = WDIOF_SETTIMEOUT | WDIOF_MAGICCLOSE | WDIOF_KEEPALIVEPING,
	.identity = "Palmas Watchdog",
};

static const struct watchdog_ops palmas_wdt_ops = {
	.owner		= THIS_MODULE,
	.start		= palmas_wdt_start,
	.stop		= palmas_wdt_stop,
	.set_timeout	= palmas_wdt_set_timeout,
	.ping		= palmas_wdt_ping,
};

static int palmas_clear_interrupts(struct watchdog_device *wdt)
{
	int ret;
	struct palmas_wdt_data *palmas_wdt_data;
	struct palmas *palmas;

	palmas_wdt_data = watchdog_get_drvdata(wdt);
        if (palmas_wdt_data == NULL) {
		return -ENODEV;
	}

	palmas = palmas_wdt_data->palmas;

        if (palmas == NULL) {
		return -ENODEV;
	}

	ret = palmas_write(palmas, PALMAS_INTERRUPT_BASE, PALMAS_INT2_STATUS,
			PALMAS_INT2_STATUS_WDT);
	/*printk(KERN_ERR "palmas_wdt: Clear irq (ret:%d)\n", ret );*/
	if (ret < 0) {
		pr_err("watchdog%d: RTC_STATUS write failed, err = %d\n", wdt->id, ret);
		return ret;
	}
	return 0;
}

static irqreturn_t palmas_wdt_interrupt(int irq, void *context)
{
	struct watchdog_device *wdt = context;
	int ret;
	struct palmas_wdt_data *palmas_wdt_data;

	palmas_wdt_data = watchdog_get_drvdata(wdt);
        if (palmas_wdt_data == NULL) {
		pr_err("watchdog%d: WDT interrupt invalid wdt data, err = %d\n", wdt->id, 0);
		return IRQ_NONE;
	}

	/* Allowed to clear the watchdog condition if only watchdog has been ticked by user app */
	if (palmas_wdt_data->allowtoclear) {

		/* Clear pending interrupts */
                /* Palmas driver has been changed from default clear on read and need explicit irq clear in every palmas driver */
		ret = palmas_clear_interrupts(wdt);
		if (ret < 0) {
			pr_err("watchdog%d: WDT interrupt clear failed, err = %d\n", wdt->id, ret);
			return IRQ_NONE;
		}
		else {
			palmas_wdt_data->allowtoclear = 0;
		}
	}
	else {
		/* Clear pending interrupts */
                /* Palmas driver has been changed from default clear on read and need explicit irq clear in every palmas driver */
		ret = palmas_clear_interrupts(wdt);
		if (ret < 0) {
			pr_err("watchdog%d: WDT interrupt clear failed, err = %d\n", wdt->id, ret);
			return IRQ_NONE;
		}
		/* Schedule to disable wdt irq servicing if no wdt is ticked anymore */
		tasklet_hi_schedule(palmas_wdt_data->palmas_wdt_tasklet);
	}
	return IRQ_HANDLED;
}

/* calculates the boot info 255 based checksum */
static int palmas_calculate_boot_chk(struct palmas_wdt_boot *bootinfo)
{
    return((255 - (u8)((u8)bootinfo->id1 + (u8)bootinfo->id2 + (u8)bootinfo->count + (u8)bootinfo->step + (u8)bootinfo->stepold + (u8)bootinfo->event + (u8)bootinfo->ubootinst)));
}

/* validate boot info */
static enum palmas_wdt_boot_valid palmas_validate_boot_info(struct palmas_wdt_boot *bootinfo)
{
	/* Checks if id1 is ok? */
	if (bootinfo->id1 != PALMAS_WDT_BOOT_ID1 ) {
		return(PALMAS_WDT_BOOT_VALID_ID1);
	}

	/* Checks if id2 is ok? */
	if (bootinfo->id2 != PALMAS_WDT_BOOT_ID2 ) {
		return(PALMAS_WDT_BOOT_VALID_ID2);
	}

	/* Checks if boot step seems ok? */
	if ((bootinfo->step & 0xf) !=  (~((bootinfo->step & 0xf0) >> 4) & 0xf) || (bootinfo->step & 0xf) > (PALMAS_WDT_BOOT_STEP_KERNELWDT_LAST & 0xf)) {
		return(PALMAS_WDT_BOOT_VALID_STEP);
	}

	/* Checks if old boot step seems ok? */
	if ((bootinfo->stepold & 0xf) !=  (~((bootinfo->stepold & 0xf0) >> 4) & 0xf) || (bootinfo->stepold & 0xf) > (PALMAS_WDT_BOOT_STEP_KERNELWDT_LAST & 0xf)) {
		return(PALMAS_WDT_BOOT_VALID_STEPOLD);
	}

	/* Checks if reset event seems ok? */
	if (!(bootinfo->event == PALMAS_WDT_BOOT_EVENT_POWERON || bootinfo->event == PALMAS_WDT_BOOT_EVENT_GPADC_SHUTDOWN || bootinfo->event == PALMAS_WDT_BOOT_EVENT_VSYS_LO ||
	bootinfo->event == PALMAS_WDT_BOOT_EVENT_SW_RST || bootinfo->event == PALMAS_WDT_BOOT_EVENT_RESET_IN || bootinfo->event == PALMAS_WDT_BOOT_EVENT_TSHUT || 
	bootinfo->event == PALMAS_WDT_BOOT_EVENT_WDT || bootinfo->event == PALMAS_WDT_BOOT_EVENT_POWERDOWN || bootinfo->event == PALMAS_WDT_BOOT_EVENT_PWRON_LPK || 
	bootinfo->event == PALMAS_WDT_BOOT_EVENT_SOCRST || bootinfo->event == PALMAS_WDT_BOOT_EVENT_POWERON_SOCRST || bootinfo->event == PALMAS_WDT_BOOT_EVENT_UNKNOWN)) {
		return(PALMAS_WDT_BOOT_VALID_EVENT);
	}

	/* Checks if u-boot instance seems ok? */
	if ((bootinfo->ubootinst != 0) && (bootinfo->ubootinst != 1)) {
		return(PALMAS_WDT_BOOT_VALID_UINST);
	}

	/* Checks if checksum is ok? */
	if (bootinfo->chk != palmas_calculate_boot_chk(bootinfo) ) {
		return(PALMAS_WDT_BOOT_VALID_CHK);
	}

	return(PALMAS_WDT_BOOT_VALID_OK);
}

static int palmas_wdt_get_bootinfo(struct palmas *palmas, struct palmas_wdt_boot *bootinfo)
{
	int ret = 0, i;

	for (i=0; i<8; i++) {
		ret = palmas_read(palmas, PALMAS_WDT_BOOT_BASE,PALMAS_WDT_BOOT_REGID1 + i, (unsigned *)(&bootinfo->id1) + i);
		if (ret < 0) {
			break;
		}
	}
	return(ret);
}

/* This function read the original bootcount value saved at driver installation (is not affected by store_bcount) */
static ssize_t show_bcount(struct device* dev, struct device_attribute* attr, char* buf)
{
	struct watchdog_device *wdt;
	struct palmas_wdt_data *palmas_wdt_data;

	wdt = dev_get_drvdata(dev);
        if (wdt == NULL) {
                return -ENODEV;
        }
	palmas_wdt_data = watchdog_get_drvdata(wdt);
        if (palmas_wdt_data == NULL) {
                return -ENODEV;
        }

        return sprintf(buf, "%d\n", palmas_wdt_data->bootcount_orig);
}

/* This function allows to modify the current bootcount value (only 254 is currently allowed) */
static ssize_t store_bcount(struct device* dev, struct device_attribute* attr, const char* buf, size_t count)
{
        struct palmas *palmas;
        int err, new_bootcount;

        sscanf(buf, "%du", &new_bootcount);
        /* Bootcount modification is limited to force boot alternate only */
        if (new_bootcount != 254) {
                return -EINVAL;
        }

        /* printk(KERN_DEBUG "Bootcount: %d\n", new_bootcount); */

        palmas = dev_get_drvdata(dev->parent);
        if (palmas == NULL) {
                return -ENODEV;
        }

	/* update the bootcount info */
	err = palmas_update_boot_info(palmas, PALMAS_WDT_BOOT_STEP_KERNELWDT_BOOT, 1, new_bootcount);
        if (err < 0) {
                dev_err(dev, "palmas_wdt: failed to update bootcount, err = %d\n", err);
                return err;
        }

        /* printk(KERN_DEBUG "Successfully wrote new bootcount\n"); */

        return count;
}

static DEVICE_ATTR(bootcount, S_IWUSR | S_IRUGO, show_bcount, store_bcount);

/* This function read the current hardware watchdog state to see if it is active or not (returns in variable 1 active, 0 inactive) */
/* being active also means the associated wdt irq is also enable */
static ssize_t show_wdt_hw_state(struct device* dev, struct device_attribute* attr, char* buf)
{
	struct watchdog_device *wdt;
	unsigned char val;
	unsigned rval = 0;
	int ret, tryloop, count;

	wdt = dev_get_drvdata(dev);
        if (wdt == NULL) {
                return -ENODEV;
        }

        /* We need to sucessfully read the wdt enable state (with its irq enabled) at least (PALMAS_WDT_RD_LOOP - 1) times to consider it really enable */
        /* this is a protection in case of reading an undetected error from i2c bus, we prefer to report it not enable than opposite */
        for (tryloop = 0, count = 0, ret = -EIO; tryloop < PALMAS_WDT_RD_LOOP && ret != 0 ; tryloop++) {
		/* Attempt to read back the wdt programmed value */
		ret = palmas_wdt_read(wdt, &val);
		if (ret == 0) {
			if (val & PALMAS_WATCHDOG_ENABLE) {
				/* Attempt to read back wdt irq enable state */
				ret = palmas_wdtirq_read(wdt, &val);
				if (ret == 0) {
					/* wdt irq enable? */
					if (val) {
						count++;
					}
					if (count != 0 && count >= (PALMAS_WDT_RD_LOOP - 1)) {
						rval = 1;
					}
					else {
						ret = -EIO;
					}
				}
			}
			else
				ret = -EIO;
		}
	}

        return sprintf(buf, "%d\n", rval);
}

/* This function is dummy (wdt hw state could not be changed here) */
static ssize_t store_wdt_hw_state(struct device* dev, struct device_attribute* attr, const char* buf, size_t count)
{
        return count;
}

static DEVICE_ATTR(wdt_hw_state, S_IWUSR | S_IRUGO, show_wdt_hw_state, store_wdt_hw_state);

static int palmas_wdt_probe(struct platform_device *pdev)
{
	int irq;
	int ret = 0;
	struct palmas *palmas;
	struct watchdog_device *wdt;
	struct palmas_wdt_data *palmas_wdt_data;
	struct tasklet_struct *palmas_wdt_tasklet;
	struct palmas_wdt_boot bootinfo;

	/*printk(KERN_ERR "palmas_wdt: palmas_wdt_probe start\n");*/

	palmas = dev_get_drvdata(pdev->dev.parent);
 	if (palmas == NULL) {
		return -ENODEV;
	}

	wdt = devm_kzalloc(&pdev->dev, sizeof(*wdt), GFP_KERNEL);
	if (wdt == NULL)
		return -ENOMEM;

	palmas_wdt_data = devm_kzalloc(&pdev->dev, sizeof(*palmas_wdt_data), GFP_KERNEL);
	if (palmas_wdt_data == NULL)
		return -ENOMEM;

	palmas_wdt_tasklet = devm_kzalloc(&pdev->dev, sizeof(*palmas_wdt_tasklet), GFP_KERNEL);
	if (palmas_wdt_tasklet == NULL)
		return -ENOMEM;

	palmas_wdt_data->palmas = palmas;
	palmas_wdt_data->palmas_wdt_tasklet = palmas_wdt_tasklet;
	palmas_wdt_data->allowtoclear = 0;
	palmas_wdt_data->alive = 0;
	palmas_wdt_data->timershutdown = 0;
	palmas_wdt_data->bootcount_orig = 0;
	init_timer(&palmas_wdt_data->alarm_clock);

        if (palmas->irq_bit >= 0 && palmas->irq_bit < 32 && palmas->irq_based != -1) {
	palmas_wdt_data->bankaddr = ioremap(palmas->irq_based, PALMAS_WDT_GPIO_BANK_SIZE);
        }
        else {
		return(-ENXIO);
        }

	tasklet_init(palmas_wdt_tasklet, palmas_wdt_tasklet_func, (unsigned long)(wdt));

	watchdog_set_drvdata(wdt, palmas_wdt_data);

	wdt->info		= &palmas_wdt_info;
	wdt->ops		= &palmas_wdt_ops;
	wdt->status		= 0;
	wdt->bootstatus		= 0;
	wdt->timeout		= 128;
	/* set it the min to 2 to make it compatible with the timer alarm set for 1 sec */
	wdt->min_timeout	= 2;
	wdt->max_timeout	= 128;
	wdt->parent		= &pdev->dev;

        ret = device_create_file(&pdev->dev, &dev_attr_bootcount);
        if (ret < 0) {
            pr_warn("watchdog%d: palmas_wdt: failed to create bootcount sysfs entry\n", wdt->id);
        }

        ret = device_create_file(&pdev->dev, &dev_attr_wdt_hw_state);
        if (ret < 0) {
            pr_warn("watchdog%d: palmas_wdt: failed to create wdt_hw_state sysfs entry\n", wdt->id);
        }

	/* get boot info */
	ret = palmas_wdt_get_bootinfo(palmas, &bootinfo);
	if (ret < 0) {
		pr_err("watchdog%d: palmas_wdt: get boot info failed, err = %d\n", wdt->id, ret);
		return(-ENXIO);
	}

	/* Checks the boot info if it is valid */
	ret = palmas_validate_boot_info(&bootinfo);

	if (ret == PALMAS_WDT_BOOT_VALID_OK) {
		/* analyze current boot event */
		switch(bootinfo.event) {
			case PALMAS_WDT_BOOT_EVENT_GPADC_SHUTDOWN:
			case PALMAS_WDT_BOOT_EVENT_VSYS_LO:
			case PALMAS_WDT_BOOT_EVENT_SW_RST:
			case PALMAS_WDT_BOOT_EVENT_RESET_IN:
			case PALMAS_WDT_BOOT_EVENT_TSHUT:
			case PALMAS_WDT_BOOT_EVENT_POWERDOWN:
			case PALMAS_WDT_BOOT_EVENT_PWRON_LPK:
				wdt->bootstatus = ((bootinfo.event << 16) & 0x00ff0000);
				break;
			case PALMAS_WDT_BOOT_EVENT_WDT:
				wdt->bootstatus = WDIOF_CARDRESET;
				break;
			case PALMAS_WDT_BOOT_EVENT_POWERON:
				wdt->bootstatus = WDIOF_PWONRESET;
				break;
			case PALMAS_WDT_BOOT_EVENT_POWERON_SOCRST:
				wdt->bootstatus = WDIOF_PWONRESET;
			case PALMAS_WDT_BOOT_EVENT_SOCRST:
				wdt->bootstatus |= WDIOF_SOCRESET;
				break;
			case PALMAS_WDT_BOOT_EVENT_UNKNOWN:
				wdt->bootstatus = WDIOF_UNKNOWN;
				break;
			default:
				/* we should not get here, this means we could have more than one event! */
				break;
		}
		/* Add which u-boot spi instance who booted the kernel (only valid when booted through spi) */
		if (bootinfo.ubootinst)
			wdt->bootstatus |= WDIOF_UBOOTINST;

		/* Add old boot step information */
		wdt->bootstatus |= (((unsigned)(bootinfo.stepold) << 24) & WDIOF_BOOTSTEP);
	}
	else {
		pr_err("watchdog%d: palmas_wdt: invalid boot info, err = %d\n", wdt->id, ret);
		bootinfo.count = 0;
	}

	printk(KERN_INFO "palmas_wdt: bootstatus = 0x%08X\n", wdt->bootstatus);
	palmas_wdt_data->bootcount_orig = bootinfo.count;
	printk(KERN_INFO "palmas_wdt: boot count = %d\n", bootinfo.count);

	watchdog_set_nowayout(wdt, nowayout);
	printk(KERN_INFO "palmas_wdt: watchdog_set_nowayout (%d)\n", nowayout);
	platform_set_drvdata(pdev, wdt);

	/*palmas_wdt_stop(wdt);*/

	/* Clear pending interrupts */
	/*
	ret = palmas_clear_interrupts(wdt);
	if (ret < 0) {
		pr_err("watchdog%d: clear WDT int failed, err = %d\n", wdt->id, ret);
		return ret;
	}
	*/

	ret = watchdog_register_device(wdt);
	if (ret)
		return ret;

	irq = platform_get_irq(pdev, 0);

	ret = devm_request_threaded_irq(&pdev->dev, irq, NULL,
			palmas_wdt_interrupt,
			IRQF_TRIGGER_LOW | IRQF_ONESHOT |
			IRQF_EARLY_RESUME,
			dev_name(&pdev->dev), wdt);


	if (ret < 0) {
		pr_err("watchdog%d: IRQ request failed, err = %d\n", wdt->id, ret);
		return ret;
	}

	/* update the driver install boot step info */
        palmas_update_boot_info(palmas, PALMAS_WDT_BOOT_STEP_KERNELWDT_INST, 0, 0);
	if (ret < 0) {
		pr_err("watchdog%d: update boot step info failed, err = %d\n", wdt->id, ret);
		return ret;
	}

	/*printk(KERN_ERR "palmas_wdt: palmas_wdt_probe end\n");*/
	return 0;
}

static int palmas_wdt_remove(struct platform_device *pdev)
{
	struct watchdog_device *wdt = platform_get_drvdata(pdev);
	struct palmas_wdt_data *palmas_wdt_data;

	palmas_wdt_data = watchdog_get_drvdata(wdt);

        device_remove_file(&pdev->dev, &dev_attr_wdt_hw_state);
        device_remove_file(&pdev->dev, &dev_attr_bootcount);

        /* Stop the tasklet before we exit */
	palmas_wdt_data->timershutdown = 1;
	tasklet_disable(palmas_wdt_data->palmas_wdt_tasklet);
	palmas_wdt_data->allowtoclear = 1;
        tasklet_kill(palmas_wdt_data->palmas_wdt_tasklet);

	/* Stop the timer before we exit */
	del_timer_sync(&palmas_wdt_data->alarm_clock);

        iounmap(palmas_wdt_data->bankaddr);

        /*printk(KERN_ERR "palmas_wdt: palmas_wdt_remove start\n");*/
	watchdog_unregister_device(wdt);
	/*printk(KERN_ERR "palmas_wdt: palmas_wdt_remove end\n");*/

	return 0;
}



#ifdef CONFIG_PM
static int palmas_wdt_suspend(struct platform_device *pdev, pm_message_t state)
{
	struct watchdog_device *wdt = platform_get_drvdata(pdev);
	if (watchdog_active(wdt))
		return palmas_wdt_stop(wdt);

	return 0;
}

static int palmas_wdt_resume(struct platform_device *pdev)
{
	struct watchdog_device *wdt = platform_get_drvdata(pdev);
	if (watchdog_active(wdt))
		return palmas_wdt_start(wdt);

	return 0;
}
#else
#define palmas_wdt_suspend        NULL
#define palmas_wdt_resume         NULL
#endif

static const struct of_device_id twl_wdt_of_match[] = {
	{ .compatible = "ti,palmas-wdt", },
	{ },
};
MODULE_DEVICE_TABLE(of, twl_wdt_of_match);

static struct platform_driver palmas_wdt_driver = {
	.probe		= palmas_wdt_probe,
	.remove		= palmas_wdt_remove,
	.suspend	= palmas_wdt_suspend,
	.resume		= palmas_wdt_resume,
	.driver		= {
		.owner		= THIS_MODULE,
		.name		= "palmas_wdt",
		.of_match_table	= twl_wdt_of_match,
	},
};

module_platform_driver(palmas_wdt_driver);

MODULE_AUTHOR("NURAN Wireless");
MODULE_LICENSE("GPL");
MODULE_ALIAS("platform:palmas_wdt");

