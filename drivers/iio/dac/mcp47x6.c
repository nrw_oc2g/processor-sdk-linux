/*
 * mcp47x6.c - Support for Microchip MCP4706/4716/4726
 *
 * Copyright (C) 2015 Nuran Wireless <support@nuranwireless.com>
 *
 * Based on mcp4725 by Peter Meerwald <pmeerw@pmeerw.net>
 *
 * This file is subject to the terms and conditions of version 2 of
 * the GNU General Public License.  See the file COPYING in the main
 * directory of this archive for more details.
 *
 * driver for the Microchip I2C 8-bit, 10-bit, and 12-bit 
 * digital-to-analog converter (DAC) 
 * (7-bit I2C slave address 0x60, the three LSBs can be configured in
 * hardware)
 */

#include <linux/module.h>
#include <linux/init.h>
#include <linux/of.h>
#include <linux/i2c.h>
#include <linux/err.h>
#include <linux/delay.h>

#include <linux/iio/iio.h>
#include <linux/iio/sysfs.h>

#include <linux/iio/dac/mcp47x6.h>

#define MCP47X6_DRV_NAME "mcp47x6"

enum mcp47x6_supported_device_ids {
	ID_MCP4706,
	ID_MCP4716,
	ID_MCP4726,
};
struct mcp47x6_data {
	struct i2c_client *client;
	u16 model;
	u32 vrl;
	u32 gain;
	u32 vref_mv;
	u16 dac_value;
	bool powerdown;
	unsigned powerdown_mode;
};

static int mcp47x6_suspend(struct device *dev)
{
	struct mcp47x6_data *data = iio_priv(i2c_get_clientdata(
		to_i2c_client(dev)));
	u8 outbuf[2];

	outbuf[0] = (data->powerdown_mode + 1) << 4;
	outbuf[1] = 0;
	data->powerdown = true;

	return i2c_master_send(data->client, outbuf, 2);
}

static int mcp47x6_resume(struct device *dev)
{
	struct mcp47x6_data *data = iio_priv(i2c_get_clientdata(
		to_i2c_client(dev)));
	u8 outbuf[2];

	/* restore previous DAC value */
	switch (data->model) {
	case ID_MCP4706 :
		outbuf[0] = 0x00;
		outbuf[1] = data->dac_value & 0xff;
		break;
	case ID_MCP4716 :
		outbuf[0] = (data->dac_value >> 6) & 0xf;
		outbuf[1] = (data->dac_value << 2) & 0xff;
		break;
	case ID_MCP4726 :
		outbuf[0] = (data->dac_value >> 8) & 0xf;
		outbuf[1] = data->dac_value & 0xff;
		break;
	}
	data->powerdown = false;

	return i2c_master_send(data->client, outbuf, 2);
}

#ifdef CONFIG_PM_SLEEP
static SIMPLE_DEV_PM_OPS(mcp47x6_pm_ops, mcp47x6_suspend, mcp47x6_resume);
#define MCP47X6_PM_OPS (&mcp47x6_pm_ops)
#else
#define MCP47X6_PM_OPS NULL
#endif

static ssize_t mcp47x6_store_eeprom(struct device *dev,
	struct device_attribute *attr, const char *buf, size_t len)
{
	struct iio_dev *indio_dev = dev_to_iio_dev(dev);
	struct mcp47x6_data *data = iio_priv(indio_dev);
	int tries = 20;
	u8 inoutbuf[3];
	bool state;
	int ret;

	ret = strtobool(buf, &state);
	if (ret < 0)
		return ret;

	if (!state)
		return 0;


	inoutbuf[0] = 0x60 		/* write EEPROM */
	            | (data->vrl << 3)
	            | data->gain;

	switch (data->model) {
	case ID_MCP4706 :
		inoutbuf[1] = data->dac_value;
		break;
	case ID_MCP4716 :
		inoutbuf[1] = data->dac_value >> 2;
		inoutbuf[2] = (data->dac_value & 0x3) << 6;
		break;
	case ID_MCP4726 :
		inoutbuf[1] = data->dac_value >> 4;
		inoutbuf[2] = (data->dac_value & 0xf) << 4;
		break;
	}

	ret = i2c_master_send(data->client, inoutbuf, 3);
	if (ret < 0)
		return ret;
	else if (ret != 3)
		return -EIO;

	/* wait for write complete, takes up to 50ms */
	while (tries--) {
		msleep(20);
		ret = i2c_master_recv(data->client, inoutbuf, 3);
		if (ret < 0)
			return ret;
		else if (ret != 3)
			return -EIO;

		if (inoutbuf[0] & 0x80)
			break;
	}

	if (tries < 0) {
		dev_err(&data->client->dev,
			"mcp47x6_store_eeprom() failed, incomplete\n");
		return -EIO;
	}

	return len;
}

static IIO_DEVICE_ATTR(store_eeprom, S_IWUSR, NULL, mcp47x6_store_eeprom, 0);

static struct attribute *mcp47x6_attributes[] = {
	&iio_dev_attr_store_eeprom.dev_attr.attr,
	NULL,
};

static const struct attribute_group mcp47x6_attribute_group = {
	.attrs = mcp47x6_attributes,
};

static const char * const mcp47x6_powerdown_modes[] = {
	"1kohm_to_gnd",
	"100kohm_to_gnd",
	"500kohm_to_gnd"
};

static int mcp47x6_get_powerdown_mode(struct iio_dev *indio_dev,
	const struct iio_chan_spec *chan)
{
	struct mcp47x6_data *data = iio_priv(indio_dev);

	return data->powerdown_mode;
}

static int mcp47x6_set_powerdown_mode(struct iio_dev *indio_dev,
	const struct iio_chan_spec *chan, unsigned mode)
{
	struct mcp47x6_data *data = iio_priv(indio_dev);

	data->powerdown_mode = mode;

	return 0;
}

static ssize_t mcp47x6_read_powerdown(struct iio_dev *indio_dev,
	uintptr_t private, const struct iio_chan_spec *chan, char *buf)
{
	struct mcp47x6_data *data = iio_priv(indio_dev);

	return sprintf(buf, "%d\n", data->powerdown);
}

static ssize_t mcp47x6_write_powerdown(struct iio_dev *indio_dev,
	 uintptr_t private, const struct iio_chan_spec *chan,
	 const char *buf, size_t len)
{
	struct mcp47x6_data *data = iio_priv(indio_dev);
	bool state;
	int ret;

	ret = strtobool(buf, &state);
	if (ret)
		return ret;

	if (state)
		ret = mcp47x6_suspend(&data->client->dev);
	else
		ret = mcp47x6_resume(&data->client->dev);
	if (ret < 0)
		return ret;

	return len;
}

static const struct iio_enum mcp47x6_powerdown_mode_enum = {
	.items = mcp47x6_powerdown_modes,
	.num_items = ARRAY_SIZE(mcp47x6_powerdown_modes),
	.get = mcp47x6_get_powerdown_mode,
	.set = mcp47x6_set_powerdown_mode,
};

static const struct iio_chan_spec_ext_info mcp47x6_ext_info[] = {
	{
		.name = "powerdown",
		.read = mcp47x6_read_powerdown,
		.write = mcp47x6_write_powerdown,
		.shared = IIO_SEPARATE,
	},
	IIO_ENUM("powerdown_mode", IIO_SEPARATE, &mcp47x6_powerdown_mode_enum),
	IIO_ENUM_AVAILABLE("powerdown_mode", &mcp47x6_powerdown_mode_enum),
	{ },
};

static const struct iio_chan_spec mcp47x6_channel = {
	.type		= IIO_VOLTAGE,
	.indexed	= 1,
	.output		= 1,
	.channel	= 0,
	.info_mask_separate = BIT(IIO_CHAN_INFO_RAW),
	.info_mask_shared_by_type = BIT(IIO_CHAN_INFO_SCALE),
	.ext_info	= mcp47x6_ext_info,
};

static int mcp47x6_set_value(struct iio_dev *indio_dev, int val)
{
	struct mcp47x6_data *data = iio_priv(indio_dev);
	u8 outbuf[3];
	int ret;

	outbuf[0] = 0x40 		/* write volatile memory */
	          | (data->vrl << 3)
	          | (data->powerdown ? (data->powerdown_mode+1) : 0)
	          | data->gain;

	switch (data->model) {
	case ID_MCP4706 :
		if (val >= (1 << 8) || val < 0)
			return -EINVAL;
		outbuf[1] = val;
		break;
	case ID_MCP4716 :
		if (val >= (1 << 10) || val < 0)
			return -EINVAL;
		outbuf[1] = val >> 2;
		outbuf[2] = (val & 0x3) << 6;
		break;
	case ID_MCP4726 :
		if (val >= (1 << 12) || val < 0)
			return -EINVAL;
		outbuf[1] = val >> 4;
		outbuf[2] = (val & 0xf) << 4;
		break;
	}

	ret = i2c_master_send(data->client, outbuf, 3);
	if (ret < 0)
		return ret;
	else if (ret != 3)
		return -EIO;
	else
		return 0;
}

static int mcp47x6_get_value(struct iio_dev *indio_dev, int *val)
{
	struct mcp47x6_data *data = iio_priv(indio_dev);
	u8 inbuf[3];
	int err;

	/* read current DAC value */
	err = i2c_master_recv(data->client, inbuf, 3);
	if (err < 0) {
		dev_err(&data->client->dev, "failed to read DAC value");
		return err;
	}

	switch (data->model) {
	case ID_MCP4706 :
		*val = inbuf[1];
		break;
	case ID_MCP4716 :
		*val = (inbuf[1] << 2) | (inbuf[2] >> 6);
		break;
	case ID_MCP4726 :
		*val = (inbuf[1] << 4) | (inbuf[2] >> 4);
		break;
	default:
		dev_err(&data->client->dev, "Invalid DAC model\n" );
		return -EINVAL;
	}
	return 0;
}

static int mcp47x6_read_raw(struct iio_dev *indio_dev,
			   struct iio_chan_spec const *chan,
			   int *val, int *val2, long mask)
{
	int res;
	struct mcp47x6_data *data = iio_priv(indio_dev);

	switch (mask) {
	case IIO_CHAN_INFO_RAW:
		res = mcp47x6_get_value(indio_dev, val);
		if (res) return res;

		data->dac_value = *val;
		return IIO_VAL_INT;
	case IIO_CHAN_INFO_SCALE:
		*val = data->vref_mv;
		switch (data->model) {
		case ID_MCP4706 :
			*val2 = 8;
			break;
		case ID_MCP4716 :
			*val2 = 10;
			break;
		case ID_MCP4726 :
			*val2 = 12;
			break;
		}
		return IIO_VAL_FRACTIONAL_LOG2;
	}
	return -EINVAL;
}

static int mcp47x6_write_raw(struct iio_dev *indio_dev,
			       struct iio_chan_spec const *chan,
			       int val, int val2, long mask)
{
	struct mcp47x6_data *data = iio_priv(indio_dev);
	int ret;

	switch (mask) {
	case IIO_CHAN_INFO_RAW:
		ret = mcp47x6_set_value(indio_dev, val);
		if (!ret) data->dac_value = val;
		break;
	default:
		ret = -EINVAL;
		break;
	}

	return ret;
}

static const struct iio_info mcp47x6_info = {
	.read_raw = mcp47x6_read_raw,
	.write_raw = mcp47x6_write_raw,
	.attrs = &mcp47x6_attribute_group,
	.driver_module = THIS_MODULE,
};

static int mcp47x6_probe(struct i2c_client *client,
			 const struct i2c_device_id *id)
{
	u8 pd;
	int err;
	u32 value;
	u8 inbuf[3];
	struct mcp47x6_data *data;
	struct iio_dev *indio_dev;
	struct device_node *np = client->dev.of_node;
	struct mcp47x6_pdata *pdata = client->dev.platform_data;

	indio_dev = devm_iio_device_alloc(&client->dev, sizeof(*data));
	if (indio_dev == NULL)
		return -ENOMEM;
	data = iio_priv(indio_dev);
	i2c_set_clientdata(client, indio_dev);
	data->client = client;
	data->model = id->driver_data;

	indio_dev->dev.parent = &client->dev;
	indio_dev->info = &mcp47x6_info;
	indio_dev->channels = &mcp47x6_channel;
	indio_dev->num_channels = 1;
	indio_dev->modes = INDIO_DIRECT_MODE;

	if (pdata) {
		data->vref_mv = pdata->vref_mv;
		data->vrl     = pdata->vrl_sel;
		data->gain    = pdata->g_sel;
		data->powerdown_mode = pdata->pd_sel;
	} else if (np) {
                if (!of_property_read_u32(np, "mcp47x6-gain-sel", &value)) {
                        switch (value) {
                        case MCP47X6_G_1X :
				data->gain = value;
                                break;
                        case MCP47X6_G_2X :
				data->gain = value;
                                break;
                        default :
				dev_err(&client->dev, "Invalid gain selection found in DT\n" );
				return -EINVAL;
                        }
                } else {
			dev_err(&client->dev, "Invalid gain selection found in DT\n" );
			return -EINVAL;
                }

                if (!of_property_read_u32(np, "mcp47x6-pd-sel", &value)) {
                        switch (value) {
                        case MCP47X6_PD_1K_LOAD :
				data->powerdown_mode = value;
                                break;
                        case MCP47X6_PD_100K_LOAD :
				data->powerdown_mode = value;
                                break;
                        case MCP47X6_PD_500K_LOAD :
				data->powerdown_mode = value;
                                break;
                        default :
				dev_err(&client->dev, "Invalid PD load selection found in DT\n" );
				return -EINVAL;
                        }
                } else {
			dev_err(&client->dev, "Invalid gain selection found in DT\n" );
			return -EINVAL;
                }

                if (!of_property_read_u32(np, "mcp47x6-vrl-sel", &value)) {
                        switch (value) {
                        case MCP47X6_VRL_VDD_UNBUFFERED :
				data->vrl = value;
                                break;
                        case MCP47X6_VRL_VREF_UNBUFFERED :
				data->vrl = value;
                                break;
                        case MCP47X6_VRL_VREF_BUFFERED :
				data->vrl = value;
                                break;
                        default :
				dev_err(&client->dev, "Invalid Vrl selection found in DT\n" );
				return -EINVAL;
                        }
                } else {
			dev_err(&client->dev, "Invalid Vrl selection found in DT\n" );
			return -EINVAL;
                }

                if (!of_property_read_u32(np, "mcp47x6-vref-mv", &value)) {
                	data->vref_mv = value;
		} else {
			dev_err(&client->dev, "Invalid vref found in DT\n" );
			return -EINVAL;
		}
	} else {
		dev_err(&client->dev, "invalid platform data");
		return -EINVAL;
	}

	dev_dbg(&client->dev, "vref_mf = %d mV\n", data->vref_mv);
	dev_dbg(&client->dev, "vrl     = %d\n", data->vrl);
	dev_dbg(&client->dev, "gain    = %d\n", data->gain);
	dev_dbg(&client->dev, "pd_mode = %d\n", data->powerdown_mode);

	/* read current DAC value */
	err = i2c_master_recv(client, inbuf, 3);
	if (err < 0) {
		dev_err(&client->dev, "failed to read DAC value");
		return err;
	}
	pd = (inbuf[0] >> 1) & 0x3;
	data->powerdown = pd > 0 ? true : false;

	switch (data->model) {
	case ID_MCP4706 :
		data->dac_value = inbuf[1];
		break;
	case ID_MCP4716 :
		data->dac_value = (inbuf[1] << 2) | (inbuf[2] >> 6);
		break;
	case ID_MCP4726 :
		data->dac_value = (inbuf[1] << 4) | (inbuf[2] >> 4);
		break;
	default:
		dev_err(&client->dev, "Invalid DAC model\n" );
		return -EINVAL;
	}

	return iio_device_register(indio_dev);
}

static int mcp47x6_remove(struct i2c_client *client)
{
	iio_device_unregister(i2c_get_clientdata(client));
	return 0;
}

static const struct i2c_device_id mcp47x6_id[] = {
	{ "mcp4706", ID_MCP4706 },
	{ "mcp4716", ID_MCP4716 },
	{ "mcp4726", ID_MCP4726 },
	{ }
};
MODULE_DEVICE_TABLE(i2c, mcp47x6_id);

static struct i2c_driver mcp47x6_driver = {
	.driver = {
		.name	= MCP47X6_DRV_NAME,
		.pm	= MCP47X6_PM_OPS,
	},
	.probe		= mcp47x6_probe,
	.remove		= mcp47x6_remove,
	.id_table	= mcp47x6_id,
};
module_i2c_driver(mcp47x6_driver);

MODULE_AUTHOR("Nuran Wireless <support@nuranwireless.com>");
MODULE_DESCRIPTION("MCP47X6 8-bit/10-bit/12-bit DAC");
MODULE_LICENSE("GPL");
