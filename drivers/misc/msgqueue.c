#include <linux/slab.h>
#include <linux/version.h>
#include <linux/device.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/interrupt.h>
#include <linux/firmware.h>
#include <linux/platform_device.h>
#include <linux/miscdevice.h>
#include <linux/delay.h>
#include <linux/completion.h>
#include <linux/freezer.h>
#include <linux/uaccess.h>
#include <linux/io.h>
#include <linux/timer.h>
#include <linux/poll.h>
#include <linux/crc32.h>
#include <linux/msgqueue.h>
#include <linux/time.h>

#include <asm/mach-types.h>
#include <asm/gpio.h>
#include <asm/io.h>

#ifdef CONFIG_PROC_FS
#include <linux/proc_fs.h>
#include <linux/seq_file.h>
#include <asm/uaccess.h>
#endif /* CONFIG_PROC_FS */

#include "nrw_mbox.h"

#define MODULE_NAME 	"msgq"

static const char msgq_driver_version[] = "v2.3";

static int msgq_check_crc = 0;
module_param_named(check_crc, msgq_check_crc, int, S_IRUGO);

/* All processor differents id */
#define MyhostId   0
#define Mydsp1Id   1
#define Mydsp2Id   2
#define Myipu1_0Id 3
#define Myipu2_0Id 4
#define Myipu1_1Id 5
#define Myipu2_1Id 6

/* Get the source processor id of the queue */
#define MSGQUEUE_GET_SRCPROC(pQueue) ((((pQueue)->u8Dir) >> 4) & 0xF)

/* Get the destination processor id of the queue */
#define MSGQUEUE_GET_DSTPROC(pQueue) (((pQueue)->u8Dir) & 0xF)

struct msgq_t {
	u32	u32MagicId;
	u32	u32QueueSize;
	u32	u32MsgSize;
	u32	reserved[4];
	u32	u32Latency;
	u32	u32Head;
	u32	u32Tail;
	u8	u8Dir;           ///< Direction (SrcProcId << 4 | DstProcId),
                         ///< ex: DSP1->DSP2=0x12, DSP2->DSP1=0x21, HOST->DSP1=0x01, HOST->DSP2=0x02
	u8	u8ArmIntReq;
	u8	u8ArmIntFlag;
	u8	u8DspIntReq;
	u8	u8DspIntFlag;
	char	szName[83];
	u8	pu8Mem[0];
};

struct msgq_mdev_t {
	char devName[128];
	struct mutex rd_lock;
	struct mutex wr_lock;
	struct miscdevice dev;

	u8 timeout;
        volatile u8 shutdown;               // when != 0 means operation is paused while msgqueue is not available because of device firmware reload
        volatile u8 shuterr;                // when != 0 means we got shutdown problem, this will make sure this problem is reported at least once to the application
	struct timer_list alarm_clock;

	struct msgq_dev_t *parent;
	u32 u32BadCrcCnt;
	struct mutex lock;
	wait_queue_head_t waitq;
	volatile struct msgq_t __iomem *msgq;
};

struct msgq_dev_t {
	int irq;
	int wake;

	int proc_id;

	void *rxmbox;
        int rxmboxreg;
	void *txmbox;
        int txmboxreg;
	u32 mdev_count;
	struct msgq_mdev_t *mdev;

	volatile void * __iomem *mmio;
	int mmiolen;
};

static struct msgq_dev_t *msgq_dev[2];
static void __iomem *dev_timer[2];

#define TIMESTAMP_BASE 19
inline static u32 msgq_GetTimestamp(int dsp)
{
	u32 ts;

	if (dsp == 1 || dsp == 2) {
		ts = __raw_readl(dev_timer[dsp-1]);
		return ts;
	}

	return 0;
}

static const u32 crc32_tab[] = {
	0x00000000, 0x77073096, 0xee0e612c, 0x990951ba, 0x076dc419, 0x706af48f,
	0xe963a535, 0x9e6495a3,	0x0edb8832, 0x79dcb8a4, 0xe0d5e91e, 0x97d2d988,
	0x09b64c2b, 0x7eb17cbd, 0xe7b82d07, 0x90bf1d91, 0x1db71064, 0x6ab020f2,
	0xf3b97148, 0x84be41de,	0x1adad47d, 0x6ddde4eb, 0xf4d4b551, 0x83d385c7,
	0x136c9856, 0x646ba8c0, 0xfd62f97a, 0x8a65c9ec,	0x14015c4f, 0x63066cd9,
	0xfa0f3d63, 0x8d080df5,	0x3b6e20c8, 0x4c69105e, 0xd56041e4, 0xa2677172,
	0x3c03e4d1, 0x4b04d447, 0xd20d85fd, 0xa50ab56b,	0x35b5a8fa, 0x42b2986c,
	0xdbbbc9d6, 0xacbcf940,	0x32d86ce3, 0x45df5c75, 0xdcd60dcf, 0xabd13d59,
	0x26d930ac, 0x51de003a, 0xc8d75180, 0xbfd06116, 0x21b4f4b5, 0x56b3c423,
	0xcfba9599, 0xb8bda50f, 0x2802b89e, 0x5f058808, 0xc60cd9b2, 0xb10be924,
	0x2f6f7c87, 0x58684c11, 0xc1611dab, 0xb6662d3d,	0x76dc4190, 0x01db7106,
	0x98d220bc, 0xefd5102a, 0x71b18589, 0x06b6b51f, 0x9fbfe4a5, 0xe8b8d433,
	0x7807c9a2, 0x0f00f934, 0x9609a88e, 0xe10e9818, 0x7f6a0dbb, 0x086d3d2d,
	0x91646c97, 0xe6635c01, 0x6b6b51f4, 0x1c6c6162, 0x856530d8, 0xf262004e,
	0x6c0695ed, 0x1b01a57b, 0x8208f4c1, 0xf50fc457, 0x65b0d9c6, 0x12b7e950,
	0x8bbeb8ea, 0xfcb9887c, 0x62dd1ddf, 0x15da2d49, 0x8cd37cf3, 0xfbd44c65,
	0x4db26158, 0x3ab551ce, 0xa3bc0074, 0xd4bb30e2, 0x4adfa541, 0x3dd895d7,
	0xa4d1c46d, 0xd3d6f4fb, 0x4369e96a, 0x346ed9fc, 0xad678846, 0xda60b8d0,
	0x44042d73, 0x33031de5, 0xaa0a4c5f, 0xdd0d7cc9, 0x5005713c, 0x270241aa,
	0xbe0b1010, 0xc90c2086, 0x5768b525, 0x206f85b3, 0xb966d409, 0xce61e49f,
	0x5edef90e, 0x29d9c998, 0xb0d09822, 0xc7d7a8b4, 0x59b33d17, 0x2eb40d81,
	0xb7bd5c3b, 0xc0ba6cad, 0xedb88320, 0x9abfb3b6, 0x03b6e20c, 0x74b1d29a,
	0xead54739, 0x9dd277af, 0x04db2615, 0x73dc1683, 0xe3630b12, 0x94643b84,
	0x0d6d6a3e, 0x7a6a5aa8, 0xe40ecf0b, 0x9309ff9d, 0x0a00ae27, 0x7d079eb1,
	0xf00f9344, 0x8708a3d2, 0x1e01f268, 0x6906c2fe, 0xf762575d, 0x806567cb,
	0x196c3671, 0x6e6b06e7, 0xfed41b76, 0x89d32be0, 0x10da7a5a, 0x67dd4acc,
	0xf9b9df6f, 0x8ebeeff9, 0x17b7be43, 0x60b08ed5, 0xd6d6a3e8, 0xa1d1937e,
	0x38d8c2c4, 0x4fdff252, 0xd1bb67f1, 0xa6bc5767, 0x3fb506dd, 0x48b2364b,
	0xd80d2bda, 0xaf0a1b4c, 0x36034af6, 0x41047a60, 0xdf60efc3, 0xa867df55,
	0x316e8eef, 0x4669be79, 0xcb61b38c, 0xbc66831a, 0x256fd2a0, 0x5268e236,
	0xcc0c7795, 0xbb0b4703, 0x220216b9, 0x5505262f, 0xc5ba3bbe, 0xb2bd0b28,
	0x2bb45a92, 0x5cb36a04, 0xc2d7ffa7, 0xb5d0cf31, 0x2cd99e8b, 0x5bdeae1d,
	0x9b64c2b0, 0xec63f226, 0x756aa39c, 0x026d930a, 0x9c0906a9, 0xeb0e363f,
	0x72076785, 0x05005713, 0x95bf4a82, 0xe2b87a14, 0x7bb12bae, 0x0cb61b38,
	0x92d28e9b, 0xe5d5be0d, 0x7cdcefb7, 0x0bdbdf21, 0x86d3d2d4, 0xf1d4e242,
	0x68ddb3f8, 0x1fda836e, 0x81be16cd, 0xf6b9265b, 0x6fb077e1, 0x18b74777,
	0x88085ae6, 0xff0f6a70, 0x66063bca, 0x11010b5c, 0x8f659eff, 0xf862ae69,
	0x616bffd3, 0x166ccf45, 0xa00ae278, 0xd70dd2ee, 0x4e048354, 0x3903b3c2,
	0xa7672661, 0xd06016f7, 0x4969474d, 0x3e6e77db, 0xaed16a4a, 0xd9d65adc,
	0x40df0b66, 0x37d83bf0, 0xa9bcae53, 0xdebb9ec5, 0x47b2cf7f, 0x30b5ffe9,
	0xbdbdf21c, 0xcabac28a, 0x53b39330, 0x24b4a3a6, 0xbad03605, 0xcdd70693,
	0x54de5729, 0x23d967bf, 0xb3667a2e, 0xc4614ab8, 0x5d681b02, 0x2a6f2b94,
	0xb40bbe37, 0xc30c8ea1, 0x5a05df1b, 0x2d02ef8d
};

static u32 msgq_Crc32(u32 crc, const void *buf, size_t size)
{
	const u8 *p = buf;
	crc = crc ^ ~0U;

	while (size--)
		crc = crc32_tab[(crc ^ *p++) & 0xFF] ^ (crc >> 8);

	printk( KERN_ERR "CRC-32: %08X    %08X\n", crc ^ ~0U, crc32_le(crc, buf, size) );

	return crc ^ ~0U;
}

static int msgq_TrigDspIsr(struct msgq_dev_t *dev)
{
	nrw_mbox_send(dev->txmbox, 0x1);

	return 0;
}

static void alarm_trigger(unsigned long arg)
{
	struct msgq_mdev_t *mdev = (struct msgq_mdev_t *)arg;

	unsigned long now;
	unsigned long expire;

	now = jiffies;
	expire = now + HZ; /* delay of 1 second */

	mdev->timeout = 1;
	mdev->alarm_clock.expires = expire;
	wake_up_interruptible(&mdev->waitq);
}

static void set_alarm_clock(struct msgq_mdev_t *mdev)
{
	unsigned long now = jiffies;
	unsigned long expire = now + HZ; /* delay of 1 second */

	init_timer(&mdev->alarm_clock);

	mdev->timeout = 0;
	mdev->alarm_clock.function = alarm_trigger;
	mdev->alarm_clock.expires = expire;
	mdev->alarm_clock.data = (unsigned long)mdev;

	add_timer(&mdev->alarm_clock);
}

void msgq_isr(void *dev_id)
{
	int i;
	struct msgq_dev_t *dev = dev_id;

	for (i = 0; i < dev->mdev_count; i++) {
		if (dev->mdev[i].msgq->u8ArmIntFlag) {
			dev->mdev[i].msgq->u8ArmIntFlag = 0;
			dev->mdev[i].msgq->u8ArmIntReq = 0;
			wake_up_interruptible(&dev->mdev[i].waitq);
		}
	}
	return;
}

#ifdef CONFIG_PROC_FS
static void *msgq_seq_start(struct seq_file *m, loff_t *pos)
{
	return *pos < 1 ? (void *)1 : NULL;
}

static void *msgq_seq_next(struct seq_file *m, void *v, loff_t *pos)
{
	++*pos;
	return NULL;
}

static void msgq_seq_stop(struct seq_file *m, void *v)
{
}

static int msgq_proc_show(struct seq_file *m, void *v)
{
	int i;
	struct msgq_dev_t *dev = m->private;

	for (i = 0; i < dev->mdev_count; i++) {
                seq_printf(m, "\nMSGQ #%d/%d\n===========\n", i+1, dev->mdev_count);
                if (dev->mdev[i].shutdown == 1 || dev->mdev[i].shuterr == 1) {
		    seq_printf(m, "\tstatus     : ERROR\n");
		    seq_printf(m, "\tdevname    : %s\n", dev->mdev[i].devName);
                }
                else {
		    seq_printf(m, "\tstatus     : OK\n");
		    seq_printf(m, "\tdevname    : %s\n", dev->mdev[i].devName);
		    seq_printf(m, "\taddr       : 0x%p\n", dev->mdev[i].msgq);
		    seq_printf(m, "\tname       : %s\n", dev->mdev[i].msgq->szName);
		    seq_printf(m, "\tdir        : %s\n", MSGQUEUE_GET_SRCPROC(dev->mdev[i].msgq) == MyhostId ? "ARM -> DSP" : "DSP -> ARM");
		    seq_printf(m, "\tmsg size   : %d bytes\n", dev->mdev[i].msgq->u32MsgSize);
		    seq_printf(m, "\tqueue size : %d messages\n", dev->mdev[i].msgq->u32QueueSize);
		    seq_printf(m, "\thead       : %d\n", dev->mdev[i].msgq->u32Head);
		    seq_printf(m, "\ttail       : %d\n", dev->mdev[i].msgq->u32Tail);
		    seq_printf(m, "\tirq        : ARM %d:%d  DSP %d:%d\n", dev->mdev[i].msgq->u8ArmIntReq,
			    dev->mdev[i].msgq->u8ArmIntFlag, dev->mdev[i].msgq->u8DspIntReq,
			    dev->mdev[i].msgq->u8DspIntFlag );
		    seq_printf(m, "\tlatency    : < %d us\n", dev->mdev[i].msgq->u32Latency );
		    seq_printf(m, "\tcrc error  : %d\n", dev->mdev[i].u32BadCrcCnt );
		    dev->mdev[i].msgq->u32Latency = 0;
                }
	}

	return 0;
}

static const struct seq_operations msgq_proc_op = {
	.start	= msgq_seq_start,
	.next	= msgq_seq_next,
	.stop	= msgq_seq_stop,
	.show	= msgq_proc_show
};

static int msgq_proc_open(struct inode *inode, struct file *file)
{
	int ret;
	struct seq_file *m;
	struct msgq_dev_t *dev;

	dev = PDE_DATA(inode);
	if (!dev)
		return -ENOENT;

	ret = seq_open(file, &msgq_proc_op);
	if (ret < 0)
		return ret;

	m = file->private_data;
	m->private = dev;

	return 0;
}

static ssize_t msgq_proc_write(struct file *file, const char __user *buf,
			       size_t size, loff_t *ppos)
{
	char *kbuf;
	int ret = 0;

	if (size <= 1 || size >= PAGE_SIZE)
		return -EINVAL;

	kbuf = kmalloc(size + 1, GFP_KERNEL);
	if (!kbuf)
		return -ENOMEM;

	if (copy_from_user(kbuf, buf, size) != 0) {
		kfree(kbuf);
		return -EFAULT;
	}
	kbuf[size] = 0;

	if (0 == 1) {
	}
	else {
		printk(KERN_ERR "msgq: Invalid Command (%s)\n", kbuf);
		kfree(kbuf);
		return -EINVAL;
	}

	ret = size;

	kfree(kbuf);

	return ret;
}

static const struct file_operations msgq_proc_fops = {
	.open		= msgq_proc_open,
	.write		= msgq_proc_write,
	.read		= seq_read,
	.llseek		= seq_lseek,
	.release	= seq_release,
	.owner		= THIS_MODULE,
};

#endif /* CONFIG_PROC_FS */

static int msgq_open(struct inode *inode, struct file *filp)
{
	int i, j;
	struct msgq_mdev_t *mdev = NULL;

        //printk(KERN_INFO "msgq_open starting...\n");

        //printk(KERN_INFO "msgq_open flags: 0x%x, minor: %d\n", filp->f_flags, iminor(inode));
	for (i = 0; i < sizeof(msgq_dev)/sizeof(msgq_dev[0]) && !mdev; i++) {
		for (j = 0; j < msgq_dev[i]->mdev_count; j++) {
			if (msgq_dev[i]->mdev[j].dev.minor == iminor(inode)) {
				mdev = &msgq_dev[i]->mdev[j];
				break;
			}
		}
	}

	if (mdev == NULL) {
		printk(KERN_ERR "msgq: Invalid minor device\n");
		return -ENODEV;
	}

        // This protection allows to check for a current error and clean any past error if there
        mutex_lock(&mdev->lock);
       
        // Msgqueue is currently in error (do not allow to open it)
        if (mdev->shutdown == 1) {
                mutex_unlock(&mdev->lock);
                //printk(KERN_ERR "msgq_open: Failed: shutdown\n");
		return -ENOLINK;
        }

        // If the queue got in error before we opened it, ignore that error
        if (mdev->shuterr == 1) {
	    mdev->shuterr = 0;
        }
        mutex_unlock(&mdev->lock);

	/* Reset stats */
	mdev->msgq->u32Latency = 0;

	filp->private_data = mdev;

        //printk(KERN_INFO "msgq_open end.\n");
	return 0;
}

static int msgq_release(struct inode *inode, struct file *filp)
{
	struct msgq_mdev_t *mdev = filp->private_data;

        //printk(KERN_INFO "msgq_release starting...\n");
        //printk(KERN_INFO "msgq_release flags: 0x%x, minor: %d\n", filp->f_flags, iminor(inode));
        mdev->shuterr = 0;
        //printk(KERN_INFO "msgq_release end.\n");
	return 0;
}

static int msgq_read(struct file *filp, char *buf, size_t count, loff_t *ppos)
{
	int ret;
	u32 crc;
	u8 *pSrc;
	u32 length;
	u32 latency;
	u32 timestamp;
	wait_queue_t wq;
	struct msgq_mdev_t *mdev = filp->private_data;

/*
	static long i = 0;
	static long min = 0;
	static long max = 0;

	struct timespec ts_start,ts_end,test_of_time;
	getnstimeofday(&ts_start);
*/
        //printk(KERN_INFO "msgq_read starting...\n");
        //printk(KERN_INFO "msgq_read flags 0x%x, count: %d, offset: %lld, minor: %d.\n", filp->f_flags, count, *ppos, mdev->dev.minor);

        if (mdev->shutdown == 1 || mdev->shuterr == 1) {
            if (mdev->shuterr == 1) {
                // Only one error message is generated to avoid the application could freeze the kernel if looping there
                printk(KERN_ERR "msgq_read: Failed: shutdown\n");
            }
		mdev->shuterr = 0;
		return -ENOLINK;
        }

	mutex_lock(&mdev->rd_lock);

	while (mdev->msgq->u32Tail == mdev->msgq->u32Head) {

		if (filp->f_flags & O_NONBLOCK) {
			mutex_unlock(&mdev->rd_lock);
			return -EAGAIN;
		}

		//printk(KERN_INFO "msgq_read going to sleep !!\n");

		init_waitqueue_entry( &wq, current );

		/* Ask the DSP to be interrupted */
		mdev->msgq->u8ArmIntReq = 1;

		/* Set alarm clock before going to sleep,
		this prevent us from sleeping indefinitely */
		set_alarm_clock(mdev);

		ret = wait_event_interruptible(mdev->waitq,
			mdev->msgq->u32Tail != mdev->msgq->u32Head ||
			mdev->msgq->u8ArmIntReq == 0 || mdev->timeout == 1 || mdev->shutdown == 1 || mdev->shuterr == 1);

		del_timer(&mdev->alarm_clock);

		if (ret < 0) {
		    printk(KERN_INFO "msgq_read: Failed to wait for event (%d)\n", ret );
		    mutex_unlock(&mdev->rd_lock);
		    return ret;
		}

                if (mdev->shutdown == 1 || mdev->shuterr == 1) {
                    if (mdev->shuterr == 1) {
			printk(KERN_ERR "msgq_read: Failed to wait for event: shutdown\n");
                    }
		    mdev->shuterr = 0;
		    mutex_unlock(&mdev->rd_lock);
		    return -ENOLINK;
                }
	}

	pSrc = (u8 *)&mdev->msgq->pu8Mem[(mdev->msgq->u32MsgSize + 12) * mdev->msgq->u32Tail];

	/* Read timestamp */
	memcpy(&timestamp, &pSrc[0], sizeof(u32));

	/* Read length */
	memcpy(&length, &pSrc[4], sizeof(u32));

	/* Verify count */
	if (length > count) {
		mutex_unlock(&mdev->rd_lock);
		return -EINVAL;
	}

	/* Read message */
	ret = copy_to_user(buf, &pSrc[8], length);
	if (ret) {
		mutex_unlock(&mdev->rd_lock);
		return ret;
	}

	/* Validate the CRC */
	if (msgq_check_crc != 0) {
		memcpy(&crc, &pSrc[8 + length], sizeof(u32));
		if (crc != 0) {
			crc = msgq_Crc32(0, &pSrc[4], sizeof(u32) + length + sizeof(u32));
			if ( crc != 0xFFFFFFFF ) {
				/* Invalid CRC, drop the primitive */
				mdev->u32BadCrcCnt++;
				printk(KERN_INFO "msgq: RX a primitive with a bad CRC !! (%08X)\n", crc);

				length = -EILSEQ;
			}
		}
	}

	/* Update tail pointer */
	mdev->msgq->u32Tail = ((mdev->msgq->u32Tail+1) >= mdev->msgq->u32QueueSize) ?
				0 : (mdev->msgq->u32Tail+1);

	/* Compute round-trip delay in usec */
	latency = (msgq_GetTimestamp(mdev->parent->proc_id) - timestamp) / TIMESTAMP_BASE;
	if (latency > mdev->msgq->u32Latency) {
		mdev->msgq->u32Latency = latency;
	}

	mutex_unlock(&mdev->rd_lock);

	/* Verify if the DSP wants to be interrupted */
	if (mdev->msgq->u8DspIntReq) {

		/* Interrupt the DSP */
		mdev->msgq->u8DspIntFlag = 1;
                msgq_TrigDspIsr(mdev->parent);
	}

/*
	getnstimeofday(&ts_end);
	test_of_time = timespec_sub(ts_end,ts_start);
	i++;

	if (test_of_time.tv_nsec > max) {
		max = test_of_time.tv_nsec;
		printk("rread: used(%lu) MAX(%lu) min(%lu)\n", test_of_time.tv_nsec/1000, max/1000, min/1000);
	}

	if (test_of_time.tv_nsec < min || min == 0) {
		min = test_of_time.tv_nsec;
	}

	if (i % 2000 == 0)
		printk("rread: used(%lu) max(%lu) min(%lu)\n", test_of_time.tv_nsec/1000, max/1000, min/1000);
*/

        //printk(KERN_INFO "msgq_read end.\n");
	return length;
}

static unsigned int msgq_poll(struct file *filp, struct poll_table_struct *wait)
{
	struct msgq_mdev_t *mdev = filp->private_data;
	int rc = 0;

        //printk(KERN_INFO "msgq_poll starting...\n");
        //printk(KERN_INFO "msgq_poll flags 0x%x, minor: %d.\n", filp->f_flags, mdev->dev.minor);

        if (mdev->shutdown == 1 || mdev->shuterr == 1) {
            if (mdev->shuterr == 1) {
		//printk(KERN_ERR "msgq_poll: Failed: shutdown\n");
            }
            rc = POLLERR | POLLOUT | POLLIN;
	    return rc;
        }

	if (MSGQUEUE_GET_SRCPROC(mdev->msgq) == MyhostId) /* ARM2DSP */ {
		u32 nextHead;
		nextHead = ((mdev->msgq->u32Head+1) >= mdev->msgq->u32QueueSize) ? 0 : (mdev->msgq->u32Head+1);
		if (nextHead != mdev->msgq->u32Tail) {
			rc = POLLOUT | POLLWRNORM;
			return rc;
		}
	} else {
		if (mdev->msgq->u32Tail != mdev->msgq->u32Head) {
			rc = POLLIN | POLLRDNORM;
			return rc;
		}
	}

	/* Ask the DSP to be interrupted */
	mdev->msgq->u8ArmIntReq = 1;

	/* Add ourselves to the wait queue */
	poll_wait(filp, &mdev->waitq, wait);

	if (MSGQUEUE_GET_SRCPROC(mdev->msgq) == MyhostId) /* ARM2DSP */ {
		u32 nextHead;
		nextHead = ((mdev->msgq->u32Head+1) >= mdev->msgq->u32QueueSize) ? 0 : (mdev->msgq->u32Head+1);
		if (nextHead != mdev->msgq->u32Tail)
			rc = POLLOUT | POLLWRNORM;
	} else {
		if (mdev->msgq->u32Tail != mdev->msgq->u32Head)
			rc = POLLIN | POLLRDNORM;
	}

        //printk(KERN_INFO "msgq_poll starting end.\n");
	return rc;
}

static int msgq_write(struct file *filp, const char *buf, size_t count, loff_t *ppos)
{
	int ret;
	u32 crc;
	u32 nextHead;
	wait_queue_t wq;
	u32 timestamp;

/*
	static long i = 0;
	static long min = 0;
	static long max = 0;

	struct timespec ts_start,ts_end,test_of_time;
	getnstimeofday(&ts_start);
*/

	struct msgq_mdev_t *mdev = filp->private_data;
        //printk(KERN_INFO "msgq_write starting...\n");
        //printk(KERN_INFO "msgq_write flags 0x%x, count: %d, offset: %lld, char: %d, minor: %d.\n", filp->f_flags, count, *ppos, (int)(*buf), mdev->dev.minor);


	if (count>mdev->msgq->u32MsgSize) {
		printk(KERN_ERR "msgq: Message is too large (%d > %d)\n", count, mdev->msgq->u32MsgSize);
		return -EINVAL;
	}

        if (mdev->shutdown == 1 || mdev->shuterr == 1) {
            if (mdev->shuterr == 1) {
		printk(KERN_ERR "msgq_write: Failed: shutdown\n");
            }
            mdev->shuterr = 0;
	    return -ENOLINK;
        }

	mutex_lock(&mdev->wr_lock);

	nextHead = ((mdev->msgq->u32Head+1) >= mdev->msgq->u32QueueSize) ? 0 : (mdev->msgq->u32Head+1);

	/* Look if there is room available */
	while (nextHead == mdev->msgq->u32Tail) {
		if (filp->f_flags & O_NONBLOCK) {
			mutex_unlock(&mdev->wr_lock);
			return -EAGAIN;
		}

//		printk(KERN_INFO "msgq_write: going to sleep !!\n");

		init_waitqueue_entry(&wq, current);

		/* Ask the DSP to be interrupted */
		mdev->msgq->u8ArmIntReq = 1;

		/* Set alarm clock before going to sleep,
		this prevent us from sleeping indefinitely */
		set_alarm_clock(mdev);

		ret = wait_event_interruptible(mdev->waitq,
		       nextHead != mdev->msgq->u32Tail ||
			mdev->msgq->u8ArmIntReq == 0 || mdev->timeout == 1 || mdev->shutdown == 1 || mdev->shuterr == 1);

		del_timer(&mdev->alarm_clock);

		if (ret < 0) {
			printk(KERN_INFO "msgq_write: Failed to wait for event (%d)\n", ret );
			mutex_unlock(&mdev->wr_lock);
			return ret;
		}

                if (mdev->shutdown == 1 || mdev->shuterr == 1) {
                    if (mdev->shuterr == 1) {
			printk(KERN_ERR "msgq_write: Failed to wait for event: shutdown\n");
                    }
                    mdev->shuterr = 0;
		    mutex_unlock(&mdev->wr_lock);
		    return -ENOLINK;
                }
	}

	/* Write length */
	memcpy((void *)&mdev->msgq->pu8Mem[(mdev->msgq->u32MsgSize + 12) *
			mdev->msgq->u32Head + 1 * sizeof(u32)], &count, sizeof(u32));

	/* Write message */
	ret = copy_from_user((void *)&mdev->msgq->pu8Mem[(mdev->msgq->u32MsgSize + 12) *
			mdev->msgq->u32Head + 2 * sizeof(u32)], buf, count);
	if (ret) {
		printk(KERN_ERR "msgq: Copy from user failed (%d)\n", ret );
		mutex_unlock(&mdev->wr_lock);
		return ret;
	}

	/* Compute and add the CRC */
	if (msgq_check_crc != 0) {
		crc = msgq_Crc32(0, (void *)&mdev->msgq->pu8Mem[(mdev->msgq->u32MsgSize + 12) *
				mdev->msgq->u32Head + 1 * sizeof(u32)], sizeof(count) + count);
		crc ^= 0xFFFFFFFF;
	}
	else {
		crc = 0;
	}

	memcpy((void *)&mdev->msgq->pu8Mem[(mdev->msgq->u32MsgSize + 12) *
			mdev->msgq->u32Head + 2 * sizeof(u32) + count], &crc, sizeof(u32));

	timestamp = msgq_GetTimestamp(mdev->parent->proc_id);

	/* Write timestamp */
	memcpy((void *)&mdev->msgq->pu8Mem[(mdev->msgq->u32MsgSize + 12) *
		mdev->msgq->u32Head + 0 * sizeof(u32)], &timestamp, sizeof(u32));

	/* Update head pointer */
	mdev->msgq->u32Head = nextHead;

	mutex_unlock(&mdev->wr_lock);

	/* Verify if the DSP wants to be interrupted */
	if (mdev->msgq->u8DspIntReq) {

		mdev->msgq->u8DspIntFlag = 1;
		msgq_TrigDspIsr(mdev->parent);
	}
/*
	getnstimeofday(&ts_end);
	test_of_time = timespec_sub(ts_end,ts_start);
	i++;

	if (test_of_time.tv_nsec > max) {
		max = test_of_time.tv_nsec;
		printk("write: used(%lu) MAX(%lu) min(%lu)\n", test_of_time.tv_nsec/1000, max/1000, min/1000);
	}

	if (test_of_time.tv_nsec < min || min == 0) {
		min = test_of_time.tv_nsec;
	}

	if (i % 2000 == 0)
		printk("write: used(%lu) max(%lu) min(%lu)\n", test_of_time.tv_nsec/1000, max/1000, min/1000);
*/
        //printk(KERN_INFO "msgq_write end...\n");
	return count;
}

static const struct file_operations msgq_fops = {
	.open		= msgq_open,
	.release	= msgq_release,
	.read		= msgq_read,
	.write		= msgq_write,
	.poll		= msgq_poll,
	.owner		= THIS_MODULE,
};

static struct msgq_dev_t *msgq_probe(void *va, int len)
{
	int i;
	int ret = -ENODEV;

	struct msgq_t *pMsgq, *start_Msgq = NULL;
	struct msgq_dev_t *dev = NULL;
	struct proc_dir_entry *ent = NULL;

	/* The memory is set to zero by kzalloc */
	dev = kzalloc(sizeof(*dev), GFP_KERNEL);
	if (!dev) {
		printk(KERN_ERR "msgq: Failed to allocate device structure\n");
		ret = -ENOMEM;
		goto error_alloc_dev;
	}

	/* Map MSGQ memory section */
	dev->mmio = va;
        dev->mmiolen = len;
	//printk(KERN_INFO "msgq: va: %x, len: %d\n", (unsigned)(va), len);
	if (!dev->mmio) {
		printk(KERN_ERR "msgq: Failed to I/O remap MSGQ memory\n");
		ret = -ENOMEM;
		goto error_ioremap;
	}

	/* Count the number of queues */
	pMsgq = (struct msgq_t *)dev->mmio;
	for (i = sizeof(struct msgq_t); i < len; ) {

		if (i + pMsgq->u32QueueSize * (pMsgq->u32MsgSize + 12) > len) {
			break;
		}

		if (pMsgq->u32MagicId == 0x4D534751/*'MSGQ'*/) {

			if (dev->mdev_count == 0) {
				start_Msgq = pMsgq;
			}

			dev->mdev_count++;
		}

		i += (sizeof(struct msgq_t) + pMsgq->u32QueueSize * (pMsgq->u32MsgSize + 12) + 0x7F) & ~0x7F;
		pMsgq = (struct msgq_t *)((u32)pMsgq + ((sizeof(struct msgq_t) +
				pMsgq->u32QueueSize * (pMsgq->u32MsgSize + 12) + 0x7F) & ~0x7F));

	}

	if (dev->mdev_count == 0) {
		printk(KERN_ERR "msgq: No valid msgq found\n");
		ret = -EFAULT;
		goto error_noqueue;
	}

	/* Alloc memory for each queue misc device */
	dev->mdev = kzalloc(dev->mdev_count * sizeof(struct msgq_mdev_t), GFP_KERNEL);
	if (!dev->mdev) {
		printk(KERN_ERR "msgq: Failed to allocate misc device structure\n");
		ret = -ENOMEM;
		goto error_alloc_mdev;
	}

	/* Init each queue device */
	pMsgq = start_Msgq;

	for (i = 0; i < dev->mdev_count; i++) {

		dev->mdev[i].shutdown = 0;
                dev->mdev[i].shuterr = 0;

		dev->mdev[i].u32BadCrcCnt = 0;
		mutex_init(&dev->mdev[i].wr_lock);
		mutex_init(&dev->mdev[i].rd_lock);
		//spin_lock_init(&dev->mdev[i].lock);
		mutex_init(&dev->mdev[i].lock);
		init_waitqueue_head(&dev->mdev[i].waitq);

		dev->mdev[i].msgq = pMsgq;
		snprintf(dev->mdev[i].devName, sizeof(dev->mdev[i].devName), "msgq/%s", pMsgq->szName);
		//printk(KERN_ERR "msgq name: %s, count: %d, size: %d\n", pMsgq->szName, i, pMsgq->u32QueueSize);
		//printk(KERN_ERR "msgq addr: %08X, physical: %08X\n", (unsigned)(pMsgq), virt_to_phys(pMsgq));

		pMsgq = (struct msgq_t *)((u32)pMsgq + (((sizeof(struct msgq_t) +
				pMsgq->u32QueueSize * (pMsgq->u32MsgSize + 12)) + 0x7F) & ~0x7F));

		/* Identify on which DSP we are and set the proper signaling */
		if ((MSGQUEUE_GET_DSTPROC(dev->mdev[i].msgq) == MyhostId && MSGQUEUE_GET_SRCPROC(dev->mdev[i].msgq) == Mydsp1Id)
			|| (MSGQUEUE_GET_DSTPROC(dev->mdev[i].msgq) == Mydsp1Id && MSGQUEUE_GET_SRCPROC(dev->mdev[i].msgq) == MyhostId)) {

		        //printk(KERN_ERR "msgq mbox for dsp: %d\n", Mydsp1Id);
			dev->rxmbox = nrw_mbox_lookup("mbox_mpu_10");
			if (dev->rxmbox == NULL) {
				printk(KERN_ERR "msgq: mbox 'mbox_mpu_10' not found\n");
				goto error_mbox;
			}

			dev->txmbox = nrw_mbox_lookup("mbox_dsp1_10");
			if (dev->txmbox == NULL) {
				printk(KERN_ERR "msgq: mbox 'mbox_dsp1_10' not found\n");
				goto error_mbox;
			}

			dev->proc_id = 1;
		}

		if ((MSGQUEUE_GET_DSTPROC(dev->mdev[i].msgq) == MyhostId && MSGQUEUE_GET_SRCPROC(dev->mdev[i].msgq) == Mydsp2Id)
			|| (MSGQUEUE_GET_DSTPROC(dev->mdev[i].msgq) == Mydsp2Id && MSGQUEUE_GET_SRCPROC(dev->mdev[i].msgq) == MyhostId)) {

		        //printk(KERN_ERR "msgq mbox for dsp: %d\n", Mydsp2Id);
			dev->rxmbox = nrw_mbox_lookup("mbox_mpu_11");
			if (dev->rxmbox == NULL) {
				printk(KERN_ERR "msgq: mbox 'mbox_mpu_11' not found\n");
				goto error_mbox;
			}

			dev->txmbox = nrw_mbox_lookup("mbox_dsp2_11");
			if (dev->txmbox == NULL) {
				printk(KERN_ERR "msgq: mbox 'mbox_dsp2_11' not found\n");
				goto error_mbox;
			}

			dev->proc_id = 2;
		}

		if (dev->proc_id > 0) {

			dev->mdev[i].parent = dev;
			dev->mdev[i].dev.name  = dev->mdev[i].devName;
			dev->mdev[i].dev.minor = MISC_DYNAMIC_MINOR;
			dev->mdev[i].dev.fops  = &msgq_fops;
			ret = misc_register(&dev->mdev[i].dev);

			if (ret < 0) {
			    printk(KERN_ERR "msgq: Error duplicated queue name: %s\n", dev->mdev[i].devName);
			    printk(KERN_ERR "msgq: Error registering misc driver\n");
			    goto error_miscdev;
			}
		}
	}

	/* Set the global device structure pointer */
	if (dev->proc_id > 0) {

		if (msgq_dev[dev->proc_id-1] == NULL) {

			/* Register IRQ */
			printk(KERN_INFO "msgq: proc_id(%d) registering mbox begin.\n", dev->proc_id);
			ret = nrw_mbox_register(dev->rxmbox, msgq_isr, dev);
			if (ret != 0) {
				printk(KERN_ERR "msgq: proc_id(%d) rxmbox register failed\n", dev->proc_id);
				goto error_mbox_register;
			}
			else
				printk(KERN_INFO "msgq: proc_id(%d) registering mbox end.\n", dev->proc_id);
			dev->rxmboxreg = 1;
                        msgq_dev[dev->proc_id-1] = dev;
		}
	}

	/* Create PROC Entry */
	#ifdef CONFIG_PROC_FS
	{
		if (dev->proc_id == 1)
			ent = proc_create_data("msgq1", 0, NULL, &msgq_proc_fops, dev);
		if (dev->proc_id == 2)
			ent = proc_create_data("msgq2", 0, NULL, &msgq_proc_fops, dev);

		if (ent == NULL) {
			printk(KERN_ERR "msgq: failed to create proc entry\n");
		}
	}
	#endif

	printk(KERN_INFO "msgq: probed\n");
	return dev;

error_mbox_register:
error_miscdev:
	for (i = 0; i < dev->mdev_count; i++) {
		if (dev->mdev[i].msgq != NULL) {
			misc_deregister(&dev->mdev[i].dev);
		}
	}
error_mbox:
	kfree(dev->mdev);
error_alloc_mdev:
error_noqueue:
error_ioremap:
	kfree(dev);
error_alloc_dev:
	printk(KERN_ERR "msgq: probe failed\n");
	return NULL;
}

static int msgq_remove(struct msgq_dev_t *dev)
{
	int i;

	#ifdef CONFIG_PROC_FS
	{
		if (dev->proc_id == 1)
			remove_proc_entry("msgq1", NULL);
		if (dev->proc_id == 2)
			remove_proc_entry("msgq2", NULL);
	}
	#endif

	if (dev->proc_id == 1 || dev->proc_id == 2) {

        if (dev->rxmboxreg != 0)
		nrw_mbox_deregister(dev->rxmbox, dev);

		msgq_dev[dev->proc_id-1] = NULL;

		// Go in shutdown mode for each queue and wakeup any user process that could be sleeping in our driver code
                for (i = 0; i < dev->mdev_count; i++) {
			if (dev->mdev[i].msgq != NULL) {
                                mutex_lock(&dev->mdev[i].lock);
                                dev->mdev[i].shuterr = 1;
				dev->mdev[i].shutdown = 1;
                                mutex_unlock(&dev->mdev[i].lock);
	                        wake_up_interruptible(&dev->mdev[i].waitq);
			}
		}
		// This could cause a problem if application is still using us
		for (i = 0; i < dev->mdev_count; i++) {
			if (dev->mdev[i].msgq != NULL) {
                                misc_deregister(&dev->mdev[i].dev);
			}
		}
	}
	dev->proc_id = 0;

	kfree(dev->mdev);
	kfree(dev);
        //printk(KERN_INFO "msgqueue_stop end.\n");
	return 0;
}

static int msgq_pauseio(struct msgq_dev_t *dev)
{
	int i;

	if (dev->proc_id == 1 || dev->proc_id == 2) {

		// Unregister mbx irq if we are not already paused
                if (dev->rxmboxreg != 0)
                {
                    printk(KERN_INFO "msgq: pause proc_id(%d) deregistering mbox begin.\n", dev->proc_id);
                    nrw_mbox_deregister(dev->rxmbox, dev);
                    printk(KERN_INFO "msgq: pause proc_id(%d) deregistering mbox end.\n", dev->proc_id);
                    dev->rxmboxreg = 0;

		    // Go in shutdown mode for each queue and wakeup any user process that could be sleeping in our driver code
		    for (i = 0; i < dev->mdev_count; i++) {
			if (dev->mdev[i].msgq != NULL) {
		                mutex_lock(&dev->mdev[i].lock);
		                dev->mdev[i].shuterr = 1;
			        dev->mdev[i].shutdown = 1;
		                mutex_unlock(&dev->mdev[i].lock);
			        wake_up_interruptible(&dev->mdev[i].waitq);
			}
		    }
                }

	}
	//dev->proc_id = 0;

        //printk(KERN_INFO "msgqueue_pause end.\n");
	return 0;
}

// This function will attempt to reassign correct addresses for msgqueue when the same firmware (or with exact same queue definitions) code is reloaded
// It may fail if a different firmware is used than what was used at msgq_probe time
static int msgq_reprobe(void *va, int len, void *_dev)
{
	int i;
	int ret = 0;

	struct msgq_t *pMsgq, *start_Msgq = NULL;
	//struct proc_dir_entry *ent = NULL;
	struct msgq_dev_t *dev = NULL;
	struct msgq_dev_t *probedev = _dev;
        unsigned offset;

       // Checks if probed ptr make sense
       if (!probedev || !probedev->mmio) {
		printk(KERN_ERR "msgq: Failed reprobe MSGQ memory\n");
		ret = -EFAULT;
                goto error_alloc_dev;
	}

	// Checks if msgqueue section is the same length than what it was at 1st probe time (could detect different firmware)
        if (probedev->mmiolen != len) {
		printk(KERN_ERR "msgq: reprobe:%d probed:%d\n",len, probedev->mmiolen);
		printk(KERN_ERR "msgq: section size different than probed section\n");
		ret = -EFAULT;
                goto error_alloc_dev;
        }

        // Compute offset between reprobe va and probed va just in case they are differents
        offset = (unsigned)(va) - (unsigned)(probedev->mmio);
	//printk(KERN_INFO "msgq: new va: %08X, old va: %08X\n", (unsigned)(va), (unsigned)((unsigned)(probedev->mmio)));
	//printk(KERN_INFO "msgq: new len: %d, old len: %d, va offset: %08X\n", len, (probedev->mmiolen), offset);

        /* The memory is set to zero by kzalloc */
	dev = kzalloc(sizeof(*dev), GFP_KERNEL);
	if (!dev) {
		printk(KERN_ERR "msgq: Failed to allocate reprobe device structure\n");
		ret = -ENOMEM;
		goto error_alloc_dev;
	}

	/* Map MSGQ memory section */
	dev->mmio = va;
        dev->mmiolen = len;
	//printk(KERN_INFO "msgq: reprobe va: %x, len: %d\n", (unsigned)(va), len);
	if (!dev->mmio) {
		printk(KERN_ERR "msgq: Failed to I/O remap reprobe MSGQ memory\n");
		ret = -ENOMEM;
		goto error_ioremap;
	}

	/* Count the number of queues with current firmware */
	pMsgq = (struct msgq_t *)dev->mmio;
	for (i = sizeof(struct msgq_t); i < len; ) {

		if (i + pMsgq->u32QueueSize * (pMsgq->u32MsgSize + 12) > len) {
			break;
		}

		if (pMsgq->u32MagicId == 0x4D534751/*'MSGQ'*/) {

			if (dev->mdev_count == 0) {
				start_Msgq = pMsgq;
			}

			dev->mdev_count++;
		}

		i += (sizeof(struct msgq_t) + pMsgq->u32QueueSize * (pMsgq->u32MsgSize + 12) + 0x7F) & ~0x7F;
		pMsgq = (struct msgq_t *)((u32)pMsgq + ((sizeof(struct msgq_t) +
				pMsgq->u32QueueSize * (pMsgq->u32MsgSize + 12) + 0x7F) & ~0x7F));

	}

	if (dev->mdev_count == 0) {
		printk(KERN_ERR "msgq: No valid reprobe msgq found\n");
		ret = -EFAULT;
		goto error_noqueue;
	}

	// Checks if the same number of queue was found than at probe time
        if (dev->mdev_count != probedev->mdev_count) {
		printk(KERN_ERR "msgq: reprobe:%d probed:%d\n",dev->mdev_count, probedev->mdev_count);
		printk(KERN_ERR "msgq: different number of reprobe msgq found\n");
		ret = -EFAULT;
		goto error_noqueue;
	}

	/* Alloc memory for each queue misc device */
	dev->mdev = kzalloc(dev->mdev_count * sizeof(struct msgq_mdev_t), GFP_KERNEL);
	if (!dev->mdev) {
		printk(KERN_ERR "msgq: Failed to allocate reprobe misc device structure\n");
		ret = -ENOMEM;
		goto error_alloc_mdev;
	}

	/* Init each queue device */
	pMsgq = start_Msgq;

	// Check each reprobe queue name and address
        for (i = 0; i < dev->mdev_count; i++) {

		dev->mdev[i].msgq = pMsgq;
		snprintf(dev->mdev[i].devName, sizeof(dev->mdev[i].devName), "msgq/%s", pMsgq->szName);
		//printk(KERN_ERR "msgq name: %s, count: %d, size: %d\n", pMsgq->szName, i, pMsgq->u32QueueSize);
		//printk(KERN_ERR "msgq addr: %08X, physical: %08X\n", (unsigned)(pMsgq), virt_to_phys(pMsgq));

		pMsgq = (struct msgq_t *)((u32)pMsgq + (((sizeof(struct msgq_t) +
				pMsgq->u32QueueSize * (pMsgq->u32MsgSize + 12)) + 0x7F) & ~0x7F));

		/* Identify on which DSP we are */
		if ((MSGQUEUE_GET_DSTPROC(dev->mdev[i].msgq) == MyhostId && MSGQUEUE_GET_SRCPROC(dev->mdev[i].msgq) == Mydsp1Id)
			|| (MSGQUEUE_GET_DSTPROC(dev->mdev[i].msgq) == Mydsp1Id && MSGQUEUE_GET_SRCPROC(dev->mdev[i].msgq) == MyhostId)) {
                 
			dev->proc_id = 1;
		}

		if ((MSGQUEUE_GET_DSTPROC(dev->mdev[i].msgq) == MyhostId && MSGQUEUE_GET_SRCPROC(dev->mdev[i].msgq) == Mydsp2Id)
			|| (MSGQUEUE_GET_DSTPROC(dev->mdev[i].msgq) == Mydsp2Id && MSGQUEUE_GET_SRCPROC(dev->mdev[i].msgq) == MyhostId)) {

			dev->proc_id = 2;
		}

		if (dev->proc_id > 0) {

			dev->mdev[i].parent = dev;
			dev->mdev[i].dev.name  = dev->mdev[i].devName;
		}
	}

	// Check each reprobed queue to see if they are the same as probed ones
        for (i = 0; i < dev->mdev_count; i++) {
            // If the reprobed msgqueue name the same as probed one
            if (strncmp(dev->mdev[i].dev.name, probedev->mdev[i].dev.name,sizeof(dev->mdev[i].devName)) != 0) {
		printk(KERN_ERR "msgq: reprobe:%s probed:%s\n",dev->mdev[i].dev.name,probedev->mdev[i].dev.name);
                printk(KERN_ERR "msgq: Error reprobe different queue name than probed one\n");
		ret = -EFAULT;
	        goto error_miscdev;
            }
            
            // Check if the reprobe msgqueue is relatively (within offset) at the same address as probed ones
            if (dev->mdev[i].msgq != ((volatile struct msgq_t __iomem *)((unsigned) probedev->mdev[i].msgq + offset))) {
		printk(KERN_ERR "msgq: count:%d name:%s reprobe:0x%x probed:0x%x\n",i,probedev->mdev[i].dev.name,(unsigned)dev->mdev[i].msgq, (unsigned)((volatile struct msgq_t __iomem *)((unsigned) probedev->mdev[i].msgq + offset)));
                printk(KERN_ERR "msgq: Error reprobe different queue address than probed one\n");
		ret = -EFAULT;
	        goto error_miscdev;
            }
        }

	// Reajust all probed msgqueue addresses if now offseted from reprobed one, and remove shutdown status
        for (i = 0; i < dev->mdev_count ; i++) {
            if (offset != 0)
                probedev->mdev[i].msgq = (volatile struct msgq_t __iomem *)((unsigned) probedev->mdev[i].msgq + offset);
            mutex_lock(&probedev->mdev[i].lock);
	    probedev->mdev[i].shutdown = 0;
            mutex_unlock(&probedev->mdev[i].lock);
        }

	/* Update the old buffer address to the newly allocated one */
	if (offset != 0)
		probedev->mmio = (void *)(va);

	/* Restart irq if not already done */
	if (dev->proc_id > 0) {

		if (probedev->rxmboxreg == 0) {

			/* Register IRQ */
			printk(KERN_INFO "msgq: reprobe proc_id(%d) registering mbox begin.\n", dev->proc_id);
			ret = nrw_mbox_register(probedev->rxmbox, msgq_isr, probedev);
			if (ret != 0) {
				printk(KERN_ERR "msgq: reprobe proc_id(%d) rxmbox register failed\n", dev->proc_id);
				goto error_mbox_register;
			}
			else
				printk(KERN_INFO "msgq: reprobe proc_id(%d) registering mbox end.\n", dev->proc_id);

			probedev->rxmboxreg = 1;
		}
	}

	kfree(dev->mdev);
	kfree(dev);
	printk(KERN_INFO "msgq: reprobed\n");
	return ret;

error_mbox_register:
error_miscdev:
//error_mbox:
	kfree(dev->mdev);
error_alloc_mdev:
error_noqueue:
error_ioremap:
	kfree(dev);
error_alloc_dev:
	printk(KERN_ERR "msgq: reprobe failed\n");
	return ret;
}

void *msgqueue_start(void *va, int len)
{
        //printk(KERN_INFO "msgqueue_start.\n");
	return msgq_probe(va, len);
}
EXPORT_SYMBOL(msgqueue_start);

// To use this, application must be disconnected from the driver
int msgqueue_stop(void *_dev)
{
	struct msgq_dev_t *dev = _dev;

        //printk(KERN_INFO "msgqueue_stop start...\n");
	return msgq_remove(dev);
}

EXPORT_SYMBOL(msgqueue_stop);


int msgqueue_restart(void *va, int len, void *_dev)
{
	struct msgq_dev_t *dev = _dev;

        //printk(KERN_INFO "msgqueue_restart start...\n");
	return msgq_reprobe(va, len, dev);
}

EXPORT_SYMBOL(msgqueue_restart);

int msgqueue_pause(void *_dev)
{
	struct msgq_dev_t *dev = _dev;

        //printk(KERN_INFO "msgqueue_pause start...\n");
	return msgq_pauseio(dev);
}

EXPORT_SYMBOL(msgqueue_pause);

static int __init msgq_init(void)
{
        //printk(KERN_INFO "msgq_init.\n");

	/* get a readable virtual address,
	used to read dsp timestamp */
	dev_timer[0] = ioremap(0x4882603C, 4);
	dev_timer[1] = ioremap(0x4882403C, 4);
	return 0;
}

static void __exit msgq_cleanup(void)
{
        //printk(KERN_INFO "msgq_cleanup.\n");
	iounmap(dev_timer[0]);
	iounmap(dev_timer[1]);
}

module_init(msgq_init);
module_exit(msgq_cleanup);

MODULE_DESCRIPTION("Message Queue Driver");
MODULE_AUTHOR("Nutaq Inc. <www.nutaq.com>");
MODULE_LICENSE("GPL");
MODULE_ALIAS("platform:" MODULE_NAME);


