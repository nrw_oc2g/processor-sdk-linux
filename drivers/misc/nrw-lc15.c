/*
 * nrw-lc15.c - Litecell 1.5 board revision/option module
 *
 *  Copyright (C) 2015 Nuranwireless Inc.
 *  <support@nuranwireless.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#include <linux/slab.h>
#include <linux/module.h>
#include <linux/pm_runtime.h>
#include <linux/math64.h>
#include <linux/platform_device.h>
#include <linux/err.h>
#include <linux/io.h>
#include <linux/of.h>
#include <linux/of_gpio.h>

#define NR_REV_GPIO	 4
#define NR_OPT_GPIO	20

struct lc15_priv {
	struct device *dev;

        int  rev_gpio[NR_REV_GPIO];
        char rev_szGpio[NR_REV_GPIO][16];
        int  opt_gpio[NR_OPT_GPIO];
        char opt_szGpio[NR_OPT_GPIO][16];

	char revision;
	unsigned option;
};

static ssize_t lc15_show_revision(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	struct lc15_priv *priv = dev_get_drvdata(dev);
	return sprintf(buf, "%c\n", priv->revision);
}

static ssize_t lc15_show_option(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	struct lc15_priv *priv = dev_get_drvdata(dev);
	return sprintf(buf, "0x%05X\n", priv->option);
}

static DEVICE_ATTR(revision,  S_IRUGO, lc15_show_revision, NULL);
static DEVICE_ATTR(option,    S_IRUGO, lc15_show_option,   NULL);

static struct attribute *lc15_attrs[] = {
	&dev_attr_revision.attr,
	&dev_attr_option.attr,
	NULL,
};

static const struct attribute_group lc15_attr_group = {
	.attrs = lc15_attrs,
};
ATTRIBUTE_GROUPS(lc15);

int lc15_parse_dt(struct platform_device *pdev)
{
	int i;
        int nr;
	struct device_node *np = pdev->dev.of_node;
	struct lc15_priv *priv = dev_get_drvdata(&pdev->dev);

	if (!np) {
                return -EINVAL;
	}

	// Board revision
        nr = of_gpio_named_count(np, "rev-gpios");
	if ( nr != NR_REV_GPIO ) {
                return -EINVAL;
	}
        for (i = 0; i < NR_REV_GPIO; i++) {
                priv->rev_gpio[i] = of_get_named_gpio(np, "rev-gpios", i);
                if (priv->rev_gpio[i] < 0) return -EINVAL;
        }

	// Board option
        nr = of_gpio_named_count(np, "opt-gpios");
	if ( nr != NR_OPT_GPIO ) {
                return -EINVAL;
	}
        for (i = 0; i < NR_OPT_GPIO; i++) {
                priv->opt_gpio[i] = of_get_named_gpio(np, "opt-gpios", i);
                if (priv->opt_gpio[i] < 0) return -EINVAL;
        }
	return 0;
}

static int lc15_probe(struct platform_device *pdev)
{
	struct lc15_priv *priv;
	int ret;
	int i;

	priv = devm_kzalloc(&pdev->dev, sizeof(struct lc15_priv), GFP_KERNEL);
	if (!priv)
		return -ENOMEM;

	priv->dev = &pdev->dev;
	platform_set_drvdata(pdev, priv);

	ret = lc15_parse_dt(pdev);
	if (ret) {	
		return ret;
	}

	// Board revision
        priv->revision = 0;
	for (i = 0; i < NR_REV_GPIO; i++) {
		sprintf(priv->rev_szGpio[i], "board_rev%d", i);
 		ret = devm_gpio_request_one(&pdev->dev, priv->rev_gpio[i], GPIOF_IN, priv->rev_szGpio[i]);
		if ( ret ) return ret;
                
		priv->revision |= (gpio_get_value(priv->rev_gpio[i]) << i);
        }
	priv->revision += 'A';

        // Board option
        priv->option = 0;
	for (i = 0; i < NR_OPT_GPIO; i++) {
		sprintf(priv->opt_szGpio[i], "board_opt%d", i);
 		ret = devm_gpio_request_one(&pdev->dev, priv->opt_gpio[i], GPIOF_IN, priv->opt_szGpio[i]);
		if ( ret ) return ret;
                
		priv->option |= (gpio_get_value(priv->opt_gpio[i]) << i);
        }
	dev_info(priv->dev, "lc15: Board Revision %c model %05X\n", priv->revision, priv->option);

	ret = sysfs_create_group(&priv->dev->kobj, lc15_groups[0]);
	if (ret) {
		dev_err(priv->dev, "unable to create sysfs files\n");
		return ret;
	}
	return 0;
}

static int lc15_remove(struct platform_device *pdev)
{
	struct lc15_priv *priv = platform_get_drvdata(pdev);

	sysfs_remove_group(&priv->dev->kobj, lc15_groups[0]);
	return 0;
}

static const struct of_device_id lc15_of_match[] = {
        { .compatible = "nrw-lc15", },
	{}
};
MODULE_DEVICE_TABLE(of, lc15_of_match);

static struct platform_driver lc15_driver = {
	.driver = {
		.name = "nrw-lc15",
		.owner = THIS_MODULE,
                .of_match_table = of_match_ptr(lc15_of_match),
	},
	.probe = lc15_probe,
	.remove = lc15_remove,
};

module_platform_driver(lc15_driver);

MODULE_DESCRIPTION("Nuran Wireless Litecell 1.5 Driver");
MODULE_AUTHOR("<support@nuranwireless.com>");
MODULE_LICENSE("GPL");
