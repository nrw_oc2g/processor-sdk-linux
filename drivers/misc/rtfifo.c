#include <linux/slab.h>
#include <linux/version.h>
#include <linux/device.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/interrupt.h>
#include <linux/firmware.h>
#include <linux/platform_device.h>
#include <linux/miscdevice.h>
#include <linux/delay.h>
#include <linux/completion.h>
#include <linux/freezer.h>
#include <linux/uaccess.h>
#include <linux/io.h>
#include <linux/timer.h>
#include <linux/rtfifo.h>

#include <asm/mach-types.h>
#include <asm/gpio.h>
#include <asm/io.h>

#ifdef CONFIG_PROC_FS
#include <linux/proc_fs.h>
#include <linux/seq_file.h>
#include <asm/uaccess.h>
#endif /* CONFIG_PROC_FS */

#include "nrw_mbox.h"

#define MODULE_NAME 	"rtfifo"

static const char rtfifo_driver_version[] = "v2.3";

/* All processor differents id */
#define MyhostId   0
#define Mydsp1Id   1
#define Mydsp2Id   2
#define Myipu1_0Id 3
#define Myipu2_0Id 4
#define Myipu1_1Id 5
#define Myipu2_1Id 6

/* Get the source processor id of the fifo */
#define RTFIFO_GET_SRCPROC(pFifo) ((((pFifo)->u8Dir) >> 4) & 0xF)

/* Get the destination processor id of the fifo */
#define RTFIFO_GET_DSTPROC(pFifo) (((pFifo)->u8Dir) & 0xF)

struct rtfifo_t {
	u32	u32MagicId;
	u32	u32Size;
	u32	reserved[4];
	u32	u32Head;
	u32	u32Tail;
	u8	u8Dir;           ///< Direction (SrcProcId << 4 | DstProcId),
                          ///< ex: DSP1->DSP2=0x12, DSP2->DSP1=0x21, HOST->DSP1=0x01, HOST->DSP2=0x02
	u8	u8ArmIntReq;
	u8	u8ArmIntFlag;
	u8	u8DspIntReq;
	u8	u8DspIntFlag;
	char	szName[91];
	u8	pu8Data[0];
} rtfifo_t;

struct rtfifo_mdev_t {
	char devName[128];
	struct mutex rd_lock;
	struct mutex wr_lock;
	struct miscdevice dev;

	u8 timeout;
        volatile u8 shutdown;               // when != 0 means operation is paused while msgqueue is not available because of device firmware reload
        volatile u8 shuterr;                // when != 0 means we got shutdown problem, this will make sure this problem is reported at least once to the application
	struct timer_list alarm_clock;

	struct rtfifo_dev_t *parent;
	struct mutex lock;
	wait_queue_head_t waitq;
	volatile struct rtfifo_t __iomem *rtfifo;
};

struct rtfifo_dev_t {
	int irq;
	int wake;

	int proc_id;
	void *rxmbox;
        int rxmboxreg;
	void *txmbox;
        int txmboxreg;
	u32 mdev_count;
	struct rtfifo_mdev_t *mdev;

	volatile void * __iomem *mmio;
	int mmiolen;
};

static struct rtfifo_dev_t *rtfifo_dev[2];

static int rtfifo_TrigDspIsr(struct rtfifo_dev_t *dev)
{
	nrw_mbox_send(dev->txmbox, 0x1);

	return 0;
}

void rtfifo_isr(void *dev_id)
{
	int i;
	struct rtfifo_dev_t *dev = dev_id;

	for (i = 0; i < dev->mdev_count; i++) {
		if (dev->mdev[i].rtfifo->u8ArmIntFlag) {
			dev->mdev[i].rtfifo->u8ArmIntReq = 0;
			dev->mdev[i].rtfifo->u8ArmIntFlag = 0;
			wake_up_interruptible(&dev->mdev[i].waitq);
		}
	}
	return;
}

static void alarm_trigger(unsigned long arg)
{
	struct rtfifo_mdev_t *mdev = (struct rtfifo_mdev_t *)arg;

	unsigned long now;
	unsigned long expire;

	now = jiffies;
	expire = now + HZ; /* delay of 1 second */

	mdev->timeout = 1;
	mdev->alarm_clock.expires = expire;
	wake_up_interruptible(&mdev->waitq);
}

static void set_alarm_clock(struct rtfifo_mdev_t *mdev)
{
	unsigned long now = jiffies;
	unsigned long expire = now + HZ; /* delay of 1 second */

	init_timer(&mdev->alarm_clock);

	mdev->timeout = 0;
	mdev->alarm_clock.function = alarm_trigger;
	mdev->alarm_clock.expires = expire;
	mdev->alarm_clock.data = (unsigned long)mdev;

	add_timer(&mdev->alarm_clock);
}

#ifdef CONFIG_PROC_FS
static void *rtfifo_seq_start(struct seq_file *m, loff_t *pos)
{
	return *pos < 1 ? (void *)1 : NULL;
}

static void *rtfifo_seq_next(struct seq_file *m, void *v, loff_t *pos)
{
	++*pos;
	return NULL;
}

static void rtfifo_seq_stop(struct seq_file *m, void *v)
{
}

static int rtfifo_proc_show(struct seq_file *m, void *v)
{
	int i;
	struct rtfifo_dev_t *dev = m->private;

	for (i = 0; i < dev->mdev_count; i++) {

		seq_printf(m, "\nRTFIFO #%d/%d\n===========\n", i+1, dev->mdev_count);
                if (dev->mdev[i].shutdown == 1 || dev->mdev[i].shuterr == 1) {
		    seq_printf(m, "\tstatus : ERROR\n");
		    seq_printf(m, "\tdevname: %s\n", dev->mdev[i].devName);
                }
                else {
		    seq_printf(m, "\tstatus : OK\n");
		    seq_printf(m, "\tdevname: %s\n", dev->mdev[i].devName);
		    seq_printf(m, "\taddr   : 0x%p\n", dev->mdev[i].rtfifo);
		    seq_printf(m, "\tname   : %s\n", dev->mdev[i].rtfifo->szName);
		    seq_printf(m, "\tdir    : %s\n", RTFIFO_GET_SRCPROC(dev->mdev[i].rtfifo) == MyhostId ? "ARM -> DSP" : "DSP -> ARM");
		    seq_printf(m, "\tsize   : %d bytes\n", dev->mdev[i].rtfifo->u32Size);
		    seq_printf(m, "\thead   : %d\n", dev->mdev[i].rtfifo->u32Head);
		    seq_printf(m, "\ttail   : %d\n", dev->mdev[i].rtfifo->u32Tail);
		    seq_printf(m, "\tirq    : ARM %d:%d  DSP %d:%d\n", dev->mdev[i].rtfifo->u8ArmIntReq,
			dev->mdev[i].rtfifo->u8ArmIntFlag, dev->mdev[i].rtfifo->u8DspIntReq,
			dev->mdev[i].rtfifo->u8DspIntFlag );
         }
	}
	return 0;
}

static const struct seq_operations rtfifo_proc_op = {
	.start	= rtfifo_seq_start,
	.next	= rtfifo_seq_next,
	.stop	= rtfifo_seq_stop,
	.show	= rtfifo_proc_show
};

static int rtfifo_proc_open(struct inode *inode, struct file *file)
{
	int ret;
	struct seq_file *m;
	struct rtfifo_dev_t *dev;

	dev = PDE_DATA(inode);

	if (!dev)
		return -ENOENT;

	ret = seq_open(file, &rtfifo_proc_op);
	if (ret < 0)
		return ret;

	m = file->private_data;
	m->private = dev;

	return 0;
}

static ssize_t rtfifo_proc_write(struct file *file, const char __user *buf,
			       size_t size, loff_t *ppos)
{
	char *kbuf;
	int ret = 0;

	if (size <= 1 || size >= PAGE_SIZE)
		return -EINVAL;

	kbuf = kmalloc(size + 1, GFP_KERNEL);
	if (!kbuf)
		return -ENOMEM;

	if (copy_from_user(kbuf, buf, size) != 0) {
		kfree(kbuf);
		return -EFAULT;
	}
	kbuf[size] = 0;

	if (0 == 1) {
	}
	else {
		printk(KERN_ERR "rtfifo: Invalid Command (%s)\n", kbuf);
		kfree(kbuf);
		return -EINVAL;
	}

	ret = size;

	kfree(kbuf);
	return ret;
}

static const struct file_operations rtfifo_proc_fops = {
	.open		= rtfifo_proc_open,
	.write		= rtfifo_proc_write,
	.read		= seq_read,
	.llseek		= seq_lseek,
	.release	= seq_release,
	.owner		= THIS_MODULE,
};

#endif /* CONFIG_PROC_FS */

static int rtfifo_open(struct inode *inode, struct file *filp)
{
	int i, j;
	struct rtfifo_mdev_t *mdev = NULL;

	for (i = 0; i < sizeof(rtfifo_dev)/sizeof(rtfifo_dev[0]) && !mdev; i++) {
		for (j = 0; j < rtfifo_dev[i]->mdev_count; j++) {
			if (rtfifo_dev[i]->mdev[j].dev.minor == MINOR(inode->i_rdev)) {
				mdev = &rtfifo_dev[i]->mdev[j];
				break;
			}
		}
	}

	if (mdev == NULL) {
		printk(KERN_ERR "rtfifo: Invalid minor device\n");
		return -ENODEV;
	}

        // This protection allows to check for a current error and clean any past error if there
        mutex_lock(&mdev->lock);
       
        // Rtfifo is currently in error (do not allow to open it)
        if (mdev->shutdown == 1) {
                mutex_unlock(&mdev->lock);
                //printk(KERN_ERR "rtfifo_open: Failed: shutdown\n");
		return -ENOLINK;
        }

        // If the fifo got in error before we opened it, ignore that error
        if (mdev->shuterr == 1) {
	    mdev->shuterr = 0;
        }
        mutex_unlock(&mdev->lock);


	filp->private_data = mdev;

	return 0;
}

static int rtfifo_release(struct inode *inode, struct file *filp)
{
	struct rtfifo_mdev_t *mdev = filp->private_data;
        mdev->shuterr = 0;
	return 0;
}

static int rtfifo_read(struct file *filp, char *buf, size_t count, loff_t *ppos)
{
	int n;
	int ret;
	wait_queue_t wq;
	struct rtfifo_mdev_t *mdev = filp->private_data;

        if (mdev->shutdown == 1 || mdev->shuterr == 1) {
            if (mdev->shuterr == 1) {
	        printk(KERN_ERR "rtfifo_read: Failed: shutdown\n");
            }
	    mdev->shuterr = 0;
	    return -ENOLINK;
        }

	mutex_lock(&mdev->rd_lock);
	while (mdev->rtfifo->u32Tail == mdev->rtfifo->u32Head) {

		if (filp->f_flags & O_NONBLOCK) {
			mutex_unlock(&mdev->rd_lock);
			return -EAGAIN;
		}

		init_waitqueue_entry( &wq, current );

		/* Ask the DSP to be interrupted */
		mdev->rtfifo->u8ArmIntReq = 1;

		/* Set alarm clock before going to sleep,
		this prevent us from sleeping indefinitely */
		set_alarm_clock(mdev);

		ret = wait_event_interruptible(mdev->waitq,
			mdev->rtfifo->u32Tail != mdev->rtfifo->u32Head ||
			mdev->rtfifo->u8ArmIntReq == 0 || mdev->timeout == 1 || mdev->shutdown == 1 || mdev->shuterr == 1);

		del_timer(&mdev->alarm_clock);

		if (ret < 0) {
			printk(KERN_INFO "rtfifo_read: Failed to wait for event (%d)\n", ret );
			mutex_unlock(&mdev->rd_lock);
			return ret;
		}

                if (mdev->shutdown == 1 || mdev->shuterr == 1) {
                    if (mdev->shuterr == 1) {
			printk(KERN_ERR "rtfifo_read: Failed to wait for event: shutdown\n");
                    }
		    mdev->shuterr = 0;
		    mutex_unlock(&mdev->rd_lock);
		    return -ENOLINK;
                }
	}

	for (n=0; n<count;) {
		u32 tail = mdev->rtfifo->u32Tail;
		u32 head = mdev->rtfifo->u32Head;

		/* If no data available ... */
		if (tail == head) {
			/* return */
			mutex_unlock(&mdev->rd_lock);
			return n;
		}

		if (tail > head) {
			if ( (count-n) < (mdev->rtfifo->u32Size - tail) ) {
				ret = copy_to_user(&buf[n], (void *)&mdev->rtfifo->pu8Data[tail], count-n);
				if (ret) {
					mutex_unlock(&mdev->rd_lock);
					return ret;
				}
				mdev->rtfifo->u32Tail = tail + (count - n);
				n += (count - n);
			} else {
				ret = copy_to_user(&buf[n], (void *)&mdev->rtfifo->pu8Data[tail],
									mdev->rtfifo->u32Size-tail);
				if (ret) {
					mutex_unlock(&mdev->rd_lock);
					return ret;
				}
				mdev->rtfifo->u32Tail = 0;
				n += (mdev->rtfifo->u32Size - tail);
			}
		} else {
			if ((count-n) < (head-tail)) {
				ret = copy_to_user(&buf[n], (void *)&mdev->rtfifo->pu8Data[tail], count-n );
				mdev->rtfifo->u32Tail = tail + (count - n);
				n += (count - n);
			} else {
				ret = copy_to_user(&buf[n], (void *)&mdev->rtfifo->pu8Data[tail], head - tail );
				mdev->rtfifo->u32Tail = head;
				n += (head - tail);
			}
		}
	}
	mutex_unlock(&mdev->rd_lock);

	/* Verify if the DSP wants to be interrupted */
	if (n && mdev->rtfifo->u8DspIntReq) {

		/* Interrupt the DSP */
		mdev->rtfifo->u8DspIntFlag = 1;
		rtfifo_TrigDspIsr(mdev->parent);
	}

	return n;
}

static int rtfifo_write(struct file *filp, const char *buf,
				size_t count, loff_t *ppos)
{
	int n;
	int ret;
	uint32_t head;
	uint32_t tail;
	uint32_t nextHead;
	wait_queue_t wq;
	struct rtfifo_mdev_t *mdev = filp->private_data;

        if (mdev->shutdown == 1 || mdev->shuterr == 1) {
            if (mdev->shuterr == 1) {
		printk(KERN_ERR "rtfifo_write: Failed: shutdown\n");
            }
            mdev->shuterr = 0;
	    return -ENOLINK;
        }

	mutex_lock(&mdev->wr_lock);

	head = mdev->rtfifo->u32Head;
	nextHead = ((head+1) >= mdev->rtfifo->u32Size) ? 0 : (head+1);

	/* Look if there is room available */
	while (nextHead == mdev->rtfifo->u32Tail) {
		if (filp->f_flags & O_NONBLOCK) {
			mutex_unlock(&mdev->wr_lock);
			return -EAGAIN;
		}

		init_waitqueue_entry( &wq, current );

		/* Ask the DSP to be interrupted */
		mdev->rtfifo->u8ArmIntReq = 1;

		/* Set alarm clock before going to sleep,
		this prevent us from sleeping indefinitely */
		set_alarm_clock(mdev);

		ret = wait_event_interruptible(mdev->waitq,
			nextHead != mdev->rtfifo->u32Tail ||
			mdev->rtfifo->u8ArmIntReq == 0 || mdev->timeout == 1 || mdev->shutdown == 1 || mdev->shuterr == 1);

		del_timer(&mdev->alarm_clock);

		if (ret < 0) {
			printk(KERN_INFO "rtfifo_write: Failed to wait for event (%d)\n", ret );
			mutex_unlock(&mdev->wr_lock);
			return ret;
		}

                if (mdev->shutdown == 1 || mdev->shuterr == 1) {
                    if (mdev->shuterr == 1) {
			printk(KERN_ERR "rtfifo_write: Failed to wait for event: shutdown\n");
                    }
                    mdev->shuterr = 0;
		    mutex_unlock(&mdev->wr_lock);
		    return -ENOLINK;
                }
	}

	for (n=0; n<count;) {

		tail = mdev->rtfifo->u32Tail;
		head = mdev->rtfifo->u32Head;
		nextHead = ((head+1) >= mdev->rtfifo->u32Size) ? 0 : (head+1);

		/* If no data available ... */
		if (nextHead == tail) {
			/* return */
			mutex_unlock(&mdev->wr_lock);
			return n;
		}

		if (head >= tail) {
			if ((count-n) < (mdev->rtfifo->u32Size - head)) {
				if (copy_from_user((void *)&mdev->rtfifo->pu8Data[head], &buf[n], count-n)) {
					mutex_unlock(&mdev->wr_lock);
					return -EFAULT;
				}
				mdev->rtfifo->u32Head = head + (count - n);
				n += (count - n);
			} else {
				if (copy_from_user((void *)&mdev->rtfifo->pu8Data[head], &buf[n],
									mdev->rtfifo->u32Size-head)) {
					mutex_unlock(&mdev->wr_lock);
					return -EFAULT;
				}
				mdev->rtfifo->u32Head = 0;
				n += (mdev->rtfifo->u32Size - head);
			}
		} else {
			if ((count-n) < (tail-head)) {
				if (copy_from_user((void *)&mdev->rtfifo->pu8Data[head], &buf[n], count-n)) {
					mutex_unlock(&mdev->wr_lock);
					return -EFAULT;
				}
				mdev->rtfifo->u32Head = head + (count - n);
				n += (count - n);
			} else {
				if (copy_from_user((void *)&mdev->rtfifo->pu8Data[head], &buf[n], (tail - head) - 1)) {
					mutex_unlock(&mdev->wr_lock);
					return -EFAULT;
				}
				mdev->rtfifo->u32Head = tail - 1;
				n += (tail - head) - 1;
			}
		}
	}
	mutex_unlock(&mdev->wr_lock);

	/* Verify if the DSP wants to be interrupted */
	if (n && mdev->rtfifo->u8DspIntReq) {

		mdev->rtfifo->u8DspIntFlag = 1;
		rtfifo_TrigDspIsr(mdev->parent);
	}

	return n;
}

static const struct file_operations rtfifo_fops = {
	.open		= rtfifo_open,
	.release	= rtfifo_release,
	.read		= rtfifo_read,
	.write		= rtfifo_write,
	.owner		= THIS_MODULE,
};

static struct rtfifo_dev_t *rtfifo_probe(void *va, int len)
{
	int i;
	int ret = -ENODEV;
	struct rtfifo_t *pFifo, *start_Fifo = NULL;
	struct rtfifo_dev_t *dev = NULL;
	struct proc_dir_entry *ent = NULL;

	/* The memory is set to zero by kzalloc */
	dev = kzalloc(sizeof(*dev), GFP_KERNEL);
	if (!dev) {
		printk(KERN_ERR "rtfifo: Failed to allocate device structure\n");
		ret = -ENOMEM;
		goto error_alloc_dev;
	}

	/* Map RTFIFO memory section */
	dev->mmio = va;
        dev->mmiolen = len;
	//printk(KERN_INFO "rtfifo: va: %x, len: %d\n", (unsigned)(va), len);
	if (!dev->mmio) {
		printk(KERN_ERR "rtfifo: Failed to I/O remap RTFIFO memory\n");
		ret = -ENOMEM;
		goto error_ioremap;
	}

	/* Count the number of fifos */
	pFifo = (struct rtfifo_t *)dev->mmio;
	for (i = sizeof(struct rtfifo_t); i < len; ) {

		if (i + pFifo->u32Size > len) {
			break;
		}

		if (pFifo->u32MagicId == 0x4649464F/*'FIFO'*/) {

			if (dev->mdev_count == 0) {
				start_Fifo = pFifo;
			}

			dev->mdev_count++;
		}

		i += (sizeof(struct rtfifo_t) + pFifo->u32Size + 0x7F) & ~0x7F;
		pFifo = (struct rtfifo_t *)((u32)pFifo + ((sizeof(struct rtfifo_t) + pFifo->u32Size + 0x7F) & ~0x7F));
	}

	if (dev->mdev_count == 0) {
		printk(KERN_ERR "rtfifo: No valid rtfifo found\n");
		ret = -EFAULT;
		goto error_nofifo;
	}

	/* Alloc memory for each fifo misc device */
	dev->mdev = kzalloc(dev->mdev_count * sizeof(struct rtfifo_mdev_t), GFP_KERNEL);
	if (!dev->mdev) {
		printk(KERN_ERR "rtfifo: Failed to allocate misc device structure\n");
		ret = -ENOMEM;
		goto error_alloc_mdev;
	}

	/* Init each fifo device */
	pFifo = start_Fifo;
	for (i = 0; i < dev->mdev_count && pFifo; i++) {
		dev->mdev[i].shutdown = 0;
                dev->mdev[i].shuterr = 0;

		mutex_init(&dev->mdev[i].wr_lock);
		mutex_init(&dev->mdev[i].rd_lock);
		//spin_lock_init(&dev->mdev[i].lock);
		mutex_init(&dev->mdev[i].lock);
		init_waitqueue_head(&dev->mdev[i].waitq);

		dev->mdev[i].rtfifo = pFifo;
		snprintf(dev->mdev[i].devName, sizeof(dev->mdev[i].devName), "rtfifo/%s", pFifo->szName);

		pFifo = (struct rtfifo_t *)((u32)pFifo + ((sizeof(struct rtfifo_t) + pFifo->u32Size + 0x7F) & ~0x7F));

		/* Identify on which DSP we are and set the proper signaling */
		if ((RTFIFO_GET_DSTPROC(dev->mdev[i].rtfifo) == MyhostId && RTFIFO_GET_SRCPROC(dev->mdev[i].rtfifo) == Mydsp1Id)
			|| (RTFIFO_GET_DSTPROC(dev->mdev[i].rtfifo) == Mydsp1Id && RTFIFO_GET_SRCPROC(dev->mdev[i].rtfifo) == MyhostId)) {

			dev->rxmbox = nrw_mbox_lookup("mbox_mpu_10");
			if (dev->rxmbox == NULL) {
				printk(KERN_ERR "rtfifo: mbox 'mbox_mpu_10' not found\n");
				goto error_mbox;
			}

			dev->txmbox = nrw_mbox_lookup("mbox_dsp1_10");
			if (dev->txmbox == NULL) {
				printk(KERN_ERR "rtfifo: mbox 'mbox_dsp1_10' not found\n");
				goto error_mbox;
			}

			dev->proc_id = 1;
		}

		if ((RTFIFO_GET_DSTPROC(dev->mdev[i].rtfifo) == MyhostId && RTFIFO_GET_SRCPROC(dev->mdev[i].rtfifo) == Mydsp2Id)
			|| (RTFIFO_GET_DSTPROC(dev->mdev[i].rtfifo) == Mydsp2Id && RTFIFO_GET_SRCPROC(dev->mdev[i].rtfifo) == MyhostId)) {

			dev->rxmbox = nrw_mbox_lookup("mbox_mpu_11");
			if (dev->rxmbox == NULL) {
				printk(KERN_ERR "rtfifo: mbox 'mbox_mpu_11' not found\n");
				goto error_mbox;
			}

			dev->txmbox = nrw_mbox_lookup("mbox_dsp2_11");
			if (dev->txmbox == NULL) {
				printk(KERN_ERR "rtfifo: mbox 'mbox_dsp2_11' not found\n");
				goto error_mbox;
			}

			dev->proc_id = 2;
		}

		if (dev->proc_id > 0) {

			dev->mdev[i].parent = dev;
			dev->mdev[i].dev.name  = dev->mdev[i].devName;
			dev->mdev[i].dev.minor = MISC_DYNAMIC_MINOR;
			dev->mdev[i].dev.fops  = &rtfifo_fops;
			ret = misc_register(&dev->mdev[i].dev);

			if (ret < 0) {
				printk(KERN_ERR "rtfifo: Error duplicated queue name: %s\n", dev->mdev[i].devName);
				printk(KERN_ERR "rtfifo: Error registering misc driver\n");
				goto error_miscdev;
			}
		}
	}

	/* Set the global device structure pointer */
	if (dev->proc_id > 0) {

		if (rtfifo_dev[dev->proc_id-1] == NULL) {

			/* Register IRQ */
			printk(KERN_INFO "rtfifo: proc_id(%d) registering mbox begin.\n", dev->proc_id);
			ret = nrw_mbox_register(dev->rxmbox, rtfifo_isr, dev);
			if (ret != 0) {
				printk(KERN_ERR "rtfifo: proc_id(%d) rxmbox register failed\n", dev->proc_id);
				goto error_mbox_register;
			}
			else
				printk(KERN_INFO "rtfifo: proc_id(%d) registering mbox end.\n", dev->proc_id);

			dev->rxmboxreg = 1;
			rtfifo_dev[dev->proc_id-1] = dev;
		}
	}

	/* Create PROC Entry */
	#ifdef CONFIG_PROC_FS
	{
		if (dev->proc_id == 1)
			ent = proc_create_data("rtfifo1", 0, NULL, &rtfifo_proc_fops, dev);
		if (dev->proc_id == 2)
			ent = proc_create_data("rtfifo2", 0, NULL, &rtfifo_proc_fops, dev);

		if (ent == NULL) {
			printk(KERN_ERR "rtfifo: failed to create proc entry\n");
		}
	}
	#endif

	printk(KERN_INFO "rtfifo: probed\n");
	return dev;

error_mbox_register:
error_miscdev:
	for (i = 0; i < dev->mdev_count; i++) {
		if (dev->mdev[i].rtfifo != NULL) {
			misc_deregister(&dev->mdev[i].dev);
		}
	}
error_mbox:
	kfree(dev->mdev);
error_alloc_mdev:
error_nofifo:
error_ioremap:
	kfree(dev);
error_alloc_dev:
	printk(KERN_ERR "rtfifo: probe failed\n");
	return NULL;
}

static int rtfifo_remove(struct rtfifo_dev_t *dev)
{
	int i;

	#ifdef CONFIG_PROC_FS
	{
		if (dev->proc_id == 1)
			remove_proc_entry("rtfifo1", NULL);
		if (dev->proc_id == 2)
			remove_proc_entry("rtfifo2", NULL);
	}
	#endif

	if (dev->proc_id == 1 || dev->proc_id == 2) {

        if (dev->rxmboxreg != 0)
		nrw_mbox_deregister(dev->rxmbox, dev);

		rtfifo_dev[dev->proc_id-1] = NULL;

		// Go in shutdown mode for each fifo and wakeup any user process that could be sleeping in our driver code
                for (i = 0; i < dev->mdev_count; i++) {
			if (dev->mdev[i].rtfifo != NULL) {
                                mutex_lock(&dev->mdev[i].lock);
                                dev->mdev[i].shuterr = 1;
				dev->mdev[i].shutdown = 1;
                                mutex_unlock(&dev->mdev[i].lock);
	                        wake_up_interruptible(&dev->mdev[i].waitq);
			}
		}
		// This could cause a problem if application is still using us
		for (i = 0; i < dev->mdev_count; i++) {
			if (dev->mdev[i].rtfifo != NULL) {
                                misc_deregister(&dev->mdev[i].dev);
			}
		}
	}
	dev->proc_id = 0;

	kfree(dev->mdev);
	kfree(dev);
	return 0;
}

static int rtfifo_pauseio(struct rtfifo_dev_t *dev)
{
	int i;

	if (dev->proc_id == 1 || dev->proc_id == 2) {

		// Unregister mbx irq if we are not already paused
		if (dev->rxmboxreg != 0)
                {
                    // Unregister mbx irq
		    printk(KERN_INFO "rtfifo: pause proc_id(%d) deregistering mbox begin.\n", dev->proc_id);
		    nrw_mbox_deregister(dev->rxmbox, dev);
		    printk(KERN_INFO "rtfifo: pause proc_id(%d) deregistering mbox end.\n", dev->proc_id);

                    dev->rxmboxreg = 0;

		    // Go in shutdown mode for each fifo and wakeup any user process that could be sleeping in our driver code
                    for (i = 0; i < dev->mdev_count; i++) {
			if (dev->mdev[i].rtfifo != NULL) {
                            mutex_lock(&dev->mdev[i].lock);
                            dev->mdev[i].shuterr = 1;
			    dev->mdev[i].shutdown = 1;
                            mutex_unlock(&dev->mdev[i].lock);
	                    wake_up_interruptible(&dev->mdev[i].waitq);
		        }
		    }
                }
	}
	//dev->proc_id = 0;

	return 0;
}

// This function will attempt to reassign correct addresses for rtfifo when the same firmware  (or with exact same fifo definitions) code is reloaded
// It may fail if a different firmware is used than what was used at rtfifo_probe time
static int rtfifo_reprobe(void *va, int len, void *_dev)
{
	int i;
	int ret = 0;
	struct rtfifo_t *pFifo, *start_Fifo = NULL;
	struct rtfifo_dev_t *dev = NULL;
	//struct proc_dir_entry *ent = NULL;
	struct rtfifo_dev_t *probedev = _dev;
        unsigned offset;

       // Checks if probed ptr make sense
       if (!probedev || !probedev->mmio) {
		printk(KERN_ERR "rtfifo: Failed reprobe RTFIFO memory\n");
		ret = -EFAULT;
                goto error_alloc_dev;
	}

	// Checks if rtfifo section is the same length than what it was at 1st probe time (could detect different firmware)
        if (probedev->mmiolen != len) {
		printk(KERN_ERR "rtfifo: reprobe:%d probed:%d\n",len, probedev->mmiolen);
		printk(KERN_ERR "rtfifo: section size different than probed section\n");
		ret = -EFAULT;
                goto error_alloc_dev;
        }

        // Compute offset between reprobe va and probed va just in case they are differents
        offset = (unsigned)(va) - (unsigned)(probedev->mmio);


	/* The memory is set to zero by kzalloc */
	dev = kzalloc(sizeof(*dev), GFP_KERNEL);
	if (!dev) {
		printk(KERN_ERR "rtfifo: Failed to allocate reprobe device structure\n");
		ret = -ENOMEM;
		goto error_alloc_dev;
	}

	/* Map RTFIFO memory section */
	dev->mmio = va;
        dev->mmiolen = len;
	//printk(KERN_INFO "rtfifo: reprobe va: %x, len: %d\n", (unsigned)(va), len);
	if (!dev->mmio) {
		printk(KERN_ERR "rtfifo: Failed to I/O remap reprobe RTFIFO memory\n");
		ret = -ENOMEM;
		goto error_ioremap;
	}

	/* Count the number of fifos */
	pFifo = (struct rtfifo_t *)dev->mmio;
	for (i = sizeof(struct rtfifo_t); i < len; ) {

		if (i + pFifo->u32Size > len) {
			break;
		}

		if (pFifo->u32MagicId == 0x4649464F/*'FIFO'*/) {

			if (dev->mdev_count == 0) {
				start_Fifo = pFifo;
			}

			dev->mdev_count++;
		}

		i += (sizeof(struct rtfifo_t) + pFifo->u32Size + 0x7F) & ~0x7F;
		pFifo = (struct rtfifo_t *)((u32)pFifo + ((sizeof(struct rtfifo_t) + pFifo->u32Size + 0x7F) & ~0x7F));
	}

	if (dev->mdev_count == 0) {
		printk(KERN_ERR "rtfifo: No valid rtfifo found\n");
		ret = -EFAULT;
		goto error_nofifo;
	}

	// Checks if the same number of fifo was found than at probe time
        if (dev->mdev_count != probedev->mdev_count) {
		printk(KERN_ERR "rtfifo: reprobe:%d probed:%d\n",dev->mdev_count, probedev->mdev_count);
		printk(KERN_ERR "rtfifo: different number of reprobe rtfifo found\n");
		ret = -EFAULT;
		goto error_nofifo;
	}

	/* Alloc memory for each fifo misc device */
	dev->mdev = kzalloc(dev->mdev_count * sizeof(struct rtfifo_mdev_t), GFP_KERNEL);
	if (!dev->mdev) {
		printk(KERN_ERR "rtfifo: Failed to allocate reprobe misc device structure\n");
		ret = -ENOMEM;
		goto error_alloc_mdev;
	}

	/* Init each fifo device */
	pFifo = start_Fifo;

	// Check each reprobe fifo name and address
	for (i = 0; i < dev->mdev_count && pFifo; i++) {

		dev->mdev[i].rtfifo = pFifo;
		snprintf(dev->mdev[i].devName, sizeof(dev->mdev[i].devName), "rtfifo/%s", pFifo->szName);

		pFifo = (struct rtfifo_t *)((u32)pFifo + ((sizeof(struct rtfifo_t) + pFifo->u32Size + 0x7F) & ~0x7F));

		/* Identify on which DSP we are and set the proper signaling */
		if ((RTFIFO_GET_DSTPROC(dev->mdev[i].rtfifo) == MyhostId && RTFIFO_GET_SRCPROC(dev->mdev[i].rtfifo) == Mydsp1Id)
			|| (RTFIFO_GET_DSTPROC(dev->mdev[i].rtfifo) == Mydsp1Id && RTFIFO_GET_SRCPROC(dev->mdev[i].rtfifo) == MyhostId)) {

			dev->proc_id = 1;
		}

		if ((RTFIFO_GET_DSTPROC(dev->mdev[i].rtfifo) == MyhostId && RTFIFO_GET_SRCPROC(dev->mdev[i].rtfifo) == Mydsp2Id)
			|| (RTFIFO_GET_DSTPROC(dev->mdev[i].rtfifo) == Mydsp2Id && RTFIFO_GET_SRCPROC(dev->mdev[i].rtfifo) == MyhostId)) {

			dev->proc_id = 2;
		}

		if (dev->proc_id > 0) {

			dev->mdev[i].parent = dev;
			dev->mdev[i].dev.name  = dev->mdev[i].devName;
		}
	}

	// Check each reprobed fifo to see if they are the same as probed ones
        for (i = 0; i < dev->mdev_count; i++) {
            // If the reprobed rtfifo name the same as probed one
            if (strncmp(dev->mdev[i].dev.name, probedev->mdev[i].dev.name,sizeof(dev->mdev[i].devName)) != 0) {
		printk(KERN_ERR "rtfifo: reprobe:%s probed:%s\n",dev->mdev[i].dev.name,probedev->mdev[i].dev.name);
                printk(KERN_ERR "rtfifo: Error reprobe different rtfifo name than probed one\n");
		ret = -EFAULT;
	        goto error_miscdev;
            }
            
            // Check if the reprobe rtfifo is relatively (within offset) at the same address as probed ones
            if (dev->mdev[i].rtfifo != ((volatile struct rtfifo_t __iomem *)((unsigned) probedev->mdev[i].rtfifo + offset))) {
		printk(KERN_ERR "rtfifo: count:%d name:%s reprobe:0x%x probed:0x%x\n",i,probedev->mdev[i].dev.name,(unsigned)dev->mdev[i].rtfifo, (unsigned)((volatile struct rtfifo_t __iomem *)((unsigned) probedev->mdev[i].rtfifo + offset)));
                printk(KERN_ERR "rtfifo: Error reprobe different rtfifo address than probed one\n");
		ret = -EFAULT;
	        goto error_miscdev;
            }
        }

	// Reajust all probed rtfifo addresses if now offseted from reprobed one, and remove shutdown status
        for (i = 0; i < dev->mdev_count ; i++) {
            if (offset != 0)
                probedev->mdev[i].rtfifo = (volatile struct rtfifo_t __iomem *)((unsigned) probedev->mdev[i].rtfifo + offset);
            mutex_lock(&probedev->mdev[i].lock);
	    probedev->mdev[i].shutdown = 0;
            mutex_unlock(&probedev->mdev[i].lock);
        }

	/* Update the old buffer address to the newly allocated one */
	if (offset != 0)
		probedev->mmio = (void *)(va);

	/* Restart irq if not already done */
	if (dev->proc_id > 0) {

		if (probedev->rxmboxreg == 0) {

			/* Register IRQ */
			printk(KERN_INFO "rtfifo: reprobe proc_id(%d) registering mbox begin.\n", dev->proc_id);
			ret = nrw_mbox_register(probedev->rxmbox, rtfifo_isr, probedev);
			if (ret != 0) {
				printk(KERN_ERR "rtfifo: reprobe proc_id(%d) rxmbox register failed\n", dev->proc_id);
				goto error_mbox_register;
			}
			else
				printk(KERN_INFO "rtfifo: reprobe proc_id(%d) registering mbox end.\n", dev->proc_id);

			probedev->rxmboxreg = 1;
		}
	}

	kfree(dev->mdev);
	kfree(dev);
	printk(KERN_INFO "rtfifo: reprobed\n");
	return ret;

error_mbox_register:
error_miscdev:
//error_mbox:
	kfree(dev->mdev);
error_alloc_mdev:
error_nofifo:
error_ioremap:
	kfree(dev);
error_alloc_dev:
	printk(KERN_ERR "rtfifo: reprobe failed\n");
	return ret;
}


void *rtfifo_start(void *va, int len)
{
	return rtfifo_probe(va, len);
}
EXPORT_SYMBOL(rtfifo_start);

// To use this, application must be disconnected from the driver
int rtfifo_stop(void *_dev)
{
	struct rtfifo_dev_t *dev = _dev;
	return rtfifo_remove(dev);
}

EXPORT_SYMBOL(rtfifo_stop);

int rtfifo_restart(void *va, int len, void *_dev)
{
	struct rtfifo_dev_t *dev = _dev;

        //printk(KERN_INFO "rtfifo_restart start...\n");
	return rtfifo_reprobe(va, len, dev);
}

EXPORT_SYMBOL(rtfifo_restart);

int rtfifo_pause(void *_dev)
{
	struct rtfifo_dev_t *dev = _dev;

        //printk(KERN_INFO "rtfifo_pause start...\n");
	return rtfifo_pauseio(dev);
}

EXPORT_SYMBOL(rtfifo_pause);

static int __init rtfifo_init(void)
{
	return 0;
}

static void __exit rtfifo_cleanup(void)
{
}

module_init(rtfifo_init);
module_exit(rtfifo_cleanup);

MODULE_DESCRIPTION("RT-FIFO Driver");
MODULE_AUTHOR("Nutaq Inc. <www.nutaq.com>");
MODULE_LICENSE("GPL");
MODULE_ALIAS("platform:" MODULE_NAME);


