#ifndef NRW_MBOX_H
#define NRW_MBOX_H

struct nrw_mbox;

void nrw_mbox_send(struct nrw_mbox *mbox, unsigned int b);
void nrw_mbox_deregister(struct nrw_mbox *mbox, void *pv);
int nrw_mbox_register(struct nrw_mbox *mbox, void *msgq_cb, void *pv);
struct nrw_mbox *nrw_mbox_lookup(const char *mbox_dev_name);

#endif
