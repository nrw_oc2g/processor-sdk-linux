#ifndef MSGQUEUE_H
#define MSGQUEUE_H

void *msgqueue_start(void *va, int len);
int msgqueue_stop(void *_dev);
int msgqueue_restart(void *va, int len, void *_dev);
int msgqueue_pause(void *_dev);

#endif

