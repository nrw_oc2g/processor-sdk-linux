/* This module is based on omap-remoteproc */

#include <linux/mutex.h>
#include <linux/of_device.h>
#include <linux/platform_device.h>
#include <linux/interrupt.h>
#include <linux/io.h>
#include <linux/module.h>
#include <linux/slab.h>

#define MAILBOX_REVISION	0x000
#define MAILBOX_MESSAGE(m)	(0x040 + 4 * (m))
#define MAILBOX_FIFOSTATUS(m)	(0x080 + 4 * (m))
#define MAILBOX_MSGSTATUS(m)	(0x0c0 + 4 * (m))

#define MAILBOX_IRQSTATUS(u)	(0x104 + 0x10 * (u))
#define MAILBOX_IRQENABLE(u)	(0x108 + 0x10 * (u))
#define MAILBOX_IRQDISABLE(u)	(0x10c + 0x10 * (u))

#define MAILBOX_IRQ_NEWMSG(m)	(1 << (2 * (m)))
#define MAILBOX_IRQ_NOTFULL(m)	(1 << (2 * (m) + 1))

#define MAX_INST_CALLBACK 2

struct nrw_mbox_fifo_info {

	int queue;
	int user;

	const char *name;
	bool send_no_irq;
};

struct nrw_mbox_fifo {

	unsigned long msg;
	unsigned long fifo_stat;
	unsigned long msg_stat;
	unsigned long irqenable;
	unsigned long irqstatus;
	unsigned long irqdisable;
	u32 intr_bit;
};

struct nrw_mbox_cb {

	void (*cb)(void *);
	void *pv;
};

struct nrw_mbox {

	const char *name;
	struct nrw_mbox_fifo_info finfo;
	struct nrw_mbox_fifo fifo;

	struct mutex list_cb_lock;
	volatile struct nrw_mbox_cb list_cb[MAX_INST_CALLBACK];
	volatile unsigned list_cb_pos;

	int irq;
	struct nrw_mbox_device *parent;

};

struct nrw_mbox_device {

	const char *name;
	struct device *dev;
	void __iomem *mbox_base;
	struct nrw_mbox **mboxes;
	struct list_head elem;
};

/* gobal variables for the mailbox devices */
static DEFINE_MUTEX(nrw_mbox_devices_lock);
static LIST_HEAD(nrw_mbox_devices);

static inline
unsigned int mbox_read_reg(struct nrw_mbox_device *mdev, size_t ofs)
{
	return __raw_readl(mdev->mbox_base + ofs);
}

static inline
void mbox_write_reg(struct nrw_mbox_device *mdev, u32 val, size_t ofs)
{
	__raw_writel(val, mdev->mbox_base + ofs);
}

static void mbox_fifo_write(struct nrw_mbox *mbox, unsigned int msg)
{
	struct nrw_mbox_fifo *fifo;
	fifo = &mbox->fifo;

	mbox_write_reg(mbox->parent, msg, fifo->msg);
}

static int mbox_fifo_empty(struct nrw_mbox *mbox)
{
	struct nrw_mbox_fifo *fifo;
	fifo = &mbox->fifo;

	return (mbox_read_reg(mbox->parent, fifo->msg_stat) == 0);
}

static int mbox_write(struct nrw_mbox *mbox, unsigned int msg)
{
	if (mbox_fifo_empty(mbox)) {
		mbox_fifo_write(mbox, msg);
	} else {
		return -1;
	}
	return 0;
}

static void ack_mbox_irq(struct nrw_mbox *mbox, int irq)
{
	struct nrw_mbox_fifo *fifo;

	u32 bit;
	u32 irqstatus;

	fifo = &mbox->fifo;

	bit = fifo->intr_bit;
	irqstatus = fifo->irqstatus;

	mbox_write_reg(mbox->parent, bit, irqstatus);
}

static void nrw_mbox_disable_irq(struct nrw_mbox *mbox, int irq)
{
	struct nrw_mbox_fifo *fifo;

	u32 l;
	u32 bit;
	u32 irqdisable;

	fifo = &mbox->fifo;

	bit = fifo->intr_bit;
	irqdisable = fifo->irqdisable;

	l = mbox_read_reg(mbox->parent, irqdisable);
	l &= ~bit;
	mbox_write_reg(mbox->parent, l, irqdisable);
}

static void nrw_mbox_enable_irq(struct nrw_mbox *mbox, int irq)
{
	struct nrw_mbox_fifo *fifo;

	u32 l;
	u32 bit;
	u32 irqenable;

	fifo = &mbox->fifo;

	bit = fifo->intr_bit;
	irqenable = fifo->irqenable;

	l = mbox_read_reg(mbox->parent, irqenable);
	l |= bit;
	mbox_write_reg(mbox->parent, l, irqenable);
}

static irqreturn_t mbox_interrupt_cb(int irq, void *priv)
{
	unsigned int b = 0;
	struct nrw_mbox *mbox = priv;
	int count;
	unsigned pos;

	b = mbox_read_reg(mbox->parent, mbox->fifo.msg);
	ack_mbox_irq(mbox, mbox->finfo.user);
	mbox_write_reg(mbox->parent, 1, 0x140); /* Mailbox End Of Interrupt */

	/* Search if this callback has been registered before? */
	for (count = 0, pos = 1; count < MAX_INST_CALLBACK; count++, pos<<=1) {
		if ((pos & mbox->list_cb_pos) != 0) {
			/* if registered function not empty? */
			if (mbox->list_cb[count].pv != 0 && mbox->list_cb[count].cb != 0) {
				/* calls the registered function */
				mbox->list_cb[count].cb(mbox->list_cb[count].pv);
			}
		}
	}

	return IRQ_HANDLED;
}

struct nrw_mbox *nrw_mbox_lookup(const char *mbox_dev_name)
{
	struct nrw_mbox_device *mdev = NULL;
	struct nrw_mbox *retmbox = NULL, *mbox = NULL;
	int i;

	mutex_lock(&nrw_mbox_devices_lock);

	list_for_each_entry(mdev, &nrw_mbox_devices, elem) {
		for (i = 0; (mbox = mdev->mboxes[i]); i++) {
			if (strcmp(mbox->name, mbox_dev_name) == 0) {
				retmbox = mbox;
				mbox = NULL;
				break;
			}
		}
        }

	mutex_unlock(&nrw_mbox_devices_lock);

	return retmbox;
}
EXPORT_SYMBOL(nrw_mbox_lookup);

void nrw_mbox_send(struct nrw_mbox *mbox, unsigned int b)
{
	if (mbox == NULL) {
		return;
	}

	mbox_write(mbox, b);
}
EXPORT_SYMBOL(nrw_mbox_send);

void nrw_mbox_deregister(struct nrw_mbox *mbox, void *pv)
{
	int count;
	unsigned pos;

	if (mbox == NULL) {
		return;
	}

	mutex_lock(&mbox->list_cb_lock);

/*
	printk(KERN_INFO "nrw_mbox_deregister mbox: 0x%08x, name: %s, pv: 0x%08x, pos: 0x%02x\n",(unsigned)(mbox),mbox->name,(unsigned)(pv),mbox->list_cb_pos);
*/
	/* Search if this callback has been registered before? */
	for (count = 0, pos = 1; count < MAX_INST_CALLBACK; count++, pos<<=1) {
		if ((pos & mbox->list_cb_pos) != 0) {
			if (mbox->list_cb[count].pv == pv) {
				break;
			}
		}
	}

	if (count >= MAX_INST_CALLBACK) {
		printk(KERN_WARNING "nrw_mbox_deregister element not found\n");
		mutex_unlock(&mbox->list_cb_lock);
		return;
	}

	/* Remove the registered element */
	mbox->list_cb_pos &= (~(pos));
	/* because of unprotected irq used of these, not resetting seems safer... */
	/*mbox->list_cb[count].pv = 0;
	mbox->list_cb[count].cb = 0;*/

	/* No more registered callback, disable irq...*/
	if (mbox->list_cb_pos == 0) {
		nrw_mbox_disable_irq(mbox, mbox->finfo.user);
		free_irq(mbox->irq, mbox);
	}

	mutex_unlock(&mbox->list_cb_lock);
}
EXPORT_SYMBOL(nrw_mbox_deregister);

int nrw_mbox_register(struct nrw_mbox *mbox, void *cb, void *pv)
{
	int ret = 0, count;
	unsigned pos;

	if (mbox == NULL) {
		return -1;
	}

	mutex_lock(&mbox->list_cb_lock);

	/* Any callback registered so far? */
	if (mbox->list_cb_pos == 0) {

		ret = request_irq(mbox->irq, mbox_interrupt_cb,
			IRQF_SHARED, mbox->name, mbox);

		if (ret != 0) {
			mutex_unlock(&mbox->list_cb_lock);
			return -1;
		}
	}

	/* Search for a free spot in the cb table */
	for (count = 0, pos = 1; count < MAX_INST_CALLBACK; count++, pos<<=1) {
		if ((pos & mbox->list_cb_pos) == 0)
			break;
	}

	if (count >= MAX_INST_CALLBACK) {
		mutex_unlock(&mbox->list_cb_lock);
		printk(KERN_ERR "nrw_mbox_register: no more cb room error\n");
		return -1;
	}

	mbox->list_cb[count].cb = cb;
	mbox->list_cb[count].pv = pv;

	if (mbox->list_cb_pos == 0) {
		nrw_mbox_enable_irq(mbox, mbox->finfo.user);
	}
	mbox->list_cb_pos |= pos;
/*
	printk(KERN_INFO "nrw_mbox_register mbox: 0x%08x, name: %s, pv: 0x%08x, cb: 0x%08x, pos: 0x%02x\n",(unsigned)(mbox),mbox->name,(unsigned)(pv),(unsigned)(cb), mbox->list_cb_pos);
*/
	mutex_unlock(&mbox->list_cb_lock);

	return 0;
}

EXPORT_SYMBOL(nrw_mbox_register);

static const struct of_device_id tc_of_match[] = {

	{ .compatible = "nrw,omap4-mailbox", },
	{}
};

static void nrw_mbox_add(struct nrw_mbox_device *mdev)
{
	mutex_lock(&nrw_mbox_devices_lock);
	list_add(&mdev->elem, &nrw_mbox_devices);
	mutex_unlock(&nrw_mbox_devices_lock);
}

static int nrw_mbox_probe(struct platform_device *pdev)
{
	struct resource *mem;
	struct nrw_mbox_device *mdev;

	int ret;
	struct device_node *node = pdev->dev.of_node;
	const struct of_device_id *match;
	struct device_node *child;
	struct nrw_mbox_fifo_info *finfo, *finfoblk;
	struct nrw_mbox **list, *mbox, *mboxblk;

	u32 intr_type, info_count;
	u32 tmp[2];
	int i, count;

	if (!node) {
		dev_err(&pdev->dev, "invalid platform data");
		return -EINVAL;
	}

	match = of_match_device(tc_of_match, &pdev->dev);
	if (!match) {
		printk(KERN_ERR "nrw_mbox: Failed to match device\n");
		return -ENODEV;
	}

	intr_type = (u32)match->data;
	info_count = of_get_available_child_count(node);

	finfoblk = devm_kzalloc(&pdev->dev, info_count * sizeof(*finfoblk), GFP_KERNEL);
	if (!finfoblk) {
		printk(KERN_ERR "nrw_mbox: Failed to alocate memory buffer1\n");
		return -ENOMEM;
	}

	finfo = finfoblk;
	child = NULL;

	for (i = 0; i < info_count; i++, finfo++) {

		child = of_get_next_available_child(node, child);
		ret = of_property_read_u32_array(child, "nrw,mbox-conf",
						tmp, ARRAY_SIZE(tmp));
		if (ret) {
			printk(KERN_ERR "nrw_mbox: Failed to find 'nrw,mbox-conf'\n");
			return ret;
		}

		finfo->name = child->name;

		finfo->queue = tmp[0];
		finfo->user = tmp[1];
	}

	mdev = devm_kzalloc(&pdev->dev, sizeof(*mdev), GFP_KERNEL);
	if (mdev == NULL) {
		printk(KERN_ERR "nrw_mbox: Failed to alocate memory buffer2\n");
		return -ENOMEM;
	}

	mem = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	mdev->mbox_base = devm_ioremap_resource(&pdev->dev, mem);

	/* allocate one extra for making end of list */
	list = devm_kzalloc(&pdev->dev, (info_count + 1) * sizeof(*list), GFP_KERNEL);
	if (!list) {
		printk(KERN_ERR "nrw_mbox: Failed to alocate memory buffer3\n");
		return -ENOMEM;
	}

	mboxblk = devm_kzalloc(&pdev->dev, info_count * sizeof(*mbox), GFP_KERNEL);
	if (!mboxblk) {
		printk(KERN_ERR "nrw_mbox: Failed to alocate memory buffer4\n");
		return -ENOMEM;
	}

	mbox = mboxblk;
	finfo = finfoblk;
	for (i = 0; i < info_count; i++, finfo++, mbox++) {

		mbox->fifo.msg = MAILBOX_MESSAGE(finfo->queue);
		mbox->fifo.msg_stat = MAILBOX_MSGSTATUS(finfo->queue);
		mbox->fifo.fifo_stat = MAILBOX_FIFOSTATUS(finfo->queue);
		mbox->fifo.intr_bit = MAILBOX_IRQ_NEWMSG(finfo->queue);
		mbox->fifo.irqenable = MAILBOX_IRQENABLE(finfo->user);
		mbox->fifo.irqstatus = MAILBOX_IRQSTATUS(finfo->user);
		mbox->fifo.irqdisable = MAILBOX_IRQDISABLE(finfo->user);

		mbox->name = finfo->name;
		mbox->parent = mdev;
		mbox->list_cb_pos=0;
		for (count = 0; count < MAX_INST_CALLBACK; count++) {
			mbox->list_cb[count].pv=0;
			mbox->list_cb[count].cb=0;
		}

		mutex_init(&mbox->list_cb_lock);

		mbox->irq = platform_get_irq(pdev, finfo->user);

		list[i] = mbox;
	}

	mdev->name = node->name;
	mdev->dev = &pdev->dev;
	mdev->mboxes = list;

	nrw_mbox_add(mdev);

	platform_set_drvdata(pdev, mdev);

	printk(KERN_INFO "nrw_mbox: mbox 0x%08x probed\n", (unsigned)(mbox));
	return 0;
}

static struct platform_driver nrw_mbox_driver = {
	.driver = {
		.name = "nrw omap mailbox",
		.owner = THIS_MODULE,
		.of_match_table = of_match_ptr(tc_of_match),
	},
	.probe = nrw_mbox_probe,
};

int __init nrw_mbox_init(void)
{
	return platform_driver_register(&nrw_mbox_driver);
}

void __exit nrw_mbox_exit(void)
{
}

module_init(nrw_mbox_init);
module_exit(nrw_mbox_exit);

MODULE_AUTHOR("Nutaq Inc. <www.nutaq.com>");
MODULE_DESCRIPTION("nrw omap mailbox");
MODULE_LICENSE("GPL");
