#include <linux/fs.h>
#include <linux/clk.h>
#include <linux/uaccess.h>
#include <linux/bitrev.h>
#include <linux/debugfs.h>
#include <linux/delay.h>
#include <linux/firmware.h>
#include <linux/init.h>
#include <linux/io.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/of.h>
#include <linux/of_address.h>
#include <linux/of_gpio.h>
#include <linux/of_platform.h>
#include <linux/platform_device.h>

#define FPGADL_DRV_NAME "fpgadl"

enum fpgadl_state
{
	FPGADL_STATE_OFFLINE = 0,
	FPGADL_STATE_INVALID,
	FPGADL_STATE_ERROR,
	FPGADL_STATE_LOADED,
};

struct fpgadl_data {
	struct device *dev;
	void *va;
	bool bus_16b;
	bool bit_rev;
	bool byte_swap;
	char fwname[129];
	struct clk *clkout1;
	struct {
        	int done;
	        int init_b;
        	int prog_b;
	} gpio;
	enum fpgadl_state state;
	struct bitstream_info {
	    char design_name[129];
	    char part_name[33];
	    char date[11];
	    char time[9];
	    u32 length;
	    u8 *buf;
	} bs_info;
	struct dentry *dbgfs_dir;
};

static int fpgadl_get_bitstream_info(struct device *dev, const u8 *bs, size_t length)
{
	unsigned len;
	struct fpgadl_data *priv = dev_get_drvdata(dev);

	/* Skip the header */
	bs += 13;

	/* Design name */
	if (*bs++ != 'a') {
		dev_err(dev, "invalid file format.\n");
		return -EINVAL;
	}
	len = (*bs++ << 8);
	len |= *bs++;
	if (len < sizeof(priv->bs_info.design_name)) {
		memcpy(priv->bs_info.design_name, bs, len);
	} else {
		memcpy(priv->bs_info.design_name, bs, sizeof(priv->bs_info.design_name)-1);
	}
	bs += len;

	/* Part name */
	if (*bs++ != 'b') {
		dev_err(dev, "invalid file format.\n");
		return -EINVAL;
	}
	len = (*bs++ << 8);
	len |= *bs++;
	if (len < sizeof(priv->bs_info.part_name)) {
		memcpy(priv->bs_info.part_name, bs, len);
	} else {
		memcpy(priv->bs_info.part_name, bs, sizeof(priv->bs_info.part_name)-1);
	}
	bs += len;

	/* Date */
	if (*bs++ != 'c') {
		dev_err(dev, "invalid file format.\n");
		return -EINVAL;
	}
	len = (*bs++ << 8);
	len |= *bs++;
	if (len < sizeof(priv->bs_info.date)) {
		memcpy(priv->bs_info.date, bs, len);
	} else {
		memcpy(priv->bs_info.date, bs, sizeof(priv->bs_info.date)-1);
	}
	bs += len;

	/* Time */
	if (*bs++ != 'd') {
		dev_err(dev, "invalid file format.\n");
		return -EINVAL;
	}
	len = (*bs++ << 8);
	len |= *bs++;
	if (len < sizeof(priv->bs_info.time)) {
		memcpy(priv->bs_info.time, bs, len);
	} else {
		memcpy(priv->bs_info.time, bs, sizeof(priv->bs_info.time)-1);
	}
	bs += len;

	/* Bitstream length */
	if (*bs++ != 'e') {
		dev_err(dev, "invalid file format.\n");
		return -EINVAL;
	}
	priv->bs_info.length  = (*bs++ << 24);
	priv->bs_info.length |= (*bs++ << 16);
	priv->bs_info.length |= (*bs++ <<  8);
	priv->bs_info.length |= *bs++;

        /* Bitstream pointer */
	priv->bs_info.buf = (u8 *)bs;

	dev_dbg(dev, "Bitstream info\n"
	             "\tdesign name : %s\n"
	             "\tpart name   : %s\n"
	             "\tdate        : %s\n"
	             "\ttime        : %s\n"
	             "\tlength      : %d bytes\n",
		priv->bs_info.design_name, 
		priv->bs_info.part_name, priv->bs_info.date, 
		priv->bs_info.time, priv->bs_info.length);
	return 0;
}

static int fpgadl_write_bitstream(struct device *dev)
{
	int i;
	int pad;
	int timeout;
	struct fpgadl_data *priv = dev_get_drvdata(dev);

	/* Configuration Reset */
	gpio_set_value(priv->gpio.prog_b, 0);
	udelay(1);
	gpio_set_value(priv->gpio.prog_b, 1);

	/* Wait for Device Initialization */
	timeout = 0;
	while (gpio_get_value(priv->gpio.init_b) == 0) {
		if (++timeout > 50 ) {
			priv->state = FPGADL_STATE_ERROR;
			dev_err(dev, "timeout while waiting for device initialization.\n");
			return -EIO;
		}
		udelay(100);
	}

	/* Write the bitstream in memory. */
	if (priv->bus_16b) {
		if (priv->byte_swap && priv->bit_rev) {
			for (i = 0; i < priv->bs_info.length; i+=2)
				*(volatile u16 *)priv->va = (bitrev8(priv->bs_info.buf[i+0]) << 8) 
				                           | bitrev8(priv->bs_info.buf[i+1]);
		} else if (priv->byte_swap && !priv->bit_rev) {
			for (i = 0; i < priv->bs_info.length; i+=2)
				*(volatile u16 *)priv->va = (priv->bs_info.buf[i+0] << 8) 
				                           | priv->bs_info.buf[i+1];
		} else if (!priv->byte_swap && priv->bit_rev) {
			for (i = 0; i < priv->bs_info.length; i+=2)
				*(volatile u16 *)priv->va = (bitrev8(priv->bs_info.buf[i+1]) << 8) 
				                           | bitrev8(priv->bs_info.buf[i+0]);
		} else {
			for (i = 0; i < priv->bs_info.length; i+=2)
				*(volatile u16 *)priv->va = (priv->bs_info.buf[i+1] << 8) 
				                           | priv->bs_info.buf[i+0];
		}
	} else {
		if (priv->bit_rev) {
			for (i = 0; i < priv->bs_info.length; i++)
				*(volatile u16 *)priv->va = (bitrev8(priv->bs_info.buf[i]) << 8) | bitrev8(priv->bs_info.buf[i]);
		} else {
			for (i = 0; i < priv->bs_info.length; i++)
				*(volatile u16 *)priv->va = (priv->bs_info.buf[i] << 8) | priv->bs_info.buf[i];
		}
	}


	/* Compensate for Special Startup Conditions */
        for (pad = 0; pad < 1000; pad++) {
                if (gpio_get_value(priv->gpio.done) != 0) {
			priv->state = FPGADL_STATE_LOADED;
                        dev_info(dev, "Bitstream loaded\n");
			return 0;
                }

                if (gpio_get_value(priv->gpio.init_b) == 0) {
                        if (gpio_get_value(priv->gpio.done) == 0) {
				priv->state = FPGADL_STATE_ERROR;
				dev_err(dev, "timeout while waiting for the device to start\n");
                                return -EIO;
                        }
                }
		/* Toggle the CCLK line. */
		*(volatile u8 *)priv->va = 0;
        }	

	priv->state = FPGADL_STATE_ERROR;
	dev_err(dev, "device did not start\n");
	return -EIO;
}

static int fpgadl_load_firmware(struct device *dev)
{
	int ret;
	const struct firmware *fw;
	struct fpgadl_data *priv = dev_get_drvdata(dev);

	memset(&priv->bs_info, 0, sizeof(priv->bs_info));
	ret = request_firmware(&fw, priv->fwname, dev);
	if (ret) {
		priv->state = FPGADL_STATE_INVALID;
		dev_err(dev, "request firmware failed: %s\n", priv->fwname);
		return ret;
	}

	ret = fpgadl_get_bitstream_info(dev, fw->data, fw->size);
	if (ret) {
		priv->state = FPGADL_STATE_INVALID;
		release_firmware(fw);
		return ret;
	}

	fpgadl_write_bitstream(dev);
	release_firmware(fw);
	return 0;
}
static ssize_t fpgadl_show_state(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	struct fpgadl_data*priv = dev_get_drvdata(dev);
	
	switch (priv->state) {
	case FPGADL_STATE_OFFLINE:
		return sprintf(buf, "offline\n");
	case FPGADL_STATE_ERROR:
		return sprintf(buf, "error\n");
	case FPGADL_STATE_INVALID:
		return sprintf(buf, "invalid\n");
	case FPGADL_STATE_LOADED:
		return sprintf(buf, "loaded\n");
	default:
		return sprintf(buf, "unknown\n");
	}
}

static ssize_t fpgadl_show_fwinfo(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	struct fpgadl_data*priv = dev_get_drvdata(dev);

	return sprintf(buf, "Bitstream info\n"
                     "\tdesign name : %s\n"
                     "\tpart name   : %s\n"
                     "\tdate        : %s\n"
                     "\ttime        : %s\n"
                     "\tlength      : %d bytes\n",
		priv->bs_info.design_name,
		priv->bs_info.part_name, priv->bs_info.date,
		priv->bs_info.time, priv->bs_info.length);
}

static ssize_t fpgadl_show_fwname(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	struct fpgadl_data*priv = dev_get_drvdata(dev);
	return sprintf(buf, "%s\n", priv->fwname);
}

static ssize_t fpgadl_write_fwname(struct device *dev,
		struct device_attribute *attr,
		const char *buf, size_t count)
{
	struct fpgadl_data*priv = dev_get_drvdata(dev);
        char fwName[130];
        size_t len;
	int ret;

        fwName[sizeof(fwName) - 1] = '\0';
        strncpy(fwName, buf, sizeof(fwName) - 1);
        len = strlen(fwName);

        if (len && fwName[len - 1] == '\n')
                fwName[len - 1] = '\0';

	ret = scnprintf(priv->fwname, sizeof(priv->fwname), "%s", fwName);
        if (ret != strlen(fwName)) {
                return -EINVAL;
        }

	ret = fpgadl_load_firmware(priv->dev);
	if (ret) {
		return ret;
        }
	return count;

}

static DEVICE_ATTR(state,  S_IRUGO, fpgadl_show_state,  NULL);
static DEVICE_ATTR(fwinfo, S_IRUGO, fpgadl_show_fwinfo, NULL);
static DEVICE_ATTR(fwname, 0660,    fpgadl_show_fwname, fpgadl_write_fwname);

static struct attribute *fpgadl_attrs[] = {
	&dev_attr_state.attr,
	&dev_attr_fwinfo.attr,
	&dev_attr_fwname.attr,
	NULL,
};

static const struct attribute_group fpgadl_attr_group = {
	.attrs = fpgadl_attrs,
};
ATTRIBUTE_GROUPS(fpgadl);


static bool fpgadl_is_loaded(struct platform_device *pdev)
{
	struct fpgadl_data *priv = dev_get_drvdata(&pdev->dev);
	if (priv != NULL) 
		return (priv->state == FPGADL_STATE_LOADED);
	else 
		return false;
}
EXPORT_SYMBOL(fpgadl_is_loaded);


static int fpgadl_driver_probe(struct platform_device *pdev)
{
	int ret;
	int device = -1; 
	const char *fwname;
	struct resource res;
	struct fpgadl_data *priv;
	struct device_node *np = pdev->dev.of_node;

	priv = devm_kzalloc(&pdev->dev, sizeof(*priv), GFP_KERNEL);
	if (!priv) {
		dev_err(&pdev->dev, "Memory allocation for fpgadl_data failed\n");
		return -ENOMEM;
	}
	platform_set_drvdata(pdev, priv);
	priv->dev = &pdev->dev;
	priv->state = FPGADL_STATE_OFFLINE;

	if (!np) {
		dev_err(&pdev->dev, "invalid platform data");
		return -EINVAL;
	}

	/* Enable the FPGA clock */
	priv->clkout1 = devm_clk_get(&pdev->dev, "clkout1");
        if (PTR_ERR(priv->clkout1) == -EPROBE_DEFER) {
                return -EPROBE_DEFER;
        } else if (IS_ERR(priv->clkout1)) {
                dev_dbg(&pdev->dev, "clkout1 not found in DT.\n");
		return -EINVAL;
        }
	ret = clk_prepare_enable(priv->clkout1);
	if (ret) {
                dev_err(&pdev->dev, "failed to enable clkout1.\n");
		return ret;
	}

	ret = of_address_to_resource(np, 0, &res);
	if (ret) {
		dev_err(&pdev->dev, "Can't read resource from DT.\n");
		return ret;
	} 
	priv->va = ioremap(res.start, resource_size(&res));
	
	dev_dbg(&pdev->dev, "memory start : %08X\n", res.start);
	dev_dbg(&pdev->dev, "memory end   : %08X\n", res.end);
	dev_dbg(&pdev->dev, "memory size  : %d\n", resource_size(&res));

	priv->gpio.done   = of_get_named_gpio(np, "done-gpio",   0);
	priv->gpio.init_b = of_get_named_gpio(np, "init_b-gpio", 0);
	priv->gpio.prog_b = of_get_named_gpio(np, "prog_b-gpio", 0);
	ret = devm_gpio_request_one(&pdev->dev, priv->gpio.done,
				    GPIOF_IN, "FPGA DONE");
 	if (ret) {
		dev_info(&pdev->dev, "failed to request gpio %d\n", priv->gpio.done);
 		return ret;
	}
	ret = devm_gpio_request_one(&pdev->dev, priv->gpio.init_b,
				    GPIOF_IN, "FPGA INIT_B");
 	if (ret) {
		dev_info(&pdev->dev, "failed to request gpio %d\n", priv->gpio.init_b);
 		return ret;
	}
	ret = devm_gpio_request_one(&pdev->dev, priv->gpio.prog_b,
				    GPIOF_OUT_INIT_HIGH, "FPGA PROG_B");
 	if (ret) {
		dev_info(&pdev->dev, "failed to request gpio %d\n", priv->gpio.prog_b);
 		return ret;
	}
	dev_dbg(&pdev->dev, "gpio-done   = %d\n", priv->gpio.done);
	dev_dbg(&pdev->dev, "gpio-init_b = %d\n", priv->gpio.init_b);
	dev_dbg(&pdev->dev, "gpio-prog_b = %d\n", priv->gpio.prog_b);

	fwname = (const char *)of_get_property(np, "fw-name", NULL);
	if (fwname != NULL) {
        	if (of_gpio_named_count(np, "dev-gpios") == 2) {
			int gpio0, gpio1;

			gpio0 = of_get_named_gpio(np, "dev-gpios", 0);
			gpio1 = of_get_named_gpio(np, "dev-gpios", 1);

                	device  = gpio_get_value(gpio0) << 0;
                	device |= gpio_get_value(gpio1) << 1;
		}
		switch (device) {
		case 0 : 
			ret = scnprintf(priv->fwname, sizeof(priv->fwname), "%s-35t", fwname);
			break;
		case 1 : 
			ret = scnprintf(priv->fwname, sizeof(priv->fwname), "%s-50t", fwname);
			break;
		case 2 : 
			ret = scnprintf(priv->fwname, sizeof(priv->fwname), "%s-75t", fwname);
			break;
		case 3 : 
			ret = scnprintf(priv->fwname, sizeof(priv->fwname), "%s-100t", fwname);
			break;
		default: 
			ret = scnprintf(priv->fwname, sizeof(priv->fwname), "%s", fwname);
			break;
		}
		dev_dbg(&pdev->dev, "firmware name: %s\n", priv->fwname);
	}

	if (of_get_property(np, "bus-16b", NULL) != NULL) {
		priv->bus_16b = true;
	} else {
		priv->bus_16b = false;
	}

	if (of_get_property(np, "bit-rev", NULL) != NULL) {
		priv->bit_rev = true;
	} else {
		priv->bit_rev = false;
	}

	if (of_get_property(np, "byte-swap", NULL) != NULL) {
		priv->byte_swap = true;
	} else {
		priv->byte_swap = false;
	}

	ret = sysfs_create_group(&priv->dev->kobj, fpgadl_groups[0]);
	if (ret) {
		dev_err(priv->dev, "unable to create sysfs files\n");
		return ret;
	}

	ret = fpgadl_load_firmware(&pdev->dev);
	return 0;
}

static int fpgadl_driver_remove(struct platform_device *pdev)
{
	struct fpgadl_data *priv = dev_get_drvdata(&pdev->dev);

	sysfs_remove_group(&priv->dev->kobj, fpgadl_groups[0]);
	return 0;
}

static struct of_device_id fpgadl_of_match[] = {
	{ .compatible = "xlnx,fpgadl", },
	{}
};
MODULE_DEVICE_TABLE(of, fpgadl_of_match);

static struct platform_driver fpgadl_platform_driver = {
	.driver = {
		.name = FPGADL_DRV_NAME,
		.owner = THIS_MODULE,
		.of_match_table = of_match_ptr(fpgadl_of_match),
	},
	.probe = fpgadl_driver_probe,
	.remove = fpgadl_driver_remove,
};

int __init fpgadl_init(void)
{
	platform_driver_register(&fpgadl_platform_driver);
	return 0;
}
module_init(fpgadl_init);

void __exit fpgadl_exit(void)
{
	platform_driver_unregister(&fpgadl_platform_driver);
}
module_exit(fpgadl_exit);

MODULE_AUTHOR("Nuran Wireless <support@nuranwireless.com>");
MODULE_DESCRIPTION("Xilinx SelectMAP FPGA Loader");
MODULE_LICENSE("GPL");
