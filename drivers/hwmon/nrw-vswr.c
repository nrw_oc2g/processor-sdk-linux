/*
 * nrw-vswr.c - Voltage standing wave ratio calculation module
 *
 *  Copyright (C) 2016 Nuranwireless Inc.
 *  <support@nuranwireless.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#include <linux/slab.h>
#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/err.h>
#include <linux/io.h>
#include <linux/of.h>
#include <linux/of_device.h>
#include <linux/of_address.h>

#include <linux/hwmon.h>
#include <linux/hwmon-sysfs.h>

#include <linux/platform_data/fpgadl.h>

#define VSWR_TX0 0
#define VSWR_TX1 1
#define FWD_UNCOMP_TX0 2
#define FWD_UNCOMP_TX1 3
#define FWD_COMP_TX0 4
#define FWD_COMP_TX1 5
#define REFL_UNCOMP_TX0 6
#define REFL_UNCOMP_TX1 7
#define REFL_COMP_TX0 8
#define REFL_COMP_TX1 9
#define RETURN_LOSS_ERROR_TX0 10
#define RETURN_LOSS_ERROR_TX1 11
#define FORCE_TX0 14
#define FORCE_TX1 15

struct vswr_priv {
	struct device *hwmon_dev;
	u32 *va;
};

static ssize_t show_vswr_tx0(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	u32 val;
	struct vswr_priv *priv = dev_get_drvdata(dev);
	
	val = priv->va[VSWR_TX0];

	return sprintf( buf, "%d\n", val );
}

static ssize_t set_vswr_tx0(struct device *dev,
			       struct device_attribute *attr,
			       const char *buf, size_t count)
{
	struct vswr_priv *priv = dev_get_drvdata(dev);

	long temp;
	int err;

	err = kstrtoul(buf, 10, &temp);
	if(err)
		return err;

	priv->va[VSWR_TX0]  = (u32)temp;

	return count;
}

static ssize_t show_vswr_tx1(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	u32 val;
	struct vswr_priv *priv = dev_get_drvdata(dev);
	
	val = priv->va[VSWR_TX1];

	return sprintf( buf, "%d\n", val );
}

static ssize_t set_vswr_tx1(struct device *dev,
			       struct device_attribute *attr,
			       const char *buf, size_t count)
{
	struct vswr_priv *priv = dev_get_drvdata(dev);

	long temp;
	int err;

	err = kstrtoul(buf, 10, &temp);
	if(err)
		return err;

	priv->va[VSWR_TX1]  = (u32)temp;

	return count;
}

static ssize_t show_fwd_uncomp_tx0(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	u32 val;
	struct vswr_priv *priv = dev_get_drvdata(dev);
	
	val = priv->va[FWD_UNCOMP_TX0];

	return sprintf( buf, "%d\n", val );
}

static ssize_t show_fwd_uncomp_tx1(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	u32 val;
	struct vswr_priv *priv = dev_get_drvdata(dev);
	
	val = priv->va[FWD_UNCOMP_TX1];

	return sprintf( buf, "%d\n", val );
}

static ssize_t show_fwd_comp_tx0(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	u32 val;
	struct vswr_priv *priv = dev_get_drvdata(dev);
	
	val = priv->va[FWD_COMP_TX0];

	return sprintf( buf, "%d\n", val );
}

static ssize_t show_fwd_comp_tx1(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	u32 val;
	struct vswr_priv *priv = dev_get_drvdata(dev);
	
	val = priv->va[FWD_COMP_TX1];

	return sprintf( buf, "%d\n", val );
}

static ssize_t show_refl_uncomp_tx0(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	u32 val;
	struct vswr_priv *priv = dev_get_drvdata(dev);
	
	val = priv->va[REFL_UNCOMP_TX0];

	return sprintf( buf, "%d\n", val );
}

static ssize_t show_refl_uncomp_tx1(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	u32 val;
	struct vswr_priv *priv = dev_get_drvdata(dev);
	
	val = priv->va[REFL_UNCOMP_TX1];

	return sprintf( buf, "%d\n", val );
}

static ssize_t show_refl_comp_tx0(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	u32 val;
	struct vswr_priv *priv = dev_get_drvdata(dev);
	
	val = priv->va[REFL_COMP_TX0];

	return sprintf( buf, "%d\n", val );
}

static ssize_t show_refl_comp_tx1(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	u32 val;
	struct vswr_priv *priv = dev_get_drvdata(dev);
	
	val = priv->va[REFL_COMP_TX1];

	return sprintf( buf, "%d\n", val );
}

static ssize_t show_rl_error_tx0(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	u32 val;
	struct vswr_priv *priv = dev_get_drvdata(dev);
	
	val = priv->va[RETURN_LOSS_ERROR_TX0];

	return sprintf( buf, "%d\n", val );
}

static ssize_t show_rl_error_tx1(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	u32 val;
	struct vswr_priv *priv = dev_get_drvdata(dev);
	
	val = priv->va[RETURN_LOSS_ERROR_TX1];

	return sprintf( buf, "%d\n", val );
}

static ssize_t show_force_tx0(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	u32 val;
	struct vswr_priv *priv = dev_get_drvdata(dev);
	
	val = priv->va[FORCE_TX0];

	return sprintf( buf, "%d\n", val );
}

static ssize_t show_force_tx1(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	u32 val;
	struct vswr_priv *priv = dev_get_drvdata(dev);
	
	val = priv->va[FORCE_TX1];

	return sprintf( buf, "%d\n", val );
}

static ssize_t set_force_tx0(struct device *dev,
			       struct device_attribute *attr,
			       const char *buf, size_t count)
{
	struct vswr_priv *priv = dev_get_drvdata(dev);

	long temp;
	int err;

	err = kstrtoul(buf, 10, &temp);
	if(err)
		return err;

	priv->va[FORCE_TX0]  = (u32)temp;

	return count;
}

static ssize_t set_force_tx1(struct device *dev,
			       struct device_attribute *attr,
			       const char *buf, size_t count)
{
	struct vswr_priv *priv = dev_get_drvdata(dev);

	long temp;
	int err;

	err = kstrtoul(buf, 10, &temp);
	if(err)
		return err;

	priv->va[FORCE_TX1]  = (u32)temp;

	return count;
}

int vswr_parse_dt(struct platform_device *pdev)
{
	int ret;
	struct resource res;
	struct device_node *fpga_node;
	struct platform_device *fpga_pdev;
	struct device_node *np = pdev->dev.of_node;
	struct vswr_priv *priv = dev_get_drvdata(&pdev->dev);
	
	if (!np)
	{
		return -EINVAL;
	}
	
	fpga_node = of_parse_phandle(np, "fpga-device", 0);
	if (!fpga_node)
	{
		dev_err(&pdev->dev, "Can't get associated FPGA device from DT.\n");
		return -EINVAL;
	}
	
	fpga_pdev = of_find_device_by_node(fpga_node);
	if (!fpga_pdev)
	{
		dev_err(&pdev->dev, "Can't find FPGA device node.\n");
		return -EINVAL;
	}
	
	if (!fpgadl_is_loaded(fpga_pdev))
	{
		return -EPROBE_DEFER;
	}
	
	ret = of_address_to_resource(np, 0, &res);
	if (ret) {
		dev_err(&pdev->dev, "Can't read resource from DT.\n");
		return ret;
	} 
	priv->va = (u32 *)ioremap(res.start, resource_size(&res));
	
	return ret;
}

static SENSOR_DEVICE_ATTR(vswr_tx0, S_IWUSR | S_IRUGO, show_vswr_tx0, set_vswr_tx0, 0);
static SENSOR_DEVICE_ATTR(vswr_tx1, S_IWUSR | S_IRUGO, show_vswr_tx1, set_vswr_tx1, 0);
static SENSOR_DEVICE_ATTR(fwd_uncomp_tx0, S_IRUGO, show_fwd_uncomp_tx0, NULL, 0);
static SENSOR_DEVICE_ATTR(fwd_uncomp_tx1, S_IRUGO, show_fwd_uncomp_tx1, NULL, 0);
static SENSOR_DEVICE_ATTR(fwd_comp_tx0, S_IRUGO, show_fwd_comp_tx0, NULL, 0);
static SENSOR_DEVICE_ATTR(fwd_comp_tx1, S_IRUGO, show_fwd_comp_tx1, NULL, 0);
static SENSOR_DEVICE_ATTR(refl_uncomp_tx0, S_IRUGO, show_refl_uncomp_tx0, NULL, 0);
static SENSOR_DEVICE_ATTR(refl_uncomp_tx1, S_IRUGO, show_refl_uncomp_tx1, NULL, 0);
static SENSOR_DEVICE_ATTR(refl_comp_tx0, S_IRUGO, show_refl_comp_tx0, NULL, 0);
static SENSOR_DEVICE_ATTR(refl_comp_tx1, S_IRUGO, show_refl_comp_tx1, NULL, 0);
static SENSOR_DEVICE_ATTR(rl_error_tx0, S_IRUGO, show_rl_error_tx0, NULL, 0);
static SENSOR_DEVICE_ATTR(rl_error_tx1, S_IRUGO, show_rl_error_tx1, NULL, 0);
static SENSOR_DEVICE_ATTR(force_tx0, S_IWUSR | S_IRUGO, show_force_tx0, set_force_tx0, 0);
static SENSOR_DEVICE_ATTR(force_tx1, S_IWUSR | S_IRUGO, show_force_tx1, set_force_tx1, 0);

static struct attribute *vswr_attrs[] = {
	&sensor_dev_attr_vswr_tx0.dev_attr.attr,
	&sensor_dev_attr_vswr_tx1.dev_attr.attr,
	&sensor_dev_attr_fwd_uncomp_tx0.dev_attr.attr,
	&sensor_dev_attr_fwd_uncomp_tx1.dev_attr.attr,
	&sensor_dev_attr_fwd_comp_tx0.dev_attr.attr,
	&sensor_dev_attr_fwd_comp_tx1.dev_attr.attr,
	&sensor_dev_attr_refl_uncomp_tx0.dev_attr.attr,
	&sensor_dev_attr_refl_uncomp_tx1.dev_attr.attr,
	&sensor_dev_attr_refl_comp_tx0.dev_attr.attr,
	&sensor_dev_attr_refl_comp_tx1.dev_attr.attr,
	&sensor_dev_attr_rl_error_tx0.dev_attr.attr,
	&sensor_dev_attr_rl_error_tx1.dev_attr.attr,
	&sensor_dev_attr_force_tx0.dev_attr.attr,
	&sensor_dev_attr_force_tx1.dev_attr.attr,
	NULL,
};

static const struct attribute_group vswr_attr_group = {
	.attrs = vswr_attrs,
};
ATTRIBUTE_GROUPS(vswr);

static int  vswr_probe(struct platform_device *pdev)
{
	struct  vswr_priv *priv;
	int ret;

	priv = devm_kzalloc(&pdev->dev, sizeof(struct vswr_priv), GFP_KERNEL);
	if (!priv)
		return -ENOMEM;

	platform_set_drvdata(pdev, priv);

	ret = vswr_parse_dt(pdev);
	if (ret)	
		return ret;
	
	ret = sysfs_create_group(&pdev->dev.kobj, vswr_groups[0]);
	if (ret) {
		dev_err(&pdev->dev, "unable to create sysfs files\n");
		return ret;
	}
	
	priv->hwmon_dev = devm_hwmon_device_register_with_groups(
				&pdev->dev, "vswr", priv, vswr_groups);
	if (IS_ERR(priv->hwmon_dev)) {
		dev_err(&pdev->dev, "unable to register as hwmon device.\n");
		ret = PTR_ERR(priv->hwmon_dev);
		goto err_after_sysfs;
	}
	
	printk( KERN_INFO "NRW VSWR driver started!\n");

	return 0;

err_after_sysfs:
	sysfs_remove_group(&pdev->dev.kobj, vswr_groups[0]);
	return ret;
}

static int vswr_remove(struct platform_device *pdev)
{
	struct vswr_priv *priv = platform_get_drvdata(pdev);

	if(priv)
	{
		sysfs_remove_group(&pdev->dev.kobj, vswr_groups[0]);
		hwmon_device_unregister(priv->hwmon_dev);
		iounmap((void *)priv->va);
	}

	printk( KERN_INFO "NRW VSWR driver stopped!\n");
	
	return 0;
}

static const struct of_device_id vswr_of_match[] = {
	{ .compatible = "nrw-vswr", },
	{}
};
MODULE_DEVICE_TABLE(of, vswr_of_match);

static struct platform_driver vswr_driver = {
	.driver = {
		.name = "nrw-vswr",
		.owner = THIS_MODULE,
		.of_match_table = vswr_of_match,
	},
	.probe = vswr_probe,
	.remove = vswr_remove,
};

module_platform_driver(vswr_driver);

MODULE_DESCRIPTION("Nuran Wireless VSWR Driver");
MODULE_AUTHOR("<support@nuranwireless.com>");
MODULE_LICENSE("GPL");
