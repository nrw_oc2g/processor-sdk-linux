/*
 * nrw-clkerr.c - FPGA based clock error module
 *
 *  Copyright (C) 2015 Nuranwireless Inc.
 *  <support@nuranwireless.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#include <linux/slab.h>
#include <linux/module.h>
#include <linux/pm_runtime.h>
#include <linux/math64.h>
#include <linux/platform_device.h>
#include <linux/err.h>
#include <linux/io.h>
#include <linux/of.h>
#include <linux/of_device.h>
#include <linux/of_address.h>

#include <linux/hwmon.h>
#include <linux/hwmon-sysfs.h>

#include <linux/platform_data/fpgadl.h>

#define CLKERR_REG_CTRL		0
#define CLKERR_REG_PPS		1
#define CLKERR_REG_OFST		2

#define CLKERR_REG_CTRL_RESET				0x0001
#define CLKERR_REG_CTRL_3DFIX_NOT_PRESENT	0x0002
#define CLKERR_REG_CTRL_OFFSET_TOO_BIG	0x0004

struct clkerr_priv {
	struct device *hwmon_dev;
	struct device *dev;
	u32 *va;
	u32 freq;
        u32 auto_refresh;
	u32 pps_accuracy;
        struct mutex lock;
	struct {
		int valid;
		int ofst;
		u32 pps;
		int err;
		u32 interval;
		u32 accuracy;

		int average;
		u32 avg_interval;
		u32 avg_accuracy;
	} last;
};

static int clkerr_refresh(struct device *dev)
{
	struct clkerr_priv *priv = dev_get_drvdata(dev);
	unsigned pps;
	int ofst;
	int retry;
	int fault = 0;
	s64 err = 0;
	s64 err_accuracy = 0;
	s64 err_interval = 0;
	s64 avg = 0;
	s64 avg_accuracy = 0;
	u32 fault_val = 0;

	retry = 0;
	do {
		pps  = priv->va[CLKERR_REG_PPS];
		ofst = priv->va[CLKERR_REG_OFST];

		if (priv->va[CLKERR_REG_CTRL] & (CLKERR_REG_CTRL_3DFIX_NOT_PRESENT | 
			CLKERR_REG_CTRL_OFFSET_TOO_BIG)) {
			fault++;
		}
		if ( retry++ > 10 ) {
			printk( KERN_ERR "clkerr: Abort after %d retries\n", retry );
			return -EIO;
		}
	} while (pps != priv->va[CLKERR_REG_PPS]);

	fault_val = (priv->va[CLKERR_REG_CTRL] & (CLKERR_REG_CTRL_3DFIX_NOT_PRESENT | 
		CLKERR_REG_CTRL_OFFSET_TOO_BIG));
	
	if (fault_val & CLKERR_REG_CTRL_OFFSET_TOO_BIG) {
		printk( KERN_WARNING "clkerr: Offset count too big\n" );
	}
	
	if (fault_val) {
		fault++;
	}

	if ((pps != 0) && (priv->freq != 0)) {
        	avg          = div64_s64((s64)ofst * 1000000, (s64)priv->freq * (s64)pps);
		avg_accuracy = div64_s64((s64)(2000000000/priv->freq) + priv->pps_accuracy * 2000u, (s64)pps);

		if (priv->last.valid) {
			if (pps != priv->last.pps) {
				err_interval = pps - priv->last.pps;
				err = div64_s64(((s64)ofst - (s64)priv->last.ofst) * 1000000, 
						(s64)priv->freq * (s64)err_interval);
				err_accuracy = div64_s64((s64)(2000000000/priv->freq) + priv->pps_accuracy * 2000u, (s64)err_interval);
			}
			else {
				err = priv->last.err;
				err_interval = priv->last.interval;
				err_accuracy = priv->last.accuracy;
			}		
		}
		else {
			err = avg;
			err_interval = pps;
			err_accuracy = avg_accuracy;
		}
	}

	mutex_lock(&priv->lock);
	priv->last.valid        = !fault;
	priv->last.pps          = pps;
	priv->last.ofst         = ofst;
	priv->last.err          = (int)err;
	priv->last.interval     = err_interval;
	priv->last.accuracy     = err_accuracy;
	priv->last.average      = (int)avg;
	priv->last.avg_interval = (u32)pps;
	priv->last.avg_accuracy = (u32)avg_accuracy;
	mutex_unlock(&priv->lock);

	return 0;
}

static int clkerr_get(struct device *dev, int *perr, u32 *perr_interval, u32 *perr_accuracy, 
					  int *pavg, u32 *pavg_interval, u32 *pavg_accuracy)
{
	int ret = 0;
	struct clkerr_priv *priv = dev_get_drvdata(dev);

	if ( priv->auto_refresh ) {
		ret = clkerr_refresh(dev);
		if (ret) return ret;
	}

	mutex_lock(&priv->lock);
	*perr          = priv->last.err;
	*perr_accuracy = priv->last.accuracy;
	*perr_interval = priv->last.interval;

	*pavg          = priv->last.average;
	*pavg_accuracy = priv->last.avg_accuracy;
	*pavg_interval = priv->last.pps;
	mutex_unlock(&priv->lock);

	return ret;
}

static int clkerr_reset(struct device *dev)
{
	struct clkerr_priv *priv = dev_get_drvdata(dev);
        priv->va[CLKERR_REG_CTRL] |=  CLKERR_REG_CTRL_RESET;
        priv->last.valid = 0;
	priv->va[CLKERR_REG_CTRL] &= ~CLKERR_REG_CTRL_RESET;
	return 0;
}

static ssize_t clkerr_show_reset(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	struct clkerr_priv *priv = dev_get_drvdata(dev);
	return sprintf(buf, "%u\n", (priv->va[CLKERR_REG_CTRL] & CLKERR_REG_CTRL_RESET)?1:0);
}

static ssize_t clkerr_write_reset(struct device *dev,
		struct device_attribute *attr,
		const char *buf, size_t count)
{
	int reset;
	ssize_t ret;

	ret = kstrtouint(buf, 0, &reset);
	if (ret)
		return ret;

	if (reset) clkerr_reset(dev);
	return count;
}

static ssize_t clkerr_show_refresh(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	struct clkerr_priv *priv = dev_get_drvdata(dev);

	if (priv->auto_refresh) {
		return sprintf(buf, "none once [auto]\n");
	} else {
		return sprintf(buf, "[none] once auto\n");
	}
}

static ssize_t clkerr_write_refresh(struct device *dev,
		struct device_attribute *attr,
		const char *buf, size_t count)
{
	struct clkerr_priv *priv = dev_get_drvdata(dev);
	char szCmd[5] = {0};
	size_t len;
	int ret;

	strncpy( szCmd, buf, sizeof(szCmd)-1 );
	len = strlen( szCmd );

	if (len && szCmd[len - 1] == '\n')
		szCmd[len - 1] = '\0';

	if (!strcmp(szCmd, "auto")) {
		priv->auto_refresh = 1;
		return count;
	}
     
	if (!strcmp(szCmd, "once")) {
		priv->auto_refresh = 0;
		ret = clkerr_refresh(dev);
		if (ret < 0) return ret;
		else return count;
	}
     
	if (!strcmp(szCmd, "none")) {
		priv->auto_refresh = 0;
		return count;
	}

	return -EINVAL;
}

static ssize_t clkerr_show_fault(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	struct clkerr_priv *priv = dev_get_drvdata(dev);

	return sprintf(buf, "%u\n", (priv->va[CLKERR_REG_CTRL] & (CLKERR_REG_CTRL_3DFIX_NOT_PRESENT | 
			CLKERR_REG_CTRL_OFFSET_TOO_BIG)) >> 1);
}
	
/* Frquency error in PPT */
static ssize_t clkerr_show_input(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	int ret;
        int err = 0, avg = 0;
        u32 err_interval = 0, err_accuracy = 0;
        u32 avg_interval = 0, avg_accuracy = 0;

        ret = clkerr_get(dev, &err, &err_interval, &err_accuracy, &avg, &avg_interval, &avg_accuracy);
        if (ret) return ret;
	
        return sprintf(buf, "%d\n", err);
}

/* Frequency error accuracy in PPQ */
static ssize_t clkerr_show_accuracy(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	int ret;
        int err = 0, avg = 0;
        u32 err_interval = 0, err_accuracy = 0;
        u32 avg_interval = 0, avg_accuracy = 0;

        ret = clkerr_get(dev, &err, &err_interval, &err_accuracy, &avg, &avg_interval, &avg_accuracy);
        if (ret) return ret;
	
	return sprintf(buf, "%u\n", err_accuracy);
}

/* Frequency error interval in sec */
static ssize_t clkerr_show_interval(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	int ret;
        int err = 0, avg = 0;
	u32 err_interval = 0, err_accuracy = 0;
	u32 avg_interval = 0, avg_accuracy = 0;

	ret = clkerr_get(dev, &err, &err_interval, &err_accuracy, &avg, &avg_interval, &avg_accuracy);
	if (ret) {
		return ret;
        }
	
	ret = sprintf(buf, "%u\n", err_interval);
	return ret;
}

/* Average frquency error in PPT */
static ssize_t clkerr_show_average(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	int ret;
        int err = 0, avg = 0;
        u32 err_interval = 0, err_accuracy = 0;
        u32 avg_interval = 0, avg_accuracy = 0;

        ret = clkerr_get(dev, &err, &err_interval, &err_accuracy, &avg, &avg_interval, &avg_accuracy);
        if (ret) return ret;
	
	return sprintf(buf, "%d\n", avg);
}

/* Average frequency error accuracy in PPQ */
static ssize_t clkerr_show_avg_accuracy(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	int ret;
        int err = 0, avg = 0;
        u32 err_interval = 0, err_accuracy = 0;
        u32 avg_interval = 0, avg_accuracy = 0;

        ret = clkerr_get(dev, &err, &err_interval, &err_accuracy, &avg, &avg_interval, &avg_accuracy);
        if (ret) return ret;
	
	return sprintf(buf, "%u\n", avg_accuracy);
}

/* Average frequency error interval in sec */
static ssize_t clkerr_show_avg_interval(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	int ret;
        int err = 0, avg = 0;
        u32 err_interval = 0, err_accuracy = 0;
        u32 avg_interval = 0, avg_accuracy = 0;

        ret = clkerr_get(dev, &err, &err_interval, &err_accuracy, &avg, &avg_interval, &avg_accuracy);
        if (ret) {
            return ret;
        }
	
	ret = sprintf(buf, "%u\n", avg_interval);
        return ret;
}

static SENSOR_DEVICE_ATTR(clkerr1_fault,            S_IRUGO, clkerr_show_fault,        NULL, 0);
static SENSOR_DEVICE_ATTR(clkerr1_input,            S_IRUGO, clkerr_show_input,        NULL, 0);
static SENSOR_DEVICE_ATTR(clkerr1_accuracy,         S_IRUGO, clkerr_show_accuracy,     NULL, 0);
static SENSOR_DEVICE_ATTR(clkerr1_interval,         S_IRUGO, clkerr_show_interval,     NULL, 0);
static SENSOR_DEVICE_ATTR(clkerr1_average,          S_IRUGO, clkerr_show_average,      NULL, 0);
static SENSOR_DEVICE_ATTR(clkerr1_average_accuracy, S_IRUGO, clkerr_show_avg_accuracy, NULL, 0);
static SENSOR_DEVICE_ATTR(clkerr1_average_interval, S_IRUGO, clkerr_show_avg_interval, NULL, 0);

static DEVICE_ATTR(reset,   0660, clkerr_show_reset,   clkerr_write_reset);
static DEVICE_ATTR(refresh, 0660, clkerr_show_refresh, clkerr_write_refresh);

static struct attribute *clkerr_attrs[] = {
	&dev_attr_reset.attr,
	&dev_attr_refresh.attr,
	&sensor_dev_attr_clkerr1_fault.dev_attr.attr,
	&sensor_dev_attr_clkerr1_input.dev_attr.attr,
	&sensor_dev_attr_clkerr1_accuracy.dev_attr.attr,
	&sensor_dev_attr_clkerr1_interval.dev_attr.attr,
	&sensor_dev_attr_clkerr1_average.dev_attr.attr,
	&sensor_dev_attr_clkerr1_average_accuracy.dev_attr.attr,
	&sensor_dev_attr_clkerr1_average_interval.dev_attr.attr,
	NULL,
};

static const struct attribute_group clkerr_attr_group = {
	.attrs = clkerr_attrs,
};
ATTRIBUTE_GROUPS(clkerr);

int clkerr_parse_dt(struct platform_device *pdev)
{
	int ret;
	struct resource res;
	struct device_node *fpga_node;
	struct platform_device *fpga_pdev;
	struct device_node *np = pdev->dev.of_node;
	struct clkerr_priv *priv = dev_get_drvdata(&pdev->dev);

	if (!np) {
                return -EINVAL;
	}

        fpga_node = of_parse_phandle(np, "fpga-device", 0);
        if (!fpga_node) {
		dev_err(&pdev->dev, "Can't get associated FPGA device from DT.\n");
                return -EINVAL;
        }
        fpga_pdev = of_find_device_by_node(fpga_node);
        if (!fpga_pdev) {
		dev_err(&pdev->dev, "Can't find FPGA device node.\n");
                return -EINVAL;
        }
	if (!fpgadl_is_loaded(fpga_pdev)) {
                return -EPROBE_DEFER;
	}

	ret = of_property_read_u32(np, "freq-mhz", &priv->freq);
	if (ret < 0) {
		dev_err(&pdev->dev, "Can't read reference clock frequency from DT.\n");
		return ret;
	}

        if (of_get_property(np, "auto-refresh", NULL) != NULL) {
                priv->auto_refresh = true;
        } else {
                priv->auto_refresh = false;
        }

	ret = of_property_read_u32(np, "pps-accuracy-ps", &priv->pps_accuracy);
	if (ret < 0) {
		dev_err(&pdev->dev, "Can't read PPS accuracy from DT.\n");
		return ret;
	}

	ret = of_address_to_resource(np, 0, &res);
	if (ret) {
		dev_err(&pdev->dev, "Can't read resource from DT.\n");
		return ret;
	} 
	priv->va = (u32 *)ioremap(res.start, resource_size(&res));

	return ret;
}

static int clkerr_probe(struct platform_device *pdev)
{
	struct clkerr_priv *priv;
	int ret;

	printk(KERN_INFO "nrw-clkerr: probe started...\n");
	priv = devm_kzalloc(&pdev->dev, sizeof(struct clkerr_priv), GFP_KERNEL);
	if (!priv)
		return -ENOMEM;

	priv->dev = &pdev->dev;
	platform_set_drvdata(pdev, priv);

	ret = clkerr_parse_dt(pdev);
	if (ret)	
		return ret;

	mutex_init(&priv->lock);
	/* WARNING: this prompt allows to see if there is a kernel freeze in clkerr_reset because of gpmc hardware issue */
	printk(KERN_INFO "nrw-clkerr: clk reset started...\n");
	clkerr_reset(&pdev->dev);
	printk(KERN_INFO "nrw-clkerr: clk reset ended.\n");

	ret = sysfs_create_group(&priv->dev->kobj, clkerr_groups[0]);
	if (ret) {
		dev_err(priv->dev, "unable to create sysfs files\n");
		return ret;
	}

	priv->hwmon_dev = devm_hwmon_device_register_with_groups(
				priv->dev, "clkerr", priv, clkerr_groups);
	if (IS_ERR(priv->hwmon_dev)) {
		dev_err(priv->dev, "unable to register as hwmon device.\n");
		ret = PTR_ERR(priv->hwmon_dev);
		goto err_after_sysfs;
	}

	printk(KERN_INFO "nrw-clkerr: probe ended.\n");
	return 0;

err_after_sysfs:
	sysfs_remove_group(&priv->dev->kobj, clkerr_groups[0]);
	return ret;
}

static int clkerr_remove(struct platform_device *pdev)
{
	struct clkerr_priv *priv = platform_get_drvdata(pdev);

	hwmon_device_unregister(priv->hwmon_dev);
	sysfs_remove_group(&priv->dev->kobj, clkerr_groups[0]);
	
	return 0;
}

static const struct of_device_id clkerr_of_match[] = {
        { .compatible = "nrw-clkerr", },
	{}
};
MODULE_DEVICE_TABLE(of, clkerr_of_match);

static struct platform_driver clkerr_driver = {
	.driver = {
		.name = "nrw-clkerr",
		.owner = THIS_MODULE,
                .of_match_table = of_match_ptr(clkerr_of_match),
	},
	.probe = clkerr_probe,
	.remove = clkerr_remove,
};

module_platform_driver(clkerr_driver);

MODULE_DESCRIPTION("Nuran Wireless Clock Error Driver");
MODULE_AUTHOR("<support@nuranwireless.com>");
MODULE_LICENSE("GPL");
