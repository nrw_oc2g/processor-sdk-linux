/*
 * adl5501.c - 50 MHz to 6 GHz TruPwr Detector
 *
 *  Copyright (C) 2015 Nuranwireless Inc.
 *  <support@nuranwireless.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#include <linux/slab.h>
#include <linux/module.h>
#include <linux/pm_runtime.h>
#include <linux/math64.h>
#include <linux/platform_device.h>
#include <linux/err.h>
#include <linux/of.h>
#include <linux/of_device.h>

#include <linux/iio/iio.h>
#include <linux/iio/machine.h>
#include <linux/iio/driver.h>
#include <linux/iio/consumer.h>

#include <linux/platform_data/adl5501.h>

#include <linux/hwmon.h>
#include <linux/hwmon-sysfs.h>

struct adl5501_data {
	struct device *hwmon_dev;
	struct adl5501_platform_data *pdata;
	struct device *dev;
};

#if defined(CONFIG_OF) && IS_ENABLED(CONFIG_IIO)
static int adl5501_adc_iio_read(struct adl5501_platform_data *pdata)
{
	struct iio_channel *channel = pdata->chan;
	int val, ret, result;

	ret = iio_read_channel_raw(channel, &val);
	if (ret < 0) {
		pr_err("read channel() error: %d\n", ret);
		return ret;
	}

	/* unit: mV */
	ret = iio_convert_raw_to_processed(channel, val, &result, 1u);
	if (ret < 0) {
		pr_err("convert raw to processed error: %d\n", ret);
		return ret;
	}
	return result;
}

static struct adl5501_platform_data *
adl5501_parse_dt(struct platform_device *pdev)
{
	struct iio_channel *chan;
	struct device_node *np = pdev->dev.of_node;
	struct adl5501_platform_data *pdata;

	if (!np)
		return NULL;

	pdata = devm_kzalloc(&pdev->dev, sizeof(*pdata), GFP_KERNEL);
	if (!pdata)
		return ERR_PTR(-ENOMEM);

	chan = iio_channel_get(&pdev->dev, NULL);
	if (IS_ERR(chan))
		return ERR_CAST(chan);

	if (of_property_read_u32(np, "gain", &pdata->gain))
		return ERR_PTR(-ENODEV);
	if (of_property_read_u32(np, "intercept", &pdata->intercept))
		return ERR_PTR(-ENODEV);
	pdata->intercept = -pdata->intercept;
	if (of_property_read_u32(np, "scaling", &pdata->scaling))
		return ERR_PTR(-ENODEV);

	pdata->chan = chan;

	return pdata;
}
static void adl5501_iio_channel_release(struct adl5501_platform_data *pdata)
{
	if (pdata->chan)
		iio_channel_release(pdata->chan);
}
#else
static struct adl5501_platform_data *
adl5501_parse_dt(struct platform_device *pdev)
{
	return NULL;
}

#define adl5501_match	NULL

static void adl5501_iio_channel_release(struct adl5501_platform_data *pdata)
{ }
#endif

static u32 get_power(struct adl5501_data *data)
{
	int mVadc;
	int mVin;
	u32 uW;

	// Get ADC reading
	mVadc = adl5501_adc_iio_read(data->pdata);

	// Convert to Vrms input
	mVin = (mVadc * data->pdata->gain) + data->pdata->intercept * 1000;
	if (mVin > 0 && data->pdata->scaling > 0) { 
		mVin = mVin / data->pdata->scaling;
	}
	else mVin = 0; 

	// Convert to uW	
	uW = (mVin * mVin) / 50u;
	return uW;
}

static ssize_t adl5501_show_name(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	return sprintf(buf, "adl5501\n");
}

static ssize_t adl5501_show_gain(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	struct adl5501_data *data = dev_get_drvdata(dev);
	return sprintf(buf, "%u\n", data->pdata->gain);
}

static ssize_t adl5501_write_gain(struct device *dev,
		struct device_attribute *attr,
		const char *buf, size_t count)
{
	struct adl5501_data *data = dev_get_drvdata(dev);
	unsigned long val;
	ssize_t ret;

	ret = kstrtoul(buf, 0, &val);
	if (ret)
		return ret;

	data->pdata->gain = val;

        return count;
}

static ssize_t adl5501_show_intercept(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	struct adl5501_data *data = dev_get_drvdata(dev);
	return sprintf(buf, "%d\n", data->pdata->intercept);
}

static ssize_t adl5501_write_intercept(struct device *dev,
		struct device_attribute *attr,
		const char *buf, size_t count)
{
	struct adl5501_data *data = dev_get_drvdata(dev);
	long val;
	ssize_t ret;

	ret = kstrtol(buf, 0, &val);
	if (ret)
		return ret;

	data->pdata->intercept = val;

        return count;
}

static ssize_t adl5501_show_scaling(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	struct adl5501_data *data = dev_get_drvdata(dev);
	return sprintf(buf, "%d\n", data->pdata->scaling);
}

static ssize_t adl5501_write_scaling(struct device *dev,
                struct device_attribute *attr,
                const char *buf, size_t count)
{
        struct adl5501_data *data = dev_get_drvdata(dev);
        unsigned long val;
        ssize_t ret;

        ret = kstrtoul(buf, 0, &val);
        if (ret)
                return ret;

        data->pdata->scaling = val;

        return count;
}

static ssize_t adl5501_show_power(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	struct adl5501_data *data = dev_get_drvdata(dev);

	return sprintf(buf, "%u\n", get_power(data));
}

static SENSOR_DEVICE_ATTR(power1_input, S_IRUGO, adl5501_show_power,     NULL, 0);
static DEVICE_ATTR(name,      S_IRUGO,           adl5501_show_name,      NULL);
static DEVICE_ATTR(gain,      0660, adl5501_show_gain,      adl5501_write_gain);
static DEVICE_ATTR(intercept, 0660, adl5501_show_intercept, adl5501_write_intercept);
static DEVICE_ATTR(scaling,   0660, adl5501_show_scaling,   adl5501_write_scaling);

static struct attribute *adl5501_attributes[] = {
	&dev_attr_name.attr,
	&dev_attr_gain.attr,
	&dev_attr_intercept.attr,
	&dev_attr_scaling.attr,
	&sensor_dev_attr_power1_input.dev_attr.attr,
	NULL,
};

static const struct attribute_group adl5501_attr_group = {
	.attrs = adl5501_attributes,
};

static int adl5501_probe(struct platform_device *pdev)
{
	struct adl5501_platform_data *pdata;
	struct adl5501_data *data;
	int ret;

	pdata = adl5501_parse_dt(pdev);
	if (IS_ERR(pdata))
		return PTR_ERR(pdata);
	else if (pdata == NULL)
		pdata = dev_get_platdata(&pdev->dev);

	if (!pdata) {
		dev_err(&pdev->dev, "No platform init data supplied.\n");
		return -ENODEV;
	}

	data = devm_kzalloc(&pdev->dev, sizeof(struct adl5501_data), GFP_KERNEL);
	if (!data)
		return -ENOMEM;

	data->dev = &pdev->dev;
	data->pdata = pdata;
	platform_set_drvdata(pdev, data);

	ret = sysfs_create_group(&data->dev->kobj, &adl5501_attr_group);
	if (ret) {
		dev_err(data->dev, "unable to create sysfs files\n");
		return ret;
	}

	data->hwmon_dev = hwmon_device_register(data->dev);
	if (IS_ERR(data->hwmon_dev)) {
		dev_err(data->dev, "unable to register as hwmon device.\n");
		ret = PTR_ERR(data->hwmon_dev);
		goto err_after_sysfs;
	}

	return 0;

err_after_sysfs:
	sysfs_remove_group(&data->dev->kobj, &adl5501_attr_group);
	adl5501_iio_channel_release(pdata);

	return ret;
}

static int adl5501_remove(struct platform_device *pdev)
{
	struct adl5501_data *data = platform_get_drvdata(pdev);
	struct adl5501_platform_data *pdata = data->pdata;

	hwmon_device_unregister(data->hwmon_dev);
	sysfs_remove_group(&data->dev->kobj, &adl5501_attr_group);
	adl5501_iio_channel_release(pdata);

	return 0;
}

static const struct of_device_id adl5501_of_match[] = {
        { .compatible = "adl5501", },
	{}
};
MODULE_DEVICE_TABLE(of, adl5501_of_match);

static struct platform_driver adl5501_driver = {
	.driver = {
		.name = "adl5501",
		.owner = THIS_MODULE,
                .of_match_table = of_match_ptr(adl5501_of_match),
	},
	.probe = adl5501_probe,
	.remove = adl5501_remove,
};

module_platform_driver(adl5501_driver);

MODULE_DESCRIPTION("ADL5501 TruPwr Detector Driver");
MODULE_AUTHOR("<support@nuranwireless.com>");
MODULE_LICENSE("GPL");
