#ifndef PALMAS_WDT_H
#define PALMAS_WDT_H

#ifdef CONFIG_HW_WATCHDOG
/* type for uboot with wdt */
typedef unsigned char PALMAS_WDT_BOOT_TYPE;
#else
/* type for kernel with wdt */
typedef unsigned PALMAS_WDT_BOOT_TYPE;
#endif

/* palmas base boot info */
#define PALMAS_WDT_BOOT_BASE 0x118

/* palmas boot id0 info register offset */
#define PALMAS_WDT_BOOT_REGID1 0
/* Identifies the boot section correctly */
#define PALMAS_WDT_BOOT_ID1 0x55

/* palmas boot id1 info register offset */
#define PALMAS_WDT_BOOT_REGID2 1
/* Identifies the boot section correctly */
#define PALMAS_WDT_BOOT_ID2 0xAA

/* palmas boot count register offset */
#define PALMAS_WDT_BOOT_REGCOUNT 2

/* palmas boot step register offset */
#define PALMAS_WDT_BOOT_REGSTEP 3

/* palmas boot step backup register offset */
#define PALMAS_WDT_BOOT_REGSTEPOLD 4

/* palmas boot event register offset */
#define PALMAS_WDT_BOOT_REGEVENT 5

/* palmas boot current u-boot instance offset */
#define PALMAS_WDT_BOOT_REGUINST 6

/* palmas boot checksum (255 based) register offset */
#define PALMAS_WDT_BOOT_REGCHK 7

/* Boot step values, lower 4 bits is the step, higher 4 bits is inverted step */
enum palmas_wdt_boot_step {
	PALMAS_WDT_BOOT_STEP_DONOTHING =	0xF0,
	PALMAS_WDT_BOOT_STEP_SPL = 		0xE1,
	PALMAS_WDT_BOOT_STEP_UBOOTS = 		0xD2,
	PALMAS_WDT_BOOT_STEP_UBOOTN = 		0xC3,
	PALMAS_WDT_BOOT_STEP_UBOOTA = 		0xB4,
	PALMAS_WDT_BOOT_STEP_UBOOTAF = 		0xA5,
	PALMAS_WDT_BOOT_STEP_UBOOTANC= 		0x96,
	PALMAS_WDT_BOOT_STEP_UBOOTAN = 		0x87,
	PALMAS_WDT_BOOT_STEP_UBOOTKR = 		0x78,
	PALMAS_WDT_BOOT_STEP_KERNELWDT_INST = 	0x69,
	PALMAS_WDT_BOOT_STEP_KERNELWDT_TICK = 	0x5A,
	PALMAS_WDT_BOOT_STEP_KERNELWDT_BOOT = 	0x4B,
	PALMAS_WDT_BOOT_STEP_KERNELWDT_LAST = 	PALMAS_WDT_BOOT_STEP_KERNELWDT_BOOT,
};

/* Structure to hold boot info */
struct palmas_wdt_boot {
	PALMAS_WDT_BOOT_TYPE id1;
	PALMAS_WDT_BOOT_TYPE id2;
	PALMAS_WDT_BOOT_TYPE count;
	PALMAS_WDT_BOOT_TYPE step;
	PALMAS_WDT_BOOT_TYPE stepold;
	PALMAS_WDT_BOOT_TYPE event;
	PALMAS_WDT_BOOT_TYPE ubootinst;
	PALMAS_WDT_BOOT_TYPE chk;
};

/* Boot reset event */
enum palmas_wdt_boot_event {
	PALMAS_WDT_BOOT_EVENT_POWERON =		0x00,
	PALMAS_WDT_BOOT_EVENT_GPADC_SHUTDOWN =	0x01,
	PALMAS_WDT_BOOT_EVENT_VSYS_LO =		0x02,
	PALMAS_WDT_BOOT_EVENT_SW_RST = 		0x04,
	PALMAS_WDT_BOOT_EVENT_RESET_IN =	0x08,
	PALMAS_WDT_BOOT_EVENT_TSHUT = 		0x10,
	PALMAS_WDT_BOOT_EVENT_WDT = 		0x20,
	PALMAS_WDT_BOOT_EVENT_POWERDOWN =	0x40,
	PALMAS_WDT_BOOT_EVENT_PWRON_LPK = 	0x80,
	PALMAS_WDT_BOOT_EVENT_SOCRST = 		0xfd,
	PALMAS_WDT_BOOT_EVENT_POWERON_SOCRST = 	0xfe,
	PALMAS_WDT_BOOT_EVENT_UNKNOWN = 	0xff,
};

/* Boot info validation status */
enum palmas_wdt_boot_valid {
	PALMAS_WDT_BOOT_VALID_OK =		0x00,
	PALMAS_WDT_BOOT_VALID_ID1 =		0x01,
	PALMAS_WDT_BOOT_VALID_ID2 =		0x02,
	PALMAS_WDT_BOOT_VALID_STEP =		0x04,
	PALMAS_WDT_BOOT_VALID_STEPOLD =		0x08,
	PALMAS_WDT_BOOT_VALID_EVENT =		0x10,
	PALMAS_WDT_BOOT_VALID_UINST =		0x20,
	PALMAS_WDT_BOOT_VALID_CHK = 		0x40,
};

#endif

