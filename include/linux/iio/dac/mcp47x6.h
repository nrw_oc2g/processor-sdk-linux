/*
 * MCP47X6 DAC driver
 *
 * Copyright (C) 2015 Nuran Wireless <support@nuranwireless.com>
 *
 * Based on mcp4725 by Peter Meerwald <pmeerw@pmeerw.net>
 *
 * Licensed under the GPL-2 or later.
 *
 */

#ifndef IIO_DAC_MCP47X6_H_
#define IIO_DAC_MCP47X6_H_

enum mcp47x0_vrl_sel {
	MCP47X6_VRL_VDD_UNBUFFERED  = 0,
	MCP47X6_VRL_VREF_UNBUFFERED = 2,
	MCP47X6_VRL_VREF_BUFFERED   = 3,
};

enum mcp47x6_pd_sel {
	MCP47X6_PD_1K_LOAD   = 0,
	MCP47X6_PD_100K_LOAD = 1,
	MCP47X6_PD_500K_LOAD = 2,
};


enum mcp47x6_g_sel {
	MCP47X6_G_1X = 0,
	MCP47X6_G_2X = 1,
};

struct mcp47x6_pdata {
	u16 vref_mv;
	enum mcp47x0_vrl_sel vrl_sel;
	enum mcp47x6_pd_sel pd_sel;
	enum mcp47x6_g_sel g_sel; 
};
	
#endif /* IIO_DAC_MCP47X6_H_ */
