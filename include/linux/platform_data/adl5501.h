/*
 * adl5501.h - 50 MHz to 6 GHz TruPwr Detector
 *
 *  Copyright (C) 2015 Nuranwireless Inc.
 *  <support@nuranwireless.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#ifndef _LINUX_ADL5501_H
#define _LINUX_ADL5501_H

struct iio_channel;

struct adl5501_platform_data {
	int intercept;
	unsigned int gain;
	unsigned int scaling;	
	
	struct iio_channel *chan;
};

#endif /* _LINUX_NTC_H */
