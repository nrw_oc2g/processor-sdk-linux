#ifndef RTFIFO_H
#define RTFIFO_H

void *rtfifo_start(void *va, int len);
int rtfifo_stop(void *_dev);
int rtfifo_restart(void *va, int len, void *_dev);
int rtfifo_pause(void *_dev);

#endif

